<?php
defined('LOCALHOST') or define('LOCALHOST', $_SERVER['SERVER_NAME'] === 'dprof.lo');
//defined('INTER') or define('INTER', $_SERVER['SERVER_ADDR'] == '10.23.250.244');
defined('INTER') or define('INTER', $_SERVER['SERVER_NAME'] == 'dprof.msk.oao.rzd');


include  dirname(__FILE__) . '/protected/vendor/autoload.php';

if (LOCALHOST) {
    defined('YII_DEBUG') or define('YII_DEBUG', true);
    $config = dirname(__FILE__) . '/protected/config/local.php';
    $yii = dirname(__FILE__) . '/protected/vendor/yiisoft/yii/framework/yii.php';
} elseif (INTER) {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    $config = dirname(__FILE__) . '/protected/config/inter.php';
    $yii = dirname(__FILE__) . '/protected/vendor/yiisoft/yii/framework/yii.php';
} else {
    defined('YII_DEBUG') or define('YII_DEBUG', false);
    $config = dirname(__FILE__) . '/protected/config/web.php';
    $yii = dirname(__FILE__) . '/protected/vendor/yiisoft/yii/framework/yii.php';
}

require_once($yii);
Yii::createWebApplication($config)->run();
