<?php

use Zend\Mail\Message;

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ResoForm extends CFormModel
{

    public $name;
    public $surname;
    public $second_name;
    public $email;
    public $phone;
    public $card_number;
    public $product;
    public $city;
    public $captcha_code;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('name, surname, second_name,  email, phone, card_number, product, city', 'required'),
            array('email', 'email'),
        );
    }

    public function send()
    {
        mb_internal_encoding('UTF-8');
        $subject = 'Новая заявка на страхование с сайта Дорпрофжел на Московской ЖД';
        foreach (Yii::app()->params['resoEmails'] as $emailTo) {
            mail($emailTo, $subject, $this->getBody(), implode("\r\n", ["From: noreply@dprof.ru", "Content-Type: text/html; charset=UTF-8"]));
        }
        return true;
    }

    public function getBody()
    {
        $res = ['Данные формы:<br> '];
        foreach ($this->attributeLabels() as $key => $attributeLabel) {
            if($key === 'captcha_code') continue;
            $res[] = "<b>$attributeLabel:</b> {$this->{$key}}";
        }
        return implode('<br> ', $res);
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels()
    {
        return array(
            'surname' => 'Фамиилия',
            'name' => 'Имя',
            'second_name' => 'Отчество',
            'email' => 'Адрес email',
            'phone' => 'Контактный телефон',
            'card_number' => 'Номер профсоюзного билета',
            'product' => 'Продукт',
            'city' => 'Город',
            'captcha_code' => 'Проверка на робота',
        );
    }

}