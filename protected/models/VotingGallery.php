<?php

/**
 * This is the model class for table "{{voting_gallery}}".
 *
 * The followings are the available columns in table '{{voting_gallery}}':
 * @property integer $id
 * @property string $name
 * @property integer $region
 * @property integer $nomination
 * @property integer $created
 * @property integer $image_id
 * @property integer $active
 */
class VotingGallery extends CActiveRecord
{
    const VOTING_ID = 'VotingGallery18';
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{voting_gallery}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, region, nomination', 'required'),
            array('region, nomination, created', 'numerical', 'integerOnly' => true),
            array('active, image', 'safe'),

            array('id, name, region, nomination, created, image_id', 'safe', 'on' => 'search'),
        );
    }

    public function getVoteCount() {
        return Voting::model()->count('voting_id=:voting_id and for=:for', array(
            ':voting_id' => self::VOTING_ID,
            ':for' => $this->id,
        ));
    }

    public function getHasVoted() {
        return !!Voting::model()->count('voting_id=:voting_id and for=:for and user_id=:user_id', array(
            ':voting_id' => self::VOTING_ID,
            ':for' => $this->id,
            ':user_id' => Yii::app()->user->id,
        ));
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'image' => array(self::BELONGS_TO, 'BDImage', 'image_id'),
            'voteCountRel' => array(self::STAT, 'Voting', 'for', 'condition' => 'voting_id="VotingGallery18"'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Название работы',
            'region' => 'Регион',
            'nomination' => 'Номинация',
            'created' => 'Создан',
            'image' => 'Фото',
            'image_id' => 'Фото ID',
            'active' => 'Активность',
        );
    }


    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->created = time();
        }
        $this->addImage();
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {

    }

    public function addImage()
    {
        if (!isset(Yii::app()->user)) {
            return false;
        }
        //If we have pending images
        if (Yii::app()->user->hasState('images')) {
            $image = Yii::app()->user->getState('images')[0];
            //Resolve the final path for our images
            //Now lets create the corresponding models and move the files
            $img = new BDImage();
            $img->file_name = $image['name'];
            $img->post_id = 0;
            if (!$img->save()) {
                //Its always good to log something
                Yii::log("Could not save Image:\n" .
                    CVarDumper::dumpAsString($img->getErrors()), CLogger::LEVEL_ERROR);
                //this exception will rollback the transaction
                Yii::app()->user->setState('images', null);
                throw new Exception('Could not save Image');
            } else {
                $this->image_id = $image['name'];
            }

            //Clear the user's session
            Yii::app()->user->setState('images', null);
        }
    }


    public function getUrl()
    {
        return Yii::app()->createUrl('voteGallery/view', array(
            'id' => $this->id,
            'name' => $this->name,
        ));
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name);
        $criteria->compare('region', $this->region);
        $criteria->compare('nomination', $this->nomination);
//        $criteria->compare('created', $this->created);
        $criteria->compare('image_id', $this->image_id);
        $criteria->compare('active', $this->active);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
            'pagination'=>array(
                'pageSize'=>50,
            ),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return VotingGallery the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
