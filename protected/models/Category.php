<?php

/**
 * This is the model class for table "tbl_menu".
 */
class Category extends CActiveRecord
{

    const DOC = 71;

    /**
     * The followings are the available columns in table 'tbl_menu':
     * @property integer $id
     * @property integer $parent_id
     * @property string $title
     * @property integer $position
     */
    public $parentPath;

    /**
     * Returns the static model of the specified AR class.
     * @return Menu the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'tbl_category';
    }

    /**
     * @return array behaviors.
     */
    public function behaviors()
    {
        return array(
            'TreeBehavior' => array(
                'class' => 'ext.behaviors.XTreeBehavior',
                'treeLabelMethod' => 'getTreeLabel',
                'menuUrlMethod' => 'getMenuUrl',
                'sort' => 'position'
//                'menuLabelMethod' => 'getMenuLabel',
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent_id, position', 'numerical', 'integerOnly' => true),
            array('label, title', 'length', 'max' => 256),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'parent' => array(self::BELONGS_TO, 'Category', 'parent_id'),
            'children' => array(self::HAS_MANY, 'Category', 'parent_id', 'order' => 'position'),
            'childCount' => array(self::STAT, 'Category', 'parent_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('ui', 'ID'),
            'parent_id' => Yii::t('ui', 'Parent'),
            'label' => Yii::t('ui', 'Label'),
            'position' => Yii::t('ui', 'Position'),
        );
    }

    /**
     * @return string tree label
     */
    public function getTreeLabel()
    {
        return $this->label . ':' . $this->childCount;
    }

    public function getMenuItems($id = null, $showRoot = true, $showHidden = false)
    {
        $rootId = ($id === null) ? $this->getRootId() : $id;
        $items = array();
        if ($showRoot === false) {
            $models = $this->findAll(array(
                'condition' =>  'parent_id =' . $rootId . ' AND is_hidden = 0',
                'order' => $this->sort
            ));
            if ($models === null)
                throw new CException('The requested menu does not exist.');
            foreach ($models as $model)
                $items[] = $model->getMenuSubItems();
        } else {
            $model = $this->findByPk($rootId);
            if ($model === null)
                throw new CException('The requested menu does not exist.');
            else
                $items[] = $model->getMenuSubItems();
        }
        return $items;
    }


    /**
     * @return string tree label
     */
    public function getMenuLabel()
    {
        return $this->title . ':' . $this->childCount;
    }

    public function getChildrenList($rootId)
    {
        $items = $this->model()->findAll(array(
            'condition' => 'parent_id=:id',
            'params' => array(':id' => $rootId),
            'order' => 'position, label',
        ));
        $res = array();
        foreach ($items as $item) {
            $res[$item->id] = $item->label;
        }
        return $res;
    }

    public function getOneLevelMenu($rootId)
    {
        $items = $this->model()->findAll(array(
            'condition' => 'parent_id=:id AND is_hidden = 0',
            'params' => array(':id' => $rootId),
            'order' => 'position, label',
        ));
        $res = array();
        foreach ($items as $item) {
            $res[] = array(
                'label' => $item->label,
                'url' => array('post/index', 'cat' => $item->id)
            );
        }
        return $res;
    }

    public function getDocFilterMenu()
    {
        $items = $this->model()->findAll(array(
            'condition' => 'parent_id=:id',
            'params' => array(':id' => Category::DOC),
            'order' => 'position, label',
        ));
        $res = array();
//        $res[] = array(
//            'label' => 'Все документы',
//            'url' => array('doc/index'),
//            'linkOptions' => array('class' => 'link'),
//            'active' => Yii::app()->request->getParam('cat') == null,
//        );
        foreach ($items as $item) {
            $res[] = array(
                'label' => $item->label,
                'url' => array('doc/index', 'cat' => $item->id),
                'linkOptions' => array('class' => 'link')
            );
        }
        return $res;
    }

    /**
     * @return array menu url
     */
    public function getMenuUrl()
    {
        if (Yii::app()->controller->action->id == 'adminMenu')
            return array('admin', 'id' => $this->id);
        else
            return array('post/index', 'cat' => $this->id);
    }

    /**
     * Retrieves a list of child models
     * @param integer the id of the parent model
     * @return CActiveDataProvider the data provider
     */
    public function getDataProvider($id = null)
    {
        if ($id === null)
            $id = $this->TreeBehavior->getRootId();
        $criteria = new CDbCriteria(array(
            'condition' => 'parent_id=:id',
            'params' => array(':id' => $id),
            'order' => 'label',
            'with' => 'childCount',
        ));
        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria,
            'pagination' => false,
        ));
    }

    /**
     * Suggests a list of existing values matching the specified keyword.
     * @param string the keyword to be matched
     * @param integer maximum number of names to be returned
     * @return array list of matching lastnames
     */
    public function suggest($keyword, $limit = 20)
    {
        $models = $this->findAll(array(
            'condition' => 'label LIKE :keyword',
            'limit' => $limit,
            'params' => array(':keyword' => "$keyword%")
        ));
        $suggest = array();
        foreach ($models as $model) {
            $suggest[] = array(
                'label' => $model->TreeBehavior->pathText, // label for dropdown list
                'value' => $model->label, // value for input field
                'id' => $model->id, // return values from autocomplete
            );
        }
        return $suggest;
    }

}