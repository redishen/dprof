<?php

/**
 * This is the model class for table "{{document}}".
 *
 * The followings are the available columns in table '{{document}}':
 * @property string $id
 * @property integer $post_id
 * @property string $file_name
 * @property string $type
 * @property integer $size
 * @property string $mime_type
 *
 * The followings are the available model relations:
 * @property Post $post
 */
class DBDocument extends CActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return DBDocument the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{document}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('file_name, type', 'required'),
            array('post_id, size', 'numerical', 'integerOnly' => true),
            array('id', 'length', 'max' => 17),
            array('type', 'length', 'max' => 4),
            array('mime_type', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, post_id, file_name, type, size, mime_type', 'safe', 'on' => 'search'),
            array('id, post_id, file_name, type, size, mime_type', 'safe', 'on' => 'sync'),
        );
    }

    public static function getPath()
    {
        return Yii::getPathOfAlias('webroot') . Yii::app()->params['docPath'];
    }

    public function getFilePath()
    {
        return self::getPath() . $this->id;
    }


    protected function beforeDelete()
    {
        parent::beforeDelete();
        if (@unlink($this->getFilePath())) {
            return TRUE;
        } else {
//          todo: error handling
            return TRUE;
        }
    }

    public function getUrl()
    {
        return Yii::app()->createUrl('/doc/download', array('id' => $this->id, 'filename' => $this->file_name));
    }

    public function getPreviewUrl()
    {
        if (in_array($this->type, array('pdf', 'png', 'txt', 'zip', 'avi'))) {
            $type = $this->type;
        } elseif (in_array($this->type, array('jpg', 'jpeg', 'JPG'))) {
            $type = 'jpg';
        } elseif (in_array($this->type, array('ppt', 'pptx'))) {
            $type = 'ppt';
        } elseif (in_array($this->type, array('xls', 'xlsx'))) {
            $type = 'xls';
        } elseif (in_array($this->type, array('doc', 'docx'))) {
            $type = 'doc';
        } else {
            $type = 'file';
        }
        return '/css/img/doc/' . $type . '.png';
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'post_id' => 'Post',
            'file_name' => 'File Name',
            'type' => 'Type',
            'size' => 'Size',
            'mime_type' => 'Mime Type',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('post_id', $this->post_id);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('size', $this->size);
        $criteria->compare('mime_type', $this->mime_type, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public function searchDocs()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('post_id', $this->post_id);
        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('size', $this->size);
        $criteria->compare('mime_type', $this->mime_type, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}