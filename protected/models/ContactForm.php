<?php

/**
 * ContactForm class.
 * ContactForm is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class ContactForm extends CFormModel {

    public $name;
    public $email;
    public $subject;
    public $body;
    // public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            // name, email, subject and body are required
            array('name, email, subject, body', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            // verifyCode needs to be entered correctly
            // array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'name' => 'Ваше имя',
            'email' => 'Электронная почта',
            'subject' => 'Тема обращения',
            'body' => 'Сообщение',
        );
    }

    public function send()
    {
        mb_internal_encoding('UTF-8');
        $subject = 'Новое сообщение через форму виртуальной приемной с сайта Дорпрофжел на Московской ЖД';
        foreach (Yii::app()->params['contactEmails'] as $emailTo) {
            mail($emailTo, $subject, $this->getBody(), implode("\r\n", ["From: noreply@dprof.ru", "Content-Type: text/html; charset=UTF-8"]));
        }
        return true;
    }

    public function getBody()
    {
        $res = ['Данные формы:<br> '];
        foreach ($this->attributeLabels() as $key => $attributeLabel) {
            if($key === 'captcha_code') continue;
            $res[] = "<b>$attributeLabel:</b> {$this->{$key}}";
        }
        return implode('<br> ', $res);
    }
}