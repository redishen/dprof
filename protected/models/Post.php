<?php

class Post extends CActiveRecord
{

    /**
     * The followings are the available columns in table 'tbl_post':
     * @property integer $id
     * @property string $title
     * @property string $preview_text
     * @property string $content
     * @property string $tags
     * @property integer $status
     * @property integer $create_time
     * @property integer $update_time
     * @property integer $author_id
     * @property boolean $on_main
     * @property boolean $is_doc
     *
     * @property boolean $view_count_local
     * @property boolean $view_count_web
     *
     *
     *
     * @property string $related_posts
     *
     * @property boolean $is_event
     * @property string $event_from
     * @property string $event_to
     * @property string $event_note
     *
     * The followings are the available model relations:
     * @property Comment[] $comments
     * @property PostCategory[] $postCategories
     * @property BDImage[] $postImages
     * @property DBDocument[] $postDocuments
     */
    const STATUS_DRAFT = 1;
    const STATUS_PUBLISHED = 2;
    const STATUS_ARCHIVED = 3;
    const STATUS_IMPORTANT = 4;
    const STATUS_HIDDEN = 5;


    public $documents;
    public $videoHref;
    public $videoLocal;
    protected $_oldTags;

//    public $categories;

    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return '{{post}}';
    }

    public function behaviors()
    {
        return array(
            'EJsonBehavior' => array(
                'class' => 'ext.behaviors.EJsonBehavior'
            ),
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, preview_text, status', 'required', 'on' => 'insert, update'),
            array('title, categories', 'required', 'on' => 'doc'),
            array('status', 'in', 'range' => array(1, 2, 3, 4)),
            array('content, related_posts', 'safe'),
            array('event_from', 'validateEvent', 'on' => 'insert, update'),
            array('is_event, event_from, event_to, event_note', 'safe'),
            array('content, related_posts, preview_text', 'default', 'value' => ''),
            array('is_doc', 'default', 'value' => 1, 'on' => 'doc'),
            array('preview_text', 'safe', 'on' => 'doc'),
            array('title', 'length', 'max' => 128),
            array('tags', 'match', 'pattern' => '/^[\D\w\s,]+$/ui', 'message' => 'Тэги могут содержать только буквы.'),
            array('tags', 'normalizeTags'),
            array('update_time, videoHref, videoLocal, on_main, categories, documents', 'safe'),
            array('title, status, categories, images', 'safe', 'on' => 'search'),
            array('preview_text', 'safe', 'on' => 'doc'),
            array('id, title, preview_text, status, content, tags, status, create_time, update_time, author_id, on_main, view_count_local, view_count_web, is_doc', 'safe', 'on' => 'sync, safe'),
        );
    }

    public function validateEvent($attribute) {
        if ($this->is_event && empty($this->$attribute)) {
            $this->addError($attribute, "Если привязано событие, необходимо заполнить поле " . $this->getAttributeLabel($attribute));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'author' => array(self::BELONGS_TO, 'User', 'author_id'),
            'comments' => array(self::HAS_MANY, 'Comment', 'post_id', 'condition' => 'comments.status=' . Comment::STATUS_APPROVED, 'order' => 'comments.create_time DESC'),
            'commentCount' => array(self::STAT, 'Comment', 'post_id', 'condition' => 'status=' . Comment::STATUS_APPROVED),
            'postImages' => array(self::HAS_MANY, 'BDImage', 'post_id'),
            'postDocuments' => array(self::HAS_MANY, 'DBDocument', 'post_id', 'order' => 'file_name ASC'),
            'postVideos' => array(self::HAS_MANY, 'Video', 'post_id'),
            'postCategories' => array(self::HAS_MANY, 'PostCategory', 'post_id'),
            'categories' => array(self::HAS_MANY, 'Category', array('category_id' => 'id'), 'through' => 'postCategories'),
            'docCategories' => array(self::HAS_MANY, 'Category', array('category_id' => 'id'), 'through' => 'postCategories'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'Id',
            'title' => 'Заголовок',
            'preview_text' => 'Анонс',
            'content' => 'Содержание',
            'tags' => 'Тэги',
            'status' => 'Статус',
            'create_time' => 'Время создания',
            'update_time' => 'Время обновления',
            'author_id' => 'Автор',
            'documents' => 'Документы',
            'documents[]' => 'Документы',
            'images' => 'Изображения',
            'videoHref' => 'Видео на YouTube',
            'videoLocal' => 'Видео в интранет',
            'on_main' => 'Разместить на главной',
            'view_count_local' => 'Просмотров в локальной сети',
            'view_count_web' => 'Просмотров в интернете',
            'related_posts' => 'Связанные записи',
            'is_event' => 'Привязать событие',
            'event_from' => 'Начало события',
            'event_to' => 'Окончание события',
            'event_note' => 'Заметка',
            //  'postCategories.categories' => 'Категории',
        );
    }

    public function updateViewCount()
    {
        $isWeb = Yii::app()->params['isWeb'];
        if ($isWeb) {
            $this->updateCounters(array('view_count_web' => 1), 'id = :id', array(':id' => $this->id));
        } else {
            $this->updateCounters(array('view_count_local' => 1), 'id = :id', array(':id' => $this->id));
        }
    }

    public function getViewCount()
    {
        $isWeb = Yii::app()->params['isWeb'];
        if ($isWeb) {
            return $this->view_count_web;
        } else {
            if ($this->view_count_local == 0 && $this->view_count_web != 0) {
                return  floor($this->view_count_web / 4);
            } else {
                return $this->view_count_local;
            }
        }
    }

    public function getBreadcrumbs()
    {
        $crumbs = array();
        $subRootLabel = $this->is_doc ? 'Документы' : 'Новости';

        $crumbs[$subRootLabel] = array($this->controllerName . '/index');

        $cats = array();
        foreach ($this->getCategoriesList() as $id => $cat) {
            $cats[] = CHtml::link($cat, array($this->controllerName . '/index', 'cat' => $id), array('style' => 'font-weight: normal;'));
        }
        $crumbs[] = implode(', ', $cats);
        $crumbs[] = $this->title;
        return $crumbs;
    }

    public function getCategoriesList()
    {

        $list = array();
        if (isset($this->categories)) {
            $list = CHtml::listData($this->categories, 'id', 'label');
        }
        return $list;
    }

    public function getDocCategoriesList()
    {

        $list = array();
        if (isset($this->categories)) {
            $list = CHtml::listData($this->categories, 'id', 'label');
        }
        return $list;
    }


    public function getImagesLinks()
    {
        $images = array();
        if (!empty($this->postImages)) {
            foreach ($this->postImages as $image) {
                $images[] = array(
                    'biggest' => Yii::app()->request->baseUrl . '/files/images/' . $image->file_name,
                    'smallest' => Yii::app()->request->baseUrl . '/files/images/thumbs/150x150/' . $image->file_name,
                );
            }
        }
        return $images;
    }

    public function getControllerName()
    {
        return $this->is_doc ? 'doc' : 'post';
    }

    /**
     * @return string the URL that shows the detail of the post
     */
    public function getUrl()
    {
        return Yii::app()->createUrl($this->controllerName . '/view', array(
            'id' => $this->id,
            'title' => $this->title,
        ));
    }

    /**
     * @return array a list of links that point to the post list filtered by every tag of this post
     */
    public function getTagLinks()
    {
        $links = array();
        foreach (Tag::string2array($this->tags) as $tag)
            $links[] = CHtml::link(CHtml::encode($tag), array('post/index', 'tag' => $tag));
        return $links;
    }

    /**
     * Normalizes the user-entered tags.
     */
    public function normalizeTags($attribute, $params)
    {
        $this->tags = Tag::array2string(array_unique(Tag::string2array($this->tags)));
    }

    /**
     * Adds a new comment to this post.
     * This method will set status and post_id of the comment accordingly.
     * @param Comment the comment to be added
     * @return boolean whether the comment is saved successfully
     */
    public function addComment($comment)
    {
        if (Yii::app()->params['commentNeedApproval'])
            $comment->status = Comment::STATUS_PENDING;
        else
            $comment->status = Comment::STATUS_APPROVED;
        $comment->post_id = $this->id;
        return $comment->save();
    }

    /**
     * This is invoked when a record is populated with data from a find() call.
     */
    protected function afterFind()
    {
        parent::afterFind();
        $this->_oldTags = $this->tags;

        if (isset($this->postVideos)) {
            $this->videoHref = isset($this->postVideos[0]) ? $this->postVideos[0]->href : null;
            $this->videoLocal = isset($this->postVideos[0]) ? $this->postVideos[0]->local : null;
        }
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    protected function beforeSave()
    {
        if ($this->getScenario() == 'sync') {
            return true;
        }
        if ($this->isNewRecord) {
            switch (isset($_POST['postType']) && $_POST['postType']) {
                case 0:
                    $this->update_time = time();
                    break;
                case 1:
                    $this->update_time = strtotime('-1 day');
                    break;
                case 2:
                    $this->update_time = strtotime('today +1 day');
                    break;
                case 3:
                    $this->update_time = strtotime($this->update_time);
                    break;
            }

            if ($this->scenario == 'doc') {
                $this->is_doc = true;
                $this->on_main = false;
            }

            $this->create_time = time();
            if (!isset(Yii::app()->user)) {
                $this->author_id = 2;
            } else {
                $this->author_id = Yii::app()->user->id;
            }
        } else {
            $this->update_time = strtotime($this->update_time);
        }
        return true;
    }

    /**
     * This is invoked after the record is saved.
     */
    protected function afterSave()
    {
        if ($this->getScenario() == 'sync') {
            return true;
        }
        $this->addImages();
        $this->addDocs();
        Tag::model()->updateFrequency($this->_oldTags, $this->tags);
    }

    public function addImages()
    {
        if (!isset(Yii::app()->user)) {
            return false;
        }
        //If we have pending images
        if (Yii::app()->user->hasState('images')) {
            $userImages = Yii::app()->user->getState('images');
            //Resolve the final path for our images
            //Now lets create the corresponding models and move the files
            foreach ($userImages as $image) {
                $img = new BDImage();
                $img->file_name = $image['name'];
                $img->post_id = $this->id;
                if (!$img->save()) {
                    //Its always good to log something
                    Yii::log("Could not save Image:\n" .
                        CVarDumper::dumpAsString($img->getErrors()), CLogger::LEVEL_ERROR);
                    //this exception will rollback the transaction
                    Yii::app()->user->setState('images', null);
                    throw new Exception('Could not save Image');
                }
            }
            //Clear the user's session
            Yii::app()->user->setState('images', null);
        }
    }

    public function addDocs()
    {
        if (!isset(Yii::app()->user)) {
            return false;
        }
        //If we have pending images
        if (Yii::app()->user->hasState('docs')) {
            $userDocs = Yii::app()->user->getState('docs');
            //Resolve the final path for our images
            //Now lets create the corresponding models and move the files
            foreach ($userDocs as $doc) {
                $model = new DBDocument();

                $model->id = $doc['id'];
                $model->post_id = $this->id;
                $model->file_name = $doc['file_name'];

                if (strlen($doc['type']) > 4) {
                    $doc['type'] = substr($doc['type'], 0, 4);
                }
                $model->type = $doc['type'];

                $model->mime_type = $doc['mime_type'];
                $model->size = $doc['size'];

                if (!file_exists($model->getFilePath())) {
                    Yii::log("Uploaded file not found in {$model->getFilePath()}");
                    continue;
                }

                if (!$model->save()) {
                    //Its always good to log something
                    Yii::log("Could not save Document:\n" .
                        CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR);
                    //this exception will rollback the transaction
                    Yii::app()->user->setState('docs', null);
                    throw new Exception('Could not save Document');
                }
            }
            //Clear the user's session
            Yii::app()->user->setState('docs', null);
        }
    }

    /**
     * This is invoked after the record is deleted.
     */
    protected function afterDelete()
    {
        parent::afterDelete();
        Comment::model()->deleteAll('post_id=' . $this->id);
        Tag::model()->updateFrequency($this->tags, '');
        BDImage::model()->deleteAll('post_id=' . $this->id);
    }

    public function getRelatedPosts()
    {
        if (empty($this->related_posts)) {
            return null;
        }
        $pks = explode(',', $this->related_posts);
        $models = $this->model()->findAllByPk($pks, array('order' => 'is_doc DESC, update_time DESC'));
        return $models;
    }

    /**
     * Retrieves the list of posts based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the needed posts.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('is_doc = 0');
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider('Post', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'update_time DESC',
            ),
        ));
    }

    public function searchDocs()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('is_doc = 1');
        $criteria->compare('title', $this->title, true);
        $criteria->compare('status', $this->status);

        return new CActiveDataProvider('Post', array(
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => 'update_time DESC',
            ),
        ));
    }

}
