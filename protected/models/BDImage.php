<?php

/**
 * This is the model class for table "{{image}}".
 *
 * The followings are the available columns in table '{{image}}':
 * @property string $file_name
 * @property integer $post_id
 * @property string $title
 *
 * The followings are the available model relations:
 * @property Post $post
 */
class BDImage extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Image the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    protected function beforeDelete() {
        parent::beforeDelete();
        $imgPath = Yii::app()->params['imgPath'];
        $path = realpath(Yii::app()->getBasePath() . "/.." . $imgPath) . "/";
        $file = $path . $this->file_name;
        if (is_file($file)) {
            unlink($file);
        }
        foreach (Yii::app()->params['thumbs'] as $thumbDem) {
            $thumb = $path . "thumbs/$thumbDem/" . $this->file_name;
            if (is_file($thumb)) {
                unlink($thumb);
            }
        }
        return true;
    }

    public function getUrl() {
        return Yii::app()->request->baseUrl . '/files/images/' . $this->file_name;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return '{{image}}';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('file_name', 'required'),
            array('post_id', 'numerical', 'integerOnly' => true),
            array('file_name', 'length', 'max' => 17),
            array('title', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('file_name, post_id, title', 'safe', 'on' => 'search'),
            array('file_name, post_id, title', 'safe', 'on' => 'sync'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'post' => array(self::BELONGS_TO, 'Post', 'post_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'file_name' => 'File Name',
            'post_id' => 'Post',
            'title' => 'Title',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('file_name', $this->file_name, true);
        $criteria->compare('post_id', $this->post_id);
        $criteria->compare('title', $this->title, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
