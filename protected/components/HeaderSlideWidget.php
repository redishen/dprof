<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of HeaderSlideWidget
 *
 * @author asfto_sheftelevichdm
 */
class HeaderSlideWidget extends CWidget
{

    public $imgs = array(
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-2.jpg'),
        array('title' => 'ВСТУПАЙ В ПРОФСОЮЗ!', 'cat' => 2, 'img' => '/files/site/carousel/main-pic-1.jpg'),
        array('title' => 'ПРАВОВАЯ ЗАЩИТА', 'cat' => 16, 'img' => '/files/site/carousel/main-pic-111.jpg'),
        array('title' => 'ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД', 'cat' => 17, 'img' => '/files/site/carousel/main-pic-8.jpg'),
        array('title' => 'ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ', 'cat' => 38, 'img' => '/files/site/carousel/main-pic-15.jpg'),
        array('title' => 'ДЕТИ И ПРОФСОЮЗ', 'cat' => 15, 'img' => '/files/site/carousel/main-pic-14.jpg'),
        array('title' => 'ЗАБОТА О ВЕТЕРАНАХ', 'cat' => 37, 'img' => '/files/site/carousel/main-pic-9.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => 'МОЛОДЕЖЬ И ПРОФСОЮЗ', 'cat' => 14, 'img' => '/files/site/carousel/main-pic-13.jpg'),
        array('title' => 'ЗАБОТА О ВЕТЕРАНАХ', 'cat' => 37, 'img' => '/files/site/carousel/main-pic-10.jpg'),
        array('title' => 'ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД', 'cat' => 17, 'img' => '/files/site/carousel/main-pic-16.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => 'ЗАБОТА О ВЕТЕРАНАХ', 'cat' => 37, 'img' => '/files/site/carousel/main-pic-18.jpg'),
        array('title' => 'ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД', 'cat' => 17, 'img' => '/files/site/carousel/main-pic-19.jpg'),
        array('title' => 'МОЛОДЕЖЬ И ПРОФСОЮЗ', 'cat' => 14, 'img' => '/files/site/carousel/main-pic-20.jpg'),
        array('title' => 'ВСТУПАЙ В ПРОФСОЮЗ!', 'cat' => 2, 'img' => '/files/site/carousel/main-pic-21.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => '2020 — год Единства, Памяти и Славы', 'cat' => 21, 'img' => '/files/site/carousel/main-pic-xx-2020.png'),
        array('title' => 'СОЦИАЛЬНОЕ ПАРТНЕРСТВО', 'cat' => 21, 'img' => '/files/site/carousel/main-pic-23.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => 'ДЕТИ И ПРОФСОЮЗ', 'cat' => 15, 'img' => '/files/site/carousel/main-pic-26.jpg'),
        array('title' => 'ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД', 'cat' => 17, 'img' => '/files/site/carousel/main-pic-27.jpg'),
        array('title' => 'ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ', 'cat' => 38, 'img' => '/files/site/carousel/main-pic-28.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => 'ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ', 'cat' => 38, 'img' => '/files/site/carousel/main-pic-29.jpg'),
        array('title' => 'ДЕТИ И ПРОФСОЮЗ', 'cat' => 15, 'img' => '/files/site/carousel/main-pic-30.jpg'),
        array('title' => 'СОЦИАЛЬНО-ЭКОНОМИЧЕСКАЯ ЗАЩИТА', 'cat' => 3, 'img' => '/files/site/carousel/main-pic-6.jpg'),
        array('title' => '2020 — год Единства, Памяти и Славы', 'cat' => 21, 'img' => '/files/site/carousel/main-pic-xx-2020.png'),
        array('title' => '', 'cat' => 21, 'img' => '/files/site/carousel/main-pic-banner-2.jpg'),
    );

    public function run()
    {
        shuffle($this->imgs);
        $this->render('headerSlideShow', array(
            'imgs' => $this->imgs,
        ));
    }

    //put your code here
}

?>
