<?php

Yii::import('zii.widgets.CPortlet');

class MenuPortlet extends CPortlet {

    public $useCache = true;
    public $cacheDuration = 3600;

    public function init() {
        $this->title = 'Разделы';
        parent::init();
    }

    protected function renderContent() {
        $this->widget('ext.widgets.mlmenu.MultiLevelMenu', array(
            'items' => $this->getMenuContent(),
            'id' => 'side-menu',
            'submenuHtmlOptions' => array('class' => 'leaf'),
            'actionPrefix' => 'Post',
            'activateParents' => true,
        ));
    }

    protected function getMenuContent() {
        if ($this->useCache) {
            $cash_id = __CLASS__ . 'menuItems';
            $menu = Yii::app()->cache->get($cash_id);
            if ($menu === false) {
                // обновляем $value, т.к. переменная не найдена в кэше,
                // и сохраняем в кэш для дальнейшего использования:
                $menu = Category::model()->getMenuItems(1, false);
                Yii::app()->cache->set($cash_id, $menu, $this->cacheDuration);
            }
        } else {
            $menu = Category::model()->getMenuItems(1, false);
        }
        return $menu;
    }

}