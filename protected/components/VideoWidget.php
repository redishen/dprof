<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 *   'srcRelative'=>'/files/videos/1.avi',//OR
     'src' => 'http://dprof.lo/files/videos/1.flv', //OR
     'src' => 'http://www.youtube.com/watch?v=yQGAedJcdlI',
 */

/**
 * Description of VideoWidget
 *
 * @author Root
 * @property Video $videoModel
 */
class VideoWidget extends CWidget
{

    public $videoModel;


    public function getVideoId($url)
    {
        if (strpos($url, "http") === false) {
            $url = "http:" . $url;
        }
        if ($arUrl = parse_url($url)) {
            if ($arUrl["query"]) {
                $arQuery = array();
                parse_str($arUrl["query"], $arQuery);
                if ($arQuery["v"]) {
                    return $arQuery["v"];
                }
            }
            if ($arUrl["host"] == "youtu.be" && $arUrl["path"]) {
                $path = str_replace("/embed/", "", $arUrl["path"]);
                $path = str_replace("/", "", $path);

                return $path;
            }
        }

        return false;
    }

    public function getEmbedUrl($id)
    {
        return 'https://www.youtube.com/embed/' . $id . '?rel=0&amp;controls=2&amp;showinfo=0;autoplay=0;modestbranding=1';
    }


    public function run()
    {
        parent::run();
        $params = array(
            'width' => 480,
            'height' => 320,
        );
        if (!empty($this->videoModel->href) && !empty($this->videoModel->local)) {
            if (Yii::app()->params['isWeb']) {
                $params['src'] = $this->videoModel->href;
            } else {
                $params['src'] = $this->videoModel->local;
            }
            $src = $params['src'];
            $url = $this->getEmbedUrl($this->getVideoId($src));
            echo '<div class="embed-responsive embed-responsive-16by9">';
            echo '<iframe class="embed-responsive-item" src="' . $url . '"></iframe>';
            echo ' </div>';
//        $this->widget('application.extensions.smp.StrobeMediaPlayback', $params);
        }
    }

}

?>
