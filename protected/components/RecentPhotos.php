<?php

Yii::import('zii.widgets.CPortlet');

class RecentPhotos extends CPortlet {

    public $useCache = false;
    public $cacheDuration = 120;
    public $images;

    public function init() {
        $this->title = 'Последние фотографии';

        parent::init();
    }

    protected function renderContent() {
        echo '<div class="recentPhotosCarusel" >';
        echo '<div id="recentPhotos" >';
        foreach ($this->getWidgetContent() as $img) {
            //    echo '<a href="lorem.html" >';
            //   echo CHtml::image(Yii::app()->request->baseUrl . '/files/images/thumbs/' . $img->file_name, 'Фото');


            $this->widget('ext.lyiightbox.LyiightBox2', array(
                'thumbnail' => Yii::app()->request->baseUrl . '/files/images/thumbs/150x150/' . $img->file_name,
                'image' => Yii::app()->request->baseUrl . '/files/images/' . $img->file_name,
                'title' => $img->post->title,
            ));


            //   echo '</a>';
        }
        echo '</div>';
        echo '<a class="prev" id="foo2_prev" href="#"><span>prev</span></a>';
        echo '<a class="next" id="foo2_next" href="#"><span>next</span></a>';
//        echo '<div class="pagination" ></div>';

        echo '<div class="clear"></div>';
        echo '</div>';


        $this->widget('ext.carouFredSel.ECarouFredSel', array(
            'id' => 'carousel',
            'target' => '#recentPhotos',
            'config' => array(
                'circular' => true,
//                'pagination' => array(
//                    'container' => '.pagination',
////                    'anchorBuilder' => new CJavaScriptExpression("function( nr ) {
////			}"),
//                ),
                'mousewheel' => true,
                'direction' => 'left',
//                'width' => 450,
//                'height' => 180,
//                'items' => 3,
                'items' => array(
                    'visible' => 3,
//                    'minimum' => 3,
//                    'width' => 150,
//                    'height' => 150,
                ),
                'prev' => array(
                    'button' => '#foo2_prev',
                    'key' => 'right',
                ),
                'next' => array(
                    'button' => '#foo2_next',
                    'key' => 'left',
                ),
                'scroll' => array(
                    'items' => 3,
                    'easing' => 'swing',
                    'fx' => 'slide',
//                        'duration' => 800,
//                        'pauseDuration' => 1500,
//                        'pauseOnHover' => false,
//                        'fx' => 'crossfade',
                ),
            ),
        ));
    }

    protected function getWidgetContent() {
        if ($this->useCache) {
            $cash_id = __CLASS__ . 'images';
            $images = Yii::app()->cache->get($cash_id);
            if ($images === false) {
                // обновляем $value, т.к. переменная не найдена в кэше,
                // и сохраняем в кэш для дальнейшего использования:
                $images = $this->getImages();
                Yii::app()->cache->set($cash_id, $images, $this->cacheDuration);
            }
        } else {
            $images = $this->getImages();
        }
        return $images;
    }

    protected function getImages() {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time(),
            'order' => 'update_time DESC',
            'with' => 'postImages',
            'limit' => 20,
        ));
        $posts = Post::model()->findAll($criteria);
        $images = array();
        foreach ($posts as $post) {
            foreach ($post->postImages as $postImage) {
                $images[] = $postImage;
            }

        }
        return $images;
    }

}