<?php

Yii::import('zii.widgets.CPortlet');

class BannersWidget extends CWidget
{

    public function run()
    {
        echo '<div class="static-banners">';
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/dpz2.png', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'https://dorprf.ru/'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/rpg.jpg', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://rosprofzhel.rzd.ru'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/epb.png', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://rosprofzhel.rzd.ru/elbilet.php'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/fnpr.jpg', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://www.fnpr.ru'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/mfp.png', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://www.mtuf.ru/'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/press.jpg', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://mag-print.ru'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo CHtml::link(CHtml::image(Yii::app()->baseUrl . '/files/site/banners/fimida.jpg', '', array('class' => 'static-banners__image')), array('site/away', 'url' => 'http://online.mtuf.ru'), array('class' => 'static-banners__banner', 'target' => '_blank'));
        echo '</div>';
    }

}
