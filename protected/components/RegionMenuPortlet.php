<?php

Yii::import('zii.widgets.CPortlet');

class RegionMenuPortlet extends CPortlet {

    public $useCache = true;
    public $cacheDuration = 3600;

    public function init() {
        $this->title = 'Подразделения';
        parent::init();
    }

    protected function renderContent() {
        $this->widget('ext.widgets.mlmenu.MultiLevelMenu', array(
            'items' => self::getMenuContent(),
            'id' => 'side-menu',
            'submenuHtmlOptions' => array('class' => 'leaf'),
            'actionPrefix' => 'Post',
            'activateParents' => true,
        ));
    }

    static function getMenuContent() {
        $menu = array(
            array('label' => 'Московско-Курское РОП', 'url' => array('/post/index', 'tag' => 'Московско-Курское РОП')),
            array('label' => 'Рязанское РОП', 'url' => array('/post/index', 'tag' => 'Рязанское РОП')),
            array('label' => 'Московско-Смоленское РОП', 'url' => array('/post/index', 'tag' => 'Московско-Смоленское РОП')),
            array('label' => 'Тульское РОП', 'url' => array('/post/index', 'tag' => 'Тульское РОП')),
            array('label' => 'Орловско-Курское РОП', 'url' => array('/post/index', 'tag' => 'Орловско-Курское РОП')),
            array('label' => 'Смоленское РОП', 'url' => array('/post/index', 'tag' => 'Смоленское РОП')),
            array('label' => 'Брянское РОП', 'url' => array('/post/index', 'tag' => 'Брянское РОП')),
//            array('label' => 'ДРП и ДПМ', 'url' => array('/post/index', 'tag' => 'ДРП и ДПМ')),
            array('label' => 'ЦППК', 'url' => array('/post/index', 'tag' => 'ЦППК')),
            array('label' => 'МФПК', 'url' => array('/post/index', 'tag' => 'МФПК')),
            array('label' => 'СППО РУТ (МИИТ)', 'url' => array('/post/index', 'tag' => 'МИИТ')),
        );
        return $menu;
    }

}
