<?php

Yii::import('zii.widgets.CPortlet');

class UserMenu extends CPortlet {

    public function init() {
        $this->title = 'Меню пользователя ' . CHtml::encode(Yii::app()->user->name);
        parent::init();
    }

    protected function renderContent() {
        $items = array(
            array('label' => 'Новая запись', 'url' => array('post/create')),
            array('label' => 'Записи', 'url' => array('post/admin')),
            array('label' => 'События', 'url' => array('event/admin')),
//            array('label'=>'Синхронизация', 'url'=>array('post/sync')),
            array('label' => 'Категории', 'url' => array('/category')),
            array('label' => 'Опросы', 'url' => array('/poll')),
            array('label' => 'Подтвердить комментарии' . ' (' . Comment::model()->pendingCommentCount . ')', 'url' => array('comment/index')),
            array('label' => 'Выход', 'url' => array('site/logout')),
        );

        $userMenu = $this->controller->menu;

        if (!empty($userMenu)) {
//            foreach ($userMenu as $label => $url) {
//                $userMenuItems[] = array('label' => $label, 'url' => $url);
//            }

            $this->widget('zii.widgets.CMenu', array(
                'items' => $userMenu,
            ));
        }

        $this->widget('zii.widgets.CMenu', array(
            'items' => $items,
        ));
    }

}
