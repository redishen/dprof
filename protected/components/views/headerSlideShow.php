<?php ?>

<div id="recentPhotosLogo">

    <?php foreach ($imgs as $img) : ?>
        <div class="slide">
            <?php echo CHtml::image(Yii::app()->baseUrl . $img['img']) ?>
            <div>
                <h4><?php echo $img['title'] ?></h4>
                <?php echo CHtml::link('подробнее', array('post/index', 'cat' => $img['cat'])); ?>
            </div>
        </div>
    <?php endforeach; ?>

</div>
<div class="clearfix"></div>

<?php
$this->widget('ext.carouFredSel.ECarouFredSel', array(
    'id' => 'carouselLogo',
    'target' => '#recentPhotosLogo',
    'config' => array(
        'items' => 1,
        'onCreate' => new CJavaScriptExpression('function(data) {
                                        data.items.find("div").slideDown();
                                        return data.items;
                                        }'),
        'scroll' => array(
            'fx' => 'cover',
            'duration' => 1500,
        ),
        'auto' => array(
            'onBefore' => new CJavaScriptExpression('function( data ) {
                                        data.items.old.find("div").slideUp();
                                        }'),
            'onAfter' => new CJavaScriptExpression('function( data ) {
                                        data.items.visible.find("div").slideDown();
                                        }'),
        // 'pauseOnHover' => true,
        ),
    ),
));
?>
