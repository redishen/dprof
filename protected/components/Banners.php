<?php

Yii::import('zii.widgets.CPortlet');

class Banners extends CPortlet {
    public $title = 'Полезные ссылки';

    public function init() {
        parent::init();
    }

    protected function renderContent() {
        $this->widget('BannersWidget');

    }

}