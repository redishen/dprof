var _center = {
    width: 200,
    height: 200,
    marginLeft: 0,
    marginTop: 0,
    marginRight: 0,
    opacity: 1
};
var _left = {
    width: 150,
    height: 150,
    marginLeft: 10,
    marginTop: 50,
    marginRight: 10,
    opacity: 0.5
};
var _right = {
    width: 150,
    height: 150,
    marginLeft: 10,
    marginTop: 50,
    marginRight: 0,
    opacity: 0.5
};
$('#carousel').carouFredSel({
    width: 690,
    height: 200,
    align: false,
    items: {
        visible: 3,
        width: 170
    },
    prev: {
        button: '#rp_prev',
        key: 'right'
    },
    next: {
        button: '#rp_next',
        key: 'left'
    },
    scroll: {
        pauseOnHover: true,
        items: 1,
        duration: 400,
        onBefore: function(data) {
            //  data.items.old.eq(0).animate(_outLeft);
            data.items.visible.eq(0).animate(_left);
            data.items.visible.eq(1).animate(_center);
            data.items.visible.eq(2).animate(_right);
            data.items.visible.eq(2).next().css(_right);
        }
    }
});
$('#carousel').children().eq(0).css(_left);
$('#carousel').children().eq(1).css(_center);
$('#carousel').children().eq(2).css(_right);
$('#carousel').children().eq(3).css(_right);