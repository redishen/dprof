<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of WeatherUpdate
 *
 * @author asfto_sheftelevichdm
 */
class update extends CAction {

    public $url = "http://api.worldweatheronline.com/free/v1/weather.ashx?q={city}&format=xml&num_of_days=1&date={date}&key=8hqu8pew4xedu6buax6c88zk";
    public $cities = array(
        'Moscow' => 'Москва',
        'Tula' => 'Тула',
        'Kaluga' => 'Калуга',
        'Bryansk' => 'Брянск',
        'Smolensk' => 'Смоленск',
        'Ryazan' => 'Рязань',
        'Orel' => 'Орёл',
    );
    public $useCache = true;
    public $cacheDuration = 3600;

    public function run() {
        if ($this->useCache) {
            $cache_id = __CLASS__ . 'waetherForecast' . '_' . $_GET['city'];
            $forecast = Yii::app()->cache->get($cache_id);
            if ($forecast === false) {
                // обновляем $value, т.к. переменная не найдена в кэше,
                // и сохраняем в кэш для дальнейшего использования:
                $forecast = $this->getForecast();
                Yii::app()->cache->set($cache_id, $forecast, $this->cacheDuration);
            }
        } else {
            $forecast = $this->getForecast();
        }
        echo CJSON::encode($forecast);
    }

    private function getForecast() {
        $url = str_replace('{city}', $_GET['city'], $this->url);
        $url = str_replace('{date}', date('Y-m-d'), $url);
        $xml = simplexml_load_file($url);
        if (!$xml) {
            echo 'error';
        }

        $cities = $this->cities;
        $res = array();
        $res['city'] = $cities[$_GET['city']];
        $res['temp'] = $xml->current_condition->temp_C . ' C';
        $res['icon'] = "{$xml->current_condition->weatherIconUrl}";
        $res['condition'] = $this->getConditionByCode((int)$xml->current_condition->weatherCode);
        return $res;
    }

    private function getConditionByCode($code) {
        $conditions = array(
            395 => "переменная облачность, снег",
            392 => "гроза, местами снег",
            389 => "гроза",
            386 => "гроза, местами дождь",
            377 => "дождь со снегом",
            374 => "возможен дождь со снегом",
            371 => "переменная облачность, снег",
            368 => "умеренный снегопад",
            365 => "снег с дождем",
            362 => "возможен дождь со снегом",
            359 => "проливной дождь",
            356 => "переменная облачность, дождь",
            353 => "небольшой дождь",
            350 => "снег",
            338 => "сильный снегопад",
            335 => "местами сильный снегопад",
            332 => "снегопад",
            329 => "местами снегопад",
            326 => "возможен небольшой снегопад",
            323 => "местами небольшой снегопад",
            320 => "дождь со снегом",
            317 => "небольшой дождь со снегом",
            314 => "ледяной дождь",
            311 => "возможен ледяной дождь",
            308 => "сильный дождь",
            305 => "временами сильный дождь",
            302 => "дождь",
            299 => "временами дождь",
            296 => "небольшой дождь",
            293 => "местами небольшой дождь",
            284 => "сильная изморозь",
            281 => "изморозь",
            266 => "небольшая изморозь",
            263 => "местами небольшая изморозь",
            260 => "изморозь, туман",
            248 => "туман",
            230 => "метель",
            227 => "сильная метель",
            200 => "возможна гроза",
            185 => "местами изморозь",
            182 => "местами снег с дождем",
            179 => "местами снегопад",
            176 => "местами дождь",
            143 => "пасмурно, туман",
            122 => "пасмурно",
            119 => "облачно",
            116 => "пременная облачность",
            113 => "ясно",
        );
        return $conditions[$code];
    }

}

?>
