<?php

class WeatherWidget extends CWidget {

    public $defaultCity = 'Moscow';
    public $useCache = false;
    public $cities = array(
        'Moscow' => 'Москва',
        'Tula' => 'Тула',
        'Kaluga' => 'Калуга',
        'Bryansk' => 'Брянск',
        'Smolensk' => 'Смоленск',
        'Ryazan' => 'Рязань',
        'Orel' => 'Орёл',
    );
//    public $url = "http://rp5.ru/xml/{code}/00000/ru";
    public $url = "http://api.worldweatheronline.com/free/v1/weather.ashx?q={city}&format=xml&num_of_days=1&date={date}&key=8hqu8pew4xedu6buax6c88zk";
    public $cssFile = 'style.css';

    protected function registerClientScript() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);

        Yii::app()->clientScript->registerCssFile($baseUrl . '/' . $this->cssFile);
        Yii::app()->clientScript->registerCoreScript('jquery');
    }

    public static function actions() {
        return array(
            'update' => 'ext.weather.actions.update',
        );
    }

    public function run() {
        $this->registerClientScript();
        $this->render('main', array(
            'cities' => $this->cities,
            'defaultCity' => $this->defaultCity,
        ));
    }

}

?>
