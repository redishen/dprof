<?php
Yii::app()->clientScript->registerScript('weather', '$("#weather-title a").click(
                function() {
                    $("#weather-select").slideToggle();
                }
        );');

Yii::app()->clientScript->registerScript('weather_update', CHtml::ajax(array(
            'type' => 'POST',
            'dataType' => 'json',
            'url' => array('weather.update', 'city' => $defaultCity),
            'success' => 'js:function(data)
                        {
                        $("#weather-city").html(data.city);
                        $("#weather-temp").html(data.temp);
                        $("#weather-icon").attr("src", data.icon);
                        $("#weather-condition").html(data.condition);
                        }'
)));
?>
<div class="weatherWidget">
    <div id="weather-title"  class="weather-block">
        <?php echo CHtml::link(' <span id="weather-city">Москва </span><span class="icon"></span>', ''); ?>
        <div id="weather-select">
            <ul>
                <?php foreach ($cities as $key => $city): ?>
                    <li> <?php
                        echo CHtml::ajaxLink($city, array('weather.update', 'city' => $key), array(
                            'dataType' => 'json',
                            'success' => 'js:function(data)
                        {
                        $("#weather-city").html(data.city);
                        $("#weather-temp").html(data.temp);
                        $("#weather-icon").attr("src", data.icon);
                        $("#weather-condition").html(data.condition);
                        }')
                        );
                        ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <p id="weather-content">сейчас</p>
    </div>
    <div id="weather-pic" class="weather-block">
        <?php echo CHtml::image('http://www.worldweatheronline.com/images/wsymbols01_png_64/wsymbol_0001_sunny.png', '', array('id' => 'weather-icon'));
        ?>

    </div>
    <div id="weather-right" class="weather-block">
        <p id="weather-temp"><?php echo '4' . ' C' ?></p>
        <p id="weather-condition"><?php echo 'ясно' ?></p>
    </div>



</div>

