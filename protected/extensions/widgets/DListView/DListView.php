<?php

Yii::import('zii.widgets.CListView');

/**
 * Description of DListView
 *
 * @author Root
 */
class DListView extends CListView {

    public $groupField = 'update_time';
    public $groupFieldClass = 'post-date';

    //put your code here
    /**
     * Renders the data item list.
     */
    public function renderItems() {
        echo CHtml::openTag($this->itemsTagName, array('class' => $this->itemsCssClass)) . "\n";
        $data = $this->dataProvider->getData();
        if (($n = count($data)) > 0) {
            $owner = $this->getOwner();
            $viewFile = $owner->getViewFile($this->itemView);
            $j = 0;
            $date = NULL;
            foreach ($data as $i => $item) {
                if ($date != date("j F Y", $item->{$this->groupField})) {
                    $date = date("j F Y", $item->{$this->groupField});
                    echo "<div class={$this->groupFieldClass}>" .  $this->rusdate($item->{$this->groupField}, 'j %MONTH% Y' ) . '</div>';
                }

                $data = $this->viewData;
                $data['index'] = $i;
                $data['data'] = $item;
                $data['widget'] = $this;
                $owner->renderFile($viewFile, $data);
                if ($j++ < $n - 1)
                    echo $this->separator;
            }
        }
        else
            $this->renderEmptyText();
        echo CHtml::closeTag($this->itemsTagName);
    }

    protected function rusdate($d, $format = 'j %MONTH% Y', $offset = 0) {
        $montharr = array('января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');
        $dayarr = array('понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота', 'воскресенье');

        $d += 3600 * $offset;

        $sarr = array('/%MONTH%/i', '/%DAYWEEK%/i');
        $rarr = array($montharr[date("m", $d) - 1], $dayarr[date("N", $d) - 1]);

        $format = preg_replace($sarr, $rarr, $format);
        return date($format, $d);
    }

}

?>
