<?php

Yii::import('zii.widgets.CWidget');

class RecentPhotos extends CWidget {

    public $useCache = true;
    public $cacheDuration = 120;
    public $images;
    public $cssFile = 'style.css';

    public function init() {
        parent::init();
        $this->registerClientScript();
    }

    protected function registerClientScript() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendors';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);

        Yii::app()->clientScript->registerCoreScript('jquery');

        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/' . $this->cssFile);
//        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/' . 'jquery-ui-1.10.1.custom.min.js', CClientScript::POS_HEAD);
    }

    public function run() {
        parent::run();
//        Yii::app()->clientScript->registerScript(__CLASS__ . 'hide' ,'$(".recentPhotosCarusel").hide()');
        $this->render('main', array('images' => $this->getWidgetContent()));
//        Yii::app()->clientScript->registerScript(__CLASS__ . 'show' ,'$(".recentPhotosCarusel").show()');
    }

    protected function getWidgetContent() {
        if ($this->useCache) {
            $cash_id = __CLASS__ . 'images';
            $images = Yii::app()->cache->get($cash_id);
            if ($images === false) {
                // обновляем $value, т.к. переменная не найдена в кэше,
                // и сохраняем в кэш для дальнейшего использования:
                $images = $this->getImages();
                Yii::app()->cache->set($cash_id, $images, $this->cacheDuration);
            }
        } else {
            $images = $this->getImages();
        }
        return $images;
    }

    protected function getImages() {
        return BDImage::model()->findAll(array(
                    'select' => 'file_name, post.title',
                    'order' => 'file_name desc',
                    'with' => 'post',
                    'limit' => 9));
        ;
    }

}