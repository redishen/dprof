<div class="recentPhotosCarusel" >
    <div id="recentPhotos" >
        <?php
        foreach ($images as $img) {
            $this->widget('ext.lyiightbox.LyiightBox2', array(
                'thumbnail' => Yii::app()->request->baseUrl . '/files/images/thumbs/150x150/' . $img->file_name,
                'image' => Yii::app()->request->baseUrl . '/files/images/' . $img->file_name,
                'title' => $img->post->title,
            ));
        }
        ?>
    </div>
    <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
    <a class="next" id="foo2_next" href="#"><span>next</span></a>
<!--    <div class="pagination" ></div>-->
    <div class="clear"></div>

</div>
<?php
$this->widget('ext.carouFredSel.ECarouFredSel', array(
    'id' => 'carousel',
    'target' => '#recentPhotos',
    'config' => array(
        'circular' => true,
//        'pagination' => array(
//            'container' => '.pagination',
//        ),
        'mousewheel' => true,
        'direction' => 'left',
        'items' => array(
            'visible' => 3,
        ),
        'prev' => array(
            'button' => '#foo2_prev',
            'key' => 'right',
        ),
        'next' => array(
            'button' => '#foo2_next',
            'key' => 'left',
        ),
        'scroll' => array(
            'items' => 3,
            'easing' => 'swing',
            'fx' => 'slide',
        ),
//        'auto' => false,
    ),
));
?>
