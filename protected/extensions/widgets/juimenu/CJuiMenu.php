<?php

Yii::import('zii.widgets.CMenu');

class CJuiMenu extends CMenu {

    public $cssFile = 'jquery-ui-1.10.1.custom.min.css';
    public $options;

    public function init() {
        parent::init();
        $this->registerClientScript();
    }

    /**
     * Calls {@link renderMenu} to render the menu.
     */
    public function run() {
        $this->htmlOptions['style'] = 'display: none';
        parent::run();
        $this->registerSrc();
    }

    public function registerSrc() {
        $id = $this->getId();
        $options = CJavaScript::encode($this->options);
        $js = "jQuery('#{$id}').menu($options);
            jQuery('#{$id}').slideDown(500);
            ";
        $cs = Yii::app()->getClientScript();
        $cs->registerScript(__CLASS__ . '#' . $id, $js);
    }

    protected function registerClientScript() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendors';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);

        Yii::app()->clientScript->registerCoreScript('jquery');
        Yii::app()->clientScript->registerCssFile($baseUrl . '/css/redmond/' . $this->cssFile);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/' . 'jquery-ui-1.10.1.custom.min.js', CClientScript::POS_HEAD);
    }

}
