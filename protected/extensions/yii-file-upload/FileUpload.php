<?php

Yii::import('zii.widgets.jui.CJuiInputWidget');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FileUpload
 *
 * @author asfto_sheftelevichdm
 */
class FileUpload extends CJuiInputWidget {

    /**
     * the url to the upload handler
     * @var string
     */
    public $url;

    /**
     * set to true to use multiple file upload
     * @var boolean
     */
    public $multiple = false;

    /**
     * The upload template id to display files available for upload
     * defaults to null, meaning using the built-in template
     */
    public $uploadTemplate;

    /**
     * The template id to display files available for download
     * defaults to null, meaning using the built-in template
     */
    public $downloadTemplate;

    /**
     * Wheter or not to preview image files before upload
     */
    public $previewImages = true;

    /**
     * Wheter or not to add the image processing pluing
     */
    public $imageProcessing = true;

    /**
     * set to true to auto Uploading Files
     * @var boolean
     */
    public $autoUpload = false;

    /**
     * @var string name of the form view to be rendered
     */
    public $formView = 'form';

    /**
     * @var string name of the upload view to be rendered
     */
    public $uploadView = 'upload';

    /**
     * @var string name of the download view to be rendered
     */
    public $downloadView = 'download';

    /**
     * @var bool whether form tag should be used at widget
     */
    public $showForm = true;

    /**
     * Publishes the required assets
     */
    public function init() {
        parent::init();
        $this->publishAssets();
    }

    /**
     * Generates the required HTML and Javascript
     */
    public function run() {

        list($name, $id) = $this->resolveNameID();

        $model = $this->model;

        if ($this->uploadTemplate === null) {
            $this->uploadTemplate = "#template-upload";
        }
        if ($this->downloadTemplate === null) {
            $this->downloadTemplate = "#template-download";
        }
        $this->render($this->uploadView);
        $this->render($this->downloadView);
        
        if (!isset($this->htmlOptions['enctype'])) {
            $this->htmlOptions['enctype'] = 'multipart/form-data';
        }

        if (!isset($this->htmlOptions['id'])) {
            $this->htmlOptions['id'] = $id;
        }
        $this->options['url'] = $this->url;
        $this->options['autoUpload'] = $this->autoUpload;

        if (!$this->multiple) {
            $this->options['maxNumberOfFiles'] = 1;
        }

        $this->options['dataType'] = 'json';

        $this->options['fileInput'] = '#XUploadForm_' . $this->attribute;

        $this->options['downloadTemplateId'] = null;
        $this->options['uploadTemplateId'] = null;

        $this->options['uploadTemplate'] =
            new CJavaScriptExpression('function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $(\'<tr class="template-upload">\' +
                    \'<td><span class="preview"></span></td>\' +
                    \'<td><p class="name"></p>\' +
                    \'<div class="error"></div>\' +
                    \'</td>\' +
                    \'<td><p class="size"></p>\' +
                    \'<div class="progress"></div>\' +
                    \'</td>\' +
                    \'<td class="text-right">\' +
                    (!index && !o.options.autoUpload ?
                        \'<button class="start btn btn-primary" disabled>Старт</button>\' : \'\') +
                    (!index ? \'<button class="cancel btn btn-default">Отмена</button>\' : \'\') +
                    \'</td>\' +
                    \'</tr>\');
                row.find(\'.name\').text(file.name);
                row.find(\'.size\').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find(\'.error\').text(file.error);
                }
                rows = rows.add(row);
            });
            return rows;
        }');


        $this->options['downloadTemplate'] =
            new CJavaScriptExpression('function (o) {
        var rows = $();
        $.each(o.files, function (index, file) {
            var row = $(\'<tr class="template-download">\' +
                \'<td><span class="preview"></span></td>\' +
                \'<td><p class="name"></p>\' +
                (file.error ? \'<div class="error"></div>\' : \'\') +
                \'</td>\' +
                \'<td><span class="size"></span></td>\' +
                \'<td class="text-right"><button class="delete btn btn-danger">Удалить</button></td>\' +
                \'</tr>\');
            row.find(\'.size\').text(o.formatFileSize(file.size));
            if (file.error) {
                row.find(\'.name\').text(file.name);
                row.find(\'.error\').text(file.error);
            } else {
                row.find(\'.name\').append($(\'<a></a>\').text(file.name));
                if (file.thumbnailUrl) {
                    row.find(\'.preview\').append(
                        $(\'<a></a>\').append(
                            $(\'<img class="img-responsive">\').prop(\'src\', file.thumbnailUrl)
                        )
                    );
                }
                row.find(\'a\')
                    .attr(\'data-gallery\', \'\')
                    .prop(\'target\', \'_blank\')
                    .prop(\'href\', file.url);
                row.find(\'button.delete\')
                    .attr(\'data-type\', file.delete_type)
                    .attr(\'data-url\', file.delete_url);
            }
            rows = rows.add(row);
        });
        return rows;
    }');

        $options = CJavaScript::encode($this->options);

        $cs = Yii::app()->clientScript;
        $cs->registerScript(__CLASS__ . '#' . $this->htmlOptions['id'], "jQuery('#{$this->htmlOptions['id']}').fileupload({$options});", CClientScript::POS_READY);

        $htmlOptions = array();
        if ($this->multiple) {
            $htmlOptions["multiple"] = true;
        }

        $this->render($this->formView, compact('htmlOptions'));
    }

    /**
     * Publises and registers the required CSS and Javascript
     * @throws CHttpException if the assets folder was not found
     */
    public function publishAssets() {
        $assets = dirname(__FILE__) . '/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        if (is_dir($assets)) {
            $cs = Yii::app()->clientScript;

            $cs->registerCssFile($baseUrl . '/css/jquery.fileupload.css');
            $cs->registerCoreScript('jquery');

            $cs->registerScriptFile($baseUrl . '/js/tmpl.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/load-image.all.min.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/vendor/jquery.ui.widget.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-process.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-jquery-ui.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-ui.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-validate.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.iframe-transport.js', CClientScript::POS_END);
            if ($this->imageProcessing) {
                $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-image.js', CClientScript::POS_END);
            }
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-video.js', CClientScript::POS_END);
            $cs->registerScriptFile($baseUrl . '/js/jquery.fileupload-audio.js', CClientScript::POS_END);

//            //The localization script
//            $messages = CJavaScript::encode(array(
//                        'fileupload' => array(
//                            'errors' => array(
//                                "maxFileSize" => $this->t('File is too big'),
//                                "minFileSize" => $this->t('File is too small'),
//                                "acceptFileTypes" => $this->t('Filetype not allowed'),
//                                "maxNumberOfFiles" => $this->t('Max number of files exceeded'),
//                                "uploadedBytes" => $this->t('Uploaded bytes exceed file size'),
//                                "emptyResult" => $this->t('Empty file upload result'),
//                            ),
//                            'error' => $this->t('Error'),
//                            'start' => $this->t('Start'),
//                            'cancel' => $this->t('Cancel'),
//                            'destroy' => $this->t('Delete'),
//                        ),
//            ));
//            $js = "window.locale = {$messages}";
//
//            Yii::app()->clientScript->registerScript('XuploadI18N', $js, CClientScript::POS_END);
            /**
              <!-- The XDomainRequest Transport is included for cross-domain file deletion for IE8+ -->
              <!--[if gte IE 8]><script src="<?php echo Yii::app()->baseUrl; ?>/js/cors/jquery.xdr-transport.js"></script><![endif]-->
             *
             */
        } else {
            throw new CHttpException(500, __CLASS__ . ' - Error: Couldn\'t find assets to publish.');
        }
    }

//    protected function t($message, $params = array()) {
//        return Yii::t('xupload.widget', $message, $params);
//    }
}
