<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of JGowl
 *
 * @author asfto_sheftelevichdm
 */
class JGrowl extends CWidget {

    public $isActive = TRUE;
    public $baseScriptUrl;
    public $options = array();

    public function init() {
        if ($this->baseScriptUrl === null) {
            $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'assets';
            $this->baseScriptUrl = Yii::app()->getAssetManager()->publish($assets);
        }
    }

    public function run() {
        if ($this->isActive) {
            $this->registerClientScript();
        }
    }

    protected function registerClientScript() {
        $options = CJavaScript::encode($this->options);
        $cs = Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerCssFile($this->baseScriptUrl . '/jquery.jgrowl.min.css');
        $cs->registerScriptFile($this->baseScriptUrl . '/jquery.jgrowl.min.js', CClientScript::POS_HEAD);
        $cs->registerScript(__CLASS__ . '#', "jQuery.extend(jQuery.jGrowl.defaults,$options);");
    }

}

?>