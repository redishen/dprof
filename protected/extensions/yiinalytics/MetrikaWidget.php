<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MetrikaWidget
 *
 * @author asfto_sheftelevichdm
 */
class MetrikaWidget extends CWidget {

    public $isActive = TRUE;

    public function run() {
        if ($this->isActive) {
            $this->registerClientScript();
            echo '<noscript><div><img src="//mc.yandex.ru/watch/24172165" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->';
        }
    }

    protected function registerClientScript() {
        $assets = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'vendors';
        $baseUrl = Yii::app()->getAssetManager()->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl . '/js/' . 'metrikaScript.js', CClientScript::POS_END);
    }

}

?>
