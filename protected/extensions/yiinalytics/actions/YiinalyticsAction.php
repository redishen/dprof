<?php

class YiinalyticsAction extends CAction {

    public $metrikaId;
    public $piwikId;
    public $piwikUrl;
    public $engine;
    public $cachePeriod;
    public $cacheId;

    protected function init() {
        /* @var $component Yiinalytics */
        $component = Yii::app()->yiinalytics;
        if (!$component) {
            throw new Exception('yiinalytics component is not set', 500);
        }
        $this->import($component);
    }

    protected function import(Yiinalytics $object) {
        foreach (get_object_vars($object) as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function run() {
        $this->init();
        switch ($this->engine):
            case 'metrika':
                $this->runMetrika();
                break;
            case 'piwik':
                $this->runPiwik();
                break;
            default :
                throw new Exception("'engine' should be set. (metrika or piwik)");
        endswitch;
    }

    protected function runMetrika() {
        
    }

    //http://10.23.250.9/piwik/index.php?module=API&method=VisitsSummary.getVisits&idSite=1&date=today&period=year&format=php&filter_limit=10
    protected function runPiwik() {
        $params = array(
                'pageUrl' => 'http://dprof.msk.oao.rzd/index.php',
                 'period' => 'year',
                'date' => 'today',
            );
        $p = $this->queryPiwik('Actions.getPageUrl', $params);
        var_dump($p);
        exit();
        
        $yaearVisitsCacheId = $this->cacheId . '_piwikYearVisits';
        $monthVisits = Yii::app()->cache->get($yaearVisitsCacheId);
        if ($monthVisits === false) {
            $params = array(
                'period' => 'month',
                'date' => 'today',
            );
            $monthVisits = $this->queryPiwik('VisitsSummary.getVisits', $params);
            Yii::app()->cache->set($yaearVisitsCacheId, $monthVisits, $this->cachePeriod);
        }

        $dayVisitsCacheId = $this->cacheId . '_piwikDayVisits';
        $dayVisits = Yii::app()->cache->get($dayVisitsCacheId);
        if ($dayVisits === false) {
            $params = array(
                'period' => 'day',
                'date' => 'today',
            );
            $dayVisits = $this->queryPiwik('VisitsSummary.getVisits', $params);
            Yii::app()->cache->set($dayVisitsCacheId, $dayVisits, $this->cachePeriod);
        }


        $onlineVisitsCacheId = $this->cacheId . '_piwikOnlineVisits';
        $onlineVisits = Yii::app()->cache->get($onlineVisitsCacheId);
        if ($onlineVisits === false) {
            $params = array(
                'lastMinutes' => 30,
            );
            $onlineVisits = $this->queryPiwik('Live.getCounters', $params);
            $onlineVisits = $onlineVisits[0]['visitors'];
            Yii::app()->cache->set($onlineVisitsCacheId, $onlineVisits, $this->cachePeriod);
        }

        echo json_encode(array(
            'month' => $monthVisits,
            'day' => $dayVisits,
            'online' => $onlineVisits,
        ));
    }

    protected function queryPiwik($method, $params) {
        $defaultParams = array(
            'module' => 'API',
            'idSite' => $this->piwikId,
            'method' => $method,
            'format' => 'PHP',
            'filter_limit' => 20,
            'token_auth' => 'anonymous',
        );

        $query = http_build_query(array_merge($defaultParams, $params));
        $url = "http:{$this->piwikUrl}?" . $query;
        $fetched = file_get_contents($url);
        $content = unserialize($fetched);

        // case error
        if (!$content) {
            throw new CHttpException(404);
        }
        return $content;
    }

}
