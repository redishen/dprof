<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Yiinalytics
 *
 * @author asfto_sheftelevichdm
 */
class YiinalyticsScript extends CWidget {

    public $metrikaScript = '<!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter{{metrikaId}} = new Ya.Metrika({ id:{{metrikaId}}, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/{{metrikaId}}" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->';
    public $metrikaId;
    public $piwikScript = '<!-- Piwik --><script type="text/javascript">  var _paq = _paq || [];  _paq.push(["setCookieDomain", "*.{{piwikDomain}}"]);_paq.push([\'trackPageView\']);  _paq.push([\'enableLinkTracking\']);  (function() {    var u="{{piwikUrl}}";    _paq.push([\'setTrackerUrl\', u+\'piwik.php\']);    _paq.push([\'setSiteId\', {{piwikId}}]);    var d=document, g=d.createElement(\'script\'), s=d.getElementsByTagName(\'script\')[0];    g.type=\'text/javascript\'; g.async=true; g.defer=true; g.src=u+\'piwik.js\'; s.parentNode.insertBefore(g,s);  })();</script><noscript><p><img src="{{piwikUrl}}piwik.php?idsite={{piwikId}}" style="border:0;" alt="" /></p></noscript><!-- End Piwik Code -->';
    public $piwikId;
    public $piwikDomain;
    public $piwikUrl;
    public $engine;

    public function init() {
        /* @var $component Yiinalytics */
        $component = Yii::app()->yiinalytics;
        if (!$component) {
            throw new Exception('yiinalytics component is not set', 500);
        }
        $this->import($component);
    }

    protected function import(Yiinalytics $object) {
        foreach (get_object_vars($object) as $key => $value) {
            if (property_exists($this, $key)) {
                $this->$key = $value;
            }
        }
    }

    public function run() {
        switch ($this->engine):
            case 'metrika':
                $this->registerMetrika();
                break;
            case 'piwik':
                $this->registerPiwik();
                break;
            default :
                throw new Exception("'engine' should be set. (metrika or piwik)");
        endswitch;
    }

    public function registerPiwik() {
        $script = $this->placeParams(array('piwikDomain', 'piwikUrl', 'piwikId'), $this->piwikScript);
        echo $script;
    }

    public function registerMetrika() {
        $script = $this->placeParams(array('metrikaId'), $this->metrikaScript);
        echo $script;
    }

    public function placeParams($vars, $string) {
        $params = array();
        foreach ($vars as $var) {
            if (empty($this->$var)) {
                throw new Exception("'$var' should be set");
            }
            $params['{{' . $var . '}}'] = $this->$var;
        }
        $str = str_replace(array_keys($params), array_values($params), $string);
        return $str;
    }

}
