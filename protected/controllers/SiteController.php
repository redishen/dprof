<?php

use ReCaptcha\ReCaptcha;

class SiteController extends Controller
{

    public $layout = 'column1';

    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array(
            'yiinalytics' => array(
                'class' => 'yiinltcs.actions.YiinalyticsAction',
            ),
            'weather.' => 'ext.weather.widget.WeatherWidget',
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
            'special' => array(
                'class' => 'CViewAction',
                'basePath' => 'special',
            ),

        );
    }

    public function actionAway($url)
    {
        if (Yii::app()->params['isWeb']) {
            $this->redirect($url);
        } else {
            $this->render('away', array('url' => $url));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionGetCounters()
    {
        $counter = Yii::app()->SCounter;
        $counters = array();
        foreach (array('all', 'today', 'online') as $counterType) {
            $cash_id = __CLASS__ . 'counters_' . $counterType;
            $val = Yii::app()->cache->get($cash_id);
            if ($val === false) {
                $val = $counter->{'get' . ucfirst($counterType)}();
                Yii::app()->cache->set($cash_id, $val, 60 * 3);
            }
            $counters[$counterType] = $val;
        }

        echo CJSON::encode($counters);
    }

    public function actionIndex()
    {
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED . ' AND on_main = 1 AND update_time < ' . time(),
            'order' => 'update_time DESC',
            'with' => array('postVideos', 'commentCount'),
            'limit' => 25,
        ));
        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => false,
            'criteria' => $criteria,
        ));


        /* @var $posts PostCategory */
        $posts = PostCategory::model()->findAllByAttributes(array('category_id' => 20));
        $ids = CHtml::listData($posts, 'post_id', 'post_id');

        $profHelpCr = new CDbCriteria(array(
            'limit' => 4,
            'order' => 'update_time DESC'
        ));

        $profHelpCr->addInCondition('id', $ids);
        $profHelpCr->addInCondition('status', array(
            Post::STATUS_PUBLISHED,
            Post::STATUS_ARCHIVED,
            Post::STATUS_IMPORTANT
        ));

        $profHelpDP = new CActiveDataProvider('Post', array(
            'pagination' => false,
            'criteria' => $profHelpCr,
        ));

        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'profHelpDP' => $profHelpDP,
        ));
    }

    public function actionAbout($page = NULL)
    {
        if (!isset($page)) {
            $this->render('about');
        } else {
            $this->renderPartial('_about', array('page' => $page), true);
        }
    }

    public function actionContact()
    {
        if (!Yii::app()->params['isWeb']) $this->redirect(['site/away', 'url' => 'http://dprof.ru/site/contact']);

        $this->layout = 'column1-bare';
        $siteKey = Yii::app()->params['siteKey'];

        $model = new ContactForm();
        $success = false;

        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $recaptcha = new ReCaptcha($siteKey['private']);
                $resp = $recaptcha
                    ->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

                if ($resp->isSuccess()) {
                    if ($model->send()) {
                        $success = true;
                    }
                } else {
                    $model->addError('captcha_code', 'Ошибка при проверке на робота, повторите отправку');
                }
            }
        }

        $this->render('contact', [
            'publicKey' => $siteKey['public'],
            'model' => $model,
            'success' => $success
        ]);
    }

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $model = new LoginForm();
        // if it is ajax validation request
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login())
                $this->redirect(Yii::app()->user->returnUrl);
        }
        // display the login form
        $this->render('login', array('model' => $model));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }
}
