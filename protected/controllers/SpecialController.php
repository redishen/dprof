<?php

use ReCaptcha\ReCaptcha;

class SpecialController extends Controller
{

    public $layout = 'column1';

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('polk2019', 'polk2019View', 'reso'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionReso()
    {
        if (!Yii::app()->params['isWeb']) $this->redirect(['site/away', 'url' => 'http://dprof.ru/special/reso']);

        $this->layout = 'column1-bare';
        $siteKey = Yii::app()->params['siteKey'];

        $model = new ResoForm();
        $success = false;

        if (isset($_POST['ResoForm'])) {
            $model->attributes = $_POST['ResoForm'];
            if ($model->validate()) {
                $recaptcha = new ReCaptcha($siteKey['private']);
                $resp = $recaptcha
                    ->verify($_POST['g-recaptcha-response'], $_SERVER['REMOTE_ADDR']);

                if ($resp->isSuccess()) {
                    if ($model->send()) {
                        $success = true;
                    }
                } else {
                    $model->addError('captcha_code', 'Ошибка при проверке на робота, повторите отправку');
                }
            }
        }

        $this->render('reso', [
            'publicKey' => $siteKey['public'],
            'model' => $model,
            'success' => $success
        ]);
    }

    public function actionPolk2019()
    {
        $this->layout = 'column1-bare';

        $criteria = new CDbCriteria([
            'condition' => 'status=' . Post::STATUS_HIDDEN . ' AND update_time < ' . time(),
            'order' => 'title ASC',
        ]);
        $criteria->addSearchCondition('tags', 'Проект «Бессмертный полк» — Карточка');

        $posts = Polk2019Post::model()->findAll($criteria);
        $images = [];
        foreach ($posts as $post) {
            if($post->postImages[0]) {
                $images[] = $post->postImages[0]->getUrl();
            }
        }

        $this->render('polk2019', [
            'posts' => $posts,
            'images' => $images,
        ]);
    }

    /**
     * Displays a particular model.
     */
    public function actionPolk2019View()
    {
        $this->layout = 'column1';
        $post = Post::model()->findByPk($_GET['id']);
        $post->updateViewCount();

        $this->render('polk2019View', array(
            'model' => $post,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionPolk2019Create()
    {
        $this->layout = 'bs_column1';

        $model = new Polk2019Post('parent');
//        $this->performAjaxValidation($model);

        $model->update_time = date('Y-m-d H:i', strtotime('-3 days'));

        Yii::import("xupload.models.XUploadForm");
        $images = new XUploadForm();

        if (isset($_POST['Polk2019Post'])) {
            $model->attributes = $_POST['Polk2019Post'];
            if ($model->save()) {
                $this->redirect('/post/admin');
            } else {
                throw new Exception('Ошибка сохранения поста');
            }
        } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {
            throw new Exception('Нет данных для сохранения');
        } else {
            $model->tags = 'Проект «Бессмертный полк»';
        }

        $this->render('polk2019Create', array(
            'model' => $model,
            'images' => $images,
        ));
    }

    public function actionUpload()
    {
        Yii::import("xupload.models.XUploadForm");
        //Here we define the paths where the files will be stored temporarily
        $imgPath = Yii::app()->params['imgPath'];
        $path = realpath(Yii::app()->getBasePath() . "/.." . $imgPath) . "/";
        $publicPath = Yii::app()->getBaseUrl() . $imgPath;

        header('Content-type: application/json');

        //Here we check if we are deleting and uploaded file
        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumb = $path . "thumbs/$thumbDem/" . $_GET["file"];
                        if (is_file($thumb)) {
                            unlink($thumb);
                        }
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');

            //We check that the file was successfully uploaded
            if ($model->file !== null) {
                $origName = $model->file->getName();
                //Grab some data
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                //(optional) Generate a random name for our file
                $filename = uniqid();
                $filename .= "." . $model->file->getExtensionName();
                $model->name = $filename;
                if ($model->validate()) {
                    $thumb = Yii::app()->phpThumb->create($model->file->getTempName());
                    $dem = explode('x', Yii::app()->params['imageSize']);
                    $thumb->resize($dem[0], $dem[1]);
                    $thumb->rotateImage('EXIF');
                    $thumb->save($path . $filename);
                    chmod($path . $filename, 0777);
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumbPath = $path . "thumbs/$thumbDem";
                        $thumb = Yii::app()->phpThumb->create($path . $filename);
                        $dem = explode('x', $thumbDem);
                        $thumb->adaptiveResize($dem[0], $dem[1]);
                        if (!is_dir($thumbPath)) {
                            mkdir($thumbPath);
                            chmod($thumbPath, 0777);
                        }
                        $thumb->save("$thumbPath/$filename");
                        unset($thumb);
                    }
                    if (Yii::app()->user->hasState('images')) {
                        $userImages = Yii::app()->user->getState('images');
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $publicPath,
                        //the same file or a thumb version that you generated
                        //  "thumb" => $path . "thumbs/".Yii::app()->params['thumbs'][0]."/$filename",
                        "filename" => $origName,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('images', $userImages);

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode(array('files' => array(array(
                        "name" => $publicPath . $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => $publicPath . $filename,
                        "thumbnailUrl" => $publicPath . "thumbs/" . Yii::app()->params['thumbs'][0] . "/$filename",
                        "deleteUrl" => $this->createUrl("upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        )),
                        "deleteType" => "POST"
                    ))));
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),
                        )));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    public function actionFlashmob9May($vote = null)
    {
        $this->redirect('flashmob9May17');
        $data = $this->getData();
        if ($vote) {
            if (preg_match('/flashmob9May/im', $_SERVER['HTTP_REFERER'])) {
//              $this->redirect(array('flashmob9May'));
                $this->layout = 'blank';
            } else {
                $this->layout = 'column1';

            }

            $this->render('_flashmob_vote', array(
                'id' => $vote,
                'name' => $data[$vote]['name'],
            ));
        } else {
            $this->render('flashmob', array(
                'data' => $data,
            ));

        }
    }

    public function actionFlashmob9May17($vote = null)
    {
        $data = $this->getData17();
        if ($vote) {
            $this->breadcrumbs = array('Голосование за лучший ролик флешмоб – эстафеты' => array('flashmob9may17'), $data[$vote]['name']);
            if (preg_match('/flashmob9may17/im', $_SERVER['HTTP_REFERER'])) {
//                $this->redirect(array('flashmob9May17'));
                $this->layout = 'blank';
            } else {
                $this->layout = 'column1';
            }

            $this->render('_flashmob_vote17', array(
                'id' => $vote,
                'name' => $data[$vote]['name'],
            ));
        } else {
            $this->render('flashmob17', array(
                'data' => $data,
            ));

        }
    }

    protected function getData()
    {
        return array(
            'miit' => array(
                'tube' => 'iwuTYfppMIM',
                'name' => 'МИИТ',
            ),
            'm-krsk' => array(
                'tube' => 'kTarARqD0qo',
                'name' => 'М-Курского региона',
            ),
            'm-smlnsk' => array(
                'tube' => '_4-3f8OAPO0',
                'name' => 'М-Смоленского региона',
            ),
            'tla' => array(
                'tube' => '8MX0-XoORL4',
                'name' => 'Тульского региона',
            ),
            'tla2' => array(
                'tube' => 'HEgA1qqtyv0',
                'name' => 'Тульского региона - 2',
            ),
            'rzn' => array(
                'tube' => 'kwxitLG7j4Q',
                'name' => 'Рязанского региона',
            ),
            'bksvo' => array(
                'tube' => 'SbTfeQuopdA',
                'name' => 'Бекасово',
            ),
            'krsk' => array(
                'tube' => 'gjoo_MiilbU',
                'name' => 'Курск',
            ),
            'smlnsk' => array(
                'tube' => '3DDlnrEn_qI',
                'name' => 'Смоленск',
            ),
            'klga' => array(
                'tube' => 'DEdDqGBwzkc',
                'name' => 'Калуга',
            ),
            'orl' => array(
                'tube' => 'VKa9eCZDb68',
                'name' => 'Орел',
            ),


        );
    }

    protected function getData17()
    {
        return array(
//            'miit' => array(
//                'tube' => 'iwuTYfppMIM',
//                'name' => 'МИИТ',
//            ),
            'm-krsk' => array(
                'tube' => 'LpanauTxbh8',
                'name' => 'Московско-Курского РОП',
            ),
            'rzn' => array(
                'tube' => 'jO4EbY-MU3A',
                'name' => 'Рязанского РОП',
            ),
            'm-smlnsk' => array(
                'tube' => 'SAgyRegw2wE',
                'name' => 'Московско-Смоленского РОП',
            ),
            'tla' => array(
                'tube' => 'qg41Pblz47A',
                'name' => 'Тульского РОП',
            ),
//            'tla2' => array(
//                'tube' => 'HEgA1qqtyv0',
//                'name' => 'Тульского региона - 2',
//            ),

//            'bksvo' => array(
//                'tube' => 'SbTfeQuopdA',
//                'name' => 'Бекасово',
//            ),
            'krsk' => array(
                'tube' => 'ASeFu5cj_r4',
                'name' => 'Орловско-Курского РОП',
            ),
            'smlnsk' => array(
                'tube' => 'JkbOQSLEawk',
                'name' => 'Смоленского РОП',
            ),
//            'klga' => array(
//                'tube' => 'DEdDqGBwzkc',
//                'name' => 'Калуга',
//            ),
            'brnsk' => array(
                'tube' => '_-2wFT5Ngk8',
                'name' => 'Брянского РОП',
            ),
//            'orl' => array(
//                'tube' => 'VKa9eCZDb68',
//                'name' => 'Орел',
//            ),


        );
    }


}
