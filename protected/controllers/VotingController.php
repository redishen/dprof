<?php

class VotingController extends Controller
{

    public $layout = 'column1';

    public $vid = 'day18';
    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    public function accessRules()
    {

        return array(
            array('allow',
                'actions' => array('flashmobDay18'),
                'users' => array('*'),
            ),
            array('allow',
                'actions' => array('vote'),
                'users' => array('@'),
            ),
//            array('allow', // allow admin user to perform 'admin' and 'delete' actions
//                'actions' => array('admin', 'delete'),
//                'users' => Yii::app()->getModule('user')->getAdmins(),
//            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionVote($vote)
    {

        $data = $this->getData18();
        $this->layout = 'blank';
        $voted = Voting::model()->exists('voting_id=:voting_id and user_id=:user_id and for=:for', array(
            ':voting_id' => $this->vid,
            ':for' => $vote,
            ':user_id' => Yii::app()->user->id,
        ));

        $status = '';
        $status = 'closed';

        if($voted || $status == 'closed') {
            $status = 'voted';
        } else {
            $model = new Voting();
            $model->voting_id = $this->vid;
            $model->for = $vote;
            $model->user_id = Yii::app()->user->id;
            $model->time = time();

            if($model->save()) {
//            if(true) {
                $status = 'success';
            } else {
                $status = 'error';
                throw new CException('vote save error');
            }

        }
        $status = 'closed';
        $this->render('_flashmob_vote18', array(
            'id' => $vote,
            'status' => $status,
            'name' => $data[$vote]['name'],
        ));
    }


    public function actionFlashmobDay18($vote = null)
    {
        $data = $this->getData18();
        if ($vote) {
            $this->breadcrumbs = array('Голосование за лучший ролик флешмоб – эстафеты' => array('flashmobDay18'), $data[$vote]['name']);
            if (preg_match('/flashmobDay18/im', $_SERVER['HTTP_REFERER'])) {
//                $this->redirect(array('flashmob9May18'));
                $this->layout = 'blank';
            } else {
                $this->layout = 'column1';
            }

            $this->render('_flashmob_vote18', array(
                'id' => $vote,
                'name' => $data[$vote]['name'],
            ));
        } else {
            $this->render('flashmob18', array(
                'data' => $data,
            ));

        }
    }


    protected function getData18()
    {
        return array(
//            'miit' => array(
//                'tube' => 'iwuTYfppMIM',
//                'name' => 'МИИТ',
//            ),
            'm-krsk' => array(
                'tube' => '_hY_6vIFW7M',
                'name' => 'Московско-Курского РОП',
            ),
            'rzn' => array(
                'tube' => 'bh0oaeE5OW8',
                'name' => 'Рязанского РОП',
            ),
            'm-smlnsk' => array(
                'tube' => 'C5xEOUvFGHA',
                'name' => 'Московско-Смоленского РОП',
            ),
            'd-sad' => array(
                'tube' => 'FYzerKTbbik',
                'name' => 'Детский сад № 37',
            ),
            'tla' => array(
                'tube' => 'b55MFk_ijWs',
                'name' => 'Тульского РОП',
            ),
//            'tla2' => array(
//                'tube' => 'HEgA1qqtyv0',
//                'name' => 'Тульского региона - 2',
//            ),

//            'bksvo' => array(
//                'tube' => 'SbTfeQuopdA',
//                'name' => 'Бекасово',
//            ),
            'krsk' => array(
                'tube' => '3AvX0boDcIg',
                'name' => 'Орловско-Курского РОП',
            ),
            'smlnsk' => array(
                'tube' => 'xkGA9LYnJeY',
                'name' => 'Смоленского РОП',
            ),
////            'klga' => array(
////                'tube' => 'DEdDqGBwzkc',
////                'name' => 'Калуга',
////            ),
            'rsslvl' => array(
                'tube' => '4g-3eRhT00I',
                'name' => 'Рославль',
            ),
            'brnsk' => array(
                'tube' => 'P0ORBCX0jjA',
                'name' => 'Брянского РОП',
            ),
//            'orl' => array(
//                'tube' => 'VKa9eCZDb68',
//                'name' => 'Орел',
//            ),


        );
    }


}
