<?php

class DocController extends Controller
{

    public $layout = 'column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'cat', 'search', 'download'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionUpdateCounts($site)
    {
        if (in_array($site, array('local', 'web'))) {
            $models = Post::model()->findAll();
            $counter = $site == 'local' ? 'view_count_local' : 'view_count_web';

            foreach ($models as $model) {
                $count = SCounter::getPostViews($model->id);
                $model->updateCounters(array($counter => $count), 'id = :id', array(':id' => $model->id));
            }
        }
    }

    public function actionUpload()
    {
        Yii::import("xupload.models.XUploadForm");
        $path = DBDocument::getPath();

        header('Content-type: application/json');

        //Here we check if we are deleting and uploaded file
        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');

            //We check that the file was successfully uploaded
            if ($model->file !== null) {
                //Grab some data
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                $model->name = $model->file->getName();
                //(optional) Generate a random name for our file
                $filename = uniqid();
                $model->filename = $filename;


                if ($model->validate()) {

                    rename($model->file->getTempName(), $path . $filename);

                    if (Yii::app()->user->hasState('docs')) {
                        $userDocs = Yii::app()->user->getState('docs');
                    } else {
                        $userDocs = array();
                    }

                    $userDocs[] = array(
                        'id' => $filename,
                        'file_name' => $model->name,
                        'type' => $model->file->getExtensionName(),
                        'size' => $model->size,
                        'mime_type' => $model->mime_type,
                    );
                    Yii::app()->user->setState('docs', $userDocs);

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode(array('files' => array(array(
                        "name" => $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => '#',
                        "deleteUrl" => $this->createUrl("upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        )),
                        "deleteType" => "POST"
                    ))));
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),
                        )));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    /**
     * Displays a particular model.
     */
    public function actionView()
    {
        $this->layout = 'column1';
        $post = $this->loadModel();
        $post->updateViewCount();

        $this->render('view', array(
            'model' => $post,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->layout = 'bs_column1';

        $model = new Post('doc');
        $this->performAjaxValidation($model);

        $model->update_time = date('Y-m-d H:i', strtotime('-3 days'));
        Yii::import("xupload.models.XUploadForm");
        $files = new XUploadForm;

        if (isset($_POST['Post'])) {
            $model->attributes = $_POST['Post'];

            if ($model->save()) {
                if (isset($_POST['Post']['categories'])) {
                    $this->saveCategories($_POST['Post']['categories'], $model->id);
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'files' => $files,
        ));
    }

    public function actionDownload($id)
    {
        $doc = DBDocument::model()->findByPk($id);
        if ($doc === null) {
            throw new CHttpException(404, 'Запрашиваемый файл не найден.');
        }
        
        $filePath = Yii::getPathOfAlias('webroot') . '/files/docs/' . $doc->id;

        if (!file_exists($filePath)) {
            throw new CHttpException(404, 'Запрашиваемый файл не найден.');
        }

        if (ob_get_level()) {
            ob_end_clean();
        }

        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Type: " . $doc->mime_type);
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Content-Disposition: attachment');
        header("Content-Transfer-Encoding: binary ");
        header('Content-Length: ' . $doc->size);
        readfile($filePath);
    }

    public function actionAjaxDeleteDoc($id = NULL)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $doc = DBDocument::model()->findByPk($id);
            if ($doc === null) {
                throw new CHttpException(404, 'Запрашиваемый файл не найден.');
            }
            if ($doc->delete()) {
                return TRUE;
            } else {
                throw new CException(666, 'Файл не удалился');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionAjaxRenameDoc($id, $name)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $doc = DBDocument::model()->findByPk($id);
            if ($doc === null) {
                throw new CHttpException(404, 'Запрашиваемый файл не найден.');
            }
            $doc->file_name = $name;
            if ($doc->save()) {
                return TRUE;
            } else {
                throw new CHttpException(500, 'Ошибка переименования');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate()
    {
        $this->layout = 'bs_column1';
        $model = $this->loadModel();
        $this->performAjaxValidation($model);

        $model->update_time = date('Y-m-d H:i', $model->update_time);
        Yii::import("xupload.models.XUploadForm");
        $files = new XUploadForm();

        if (isset($_POST['Post'])) {
            $model->attributes = $_POST['Post'];
            if ($model->save()) {
                if (isset($_POST['Post']['categories'])) {
                    $this->saveCategories($_POST['Post']['categories'], $model->id);
                }
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'files' => $files,
        ));
    }

    public function saveCategories($cat, $model_id)
    {
        PostCategory::model()->deleteAll("post_id = $model_id");
        $postCat = new PostCategory();
        $postCat->post_id = $model_id;
        $postCat->category_id = $cat;
        $postCat->save();
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete()
    {
        // we only allow deletion via POST request
        $this->loadModel()->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($cat = 72)
    {
        $perPage = Yii::app()->params['postsPerPage'];
        $viewName = 'index';

        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time(),
            'order' => 'update_time DESC',
            'with' => 'commentCount',
            'limit' => $perPage * 20,
        ));


        $viewCatName = 'cat/cat-' . $cat;
        if ($this->getViewFile($viewCatName)) {
            $viewName = $viewCatName;
        }

        $catModel = Category::model()->findByAttributes(array('id' => $cat));
        $cats = CHtml::listData(Category::model()->getAllChildren($catModel), 'id', 'id');
        $cats[] = $cat;
        $cr = new CDbCriteria();
        $cr->addInCondition('category_id', $cats);
        $posts = PostCategory::model()->findAll($cr);

        $ids = CHtml::listData($posts, 'post_id', 'post_id');
        $criteria->addInCondition('id', $ids);


        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => $perPage,
            ),
            'criteria' => $criteria,
        ));


        $this->render($viewName, array(
            'dataProvider' => $dataProvider,
            'catName' => $catModel->label,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->layout = 'bs_column1';
        $model = new Post('search');

        if (isset($_GET['Post']))
            $model->attributes = $_GET['Post'];


        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array())
                echo implode("\n", $tags);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel()
    {
        if ($this->_model === null) {
            if (isset($_GET['id'])) {
                if (Yii::app()->user->isGuest)
                    $condition = 'status=' . Post::STATUS_PUBLISHED . ' OR status=' . Post::STATUS_ARCHIVED . ' OR status=' . Post::STATUS_IMPORTANT;
                else
                    $condition = '';
                $this->_model = Post::model()->findByPk($_GET['id'], $condition);
                $this->_model->setScenario('doc');
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая запись не найдена.');
        }
        return $this->_model;
    }


    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'post-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
