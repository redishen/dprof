<?php

class PostController extends Controller
{

    public $layout = 'column2';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array(
                'allow', // allow all users to access 'index' and 'view' actions.
                'actions' => array('index', 'view', 'cat', 'search', 'getEvents'),
                'users' => array('*'),
            ),
            array(
                'allow', // allow authenticated users to access all actions
                'users' => Yii::app()->findModule('user') ? Yii::app()->findModule('user')->getAdmins() : array('siteadmin'),
            ),
            array(
                'deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionUpdateCounts($site)
    {
        if (in_array($site, array('local', 'web'))) {
            $models = Post::model()->findAll();
            $counter = $site == 'local' ? 'view_count_local' : 'view_count_web';

            foreach ($models as $model) {
                $count = SCounter::getPostViews($model->id);
                $model->updateCounters(array($counter => $count), 'id = :id', array(':id' => $model->id));
            }
        }
    }

    public function actionUpload()
    {
        Yii::import("xupload.models.XUploadForm");
        //Here we define the paths where the files will be stored temporarily
        $imgPath = Yii::app()->params['imgPath'];
        $path = realpath(Yii::app()->getBasePath() . "/.." . $imgPath) . "/";
        $publicPath = Yii::app()->getBaseUrl() . $imgPath;

        header('Content-type: application/json');

        //Here we check if we are deleting and uploaded file
        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumb = $path . "thumbs/$thumbDem/" . $_GET["file"];
                        if (is_file($thumb)) {
                            unlink($thumb);
                        }
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');

            //We check that the file was successfully uploaded
            if ($model->file !== null) {
                //Grab some data
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                //(optional) Generate a random name for our file
                $filename = uniqid();
                $filename .= "." . $model->file->getExtensionName();
                $model->name = $filename;
                if ($model->validate()) {
                    $thumb = Yii::app()->phpThumb->create($model->file->getTempName());
                    $dem = explode('x', Yii::app()->params['imageSize']);
                    $thumb->resize($dem[0], $dem[1]);
                    $thumb->rotateImage('EXIF');
                    $thumb->save($path . $filename);
                    chmod($path . $filename, 0777);
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumbPath = $path . "thumbs/$thumbDem";
                        $thumb = Yii::app()->phpThumb->create($path . $filename);
                        $dem = explode('x', $thumbDem);
                        $thumb->adaptiveResize($dem[0], $dem[1]);
                        if (!is_dir($thumbPath)) {
                            mkdir($thumbPath);
                            chmod($thumbPath, 0777);
                        }
                        $thumb->save("$thumbPath/$filename");
                        unset($thumb);
                    }
                    if (Yii::app()->user->hasState('images')) {
                        $userImages = Yii::app()->user->getState('images');
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $publicPath,
                        //the same file or a thumb version that you generated
                        //  "thumb" => $path . "thumbs/".Yii::app()->params['thumbs'][0]."/$filename",
                        "filename" => $filename,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('images', $userImages);

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode(array('files' => array(array(
                        "name" => $publicPath . $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => $publicPath . $filename,
                        "thumbnailUrl" => $publicPath . "thumbs/" . Yii::app()->params['thumbs'][0] . "/$filename",
                        "deleteUrl" => $this->createUrl("upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        )),
                        "deleteType" => "POST"
                    ))));
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),)
                    ));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction");
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    /**
     * Displays a particular model.
     */
    public function actionView()
    {
        $this->layout = 'column1';
        $post = $this->loadModel();
        $post->updateViewCount();
        $comment = $this->newComment($post);

        $this->render('view', array(
            'model' => $post,
            'comment' => $comment,
        ));
    }

    public function actionSearch($string = NULL)
    {
        if (!isset($_POST['search-str'])) {
            $this->redirect(array('post/index'));
        }
        $searchStr = $_POST['search-str'];
        $criteria = new CDbCriteria(array(
            'order' => 'update_time DESC',
            'with' => 'commentCount',
            'limit' => 30,
        ));
        $criteria->addSearchCondition('content', $searchStr);
        $criteria->addSearchCondition('title', $searchStr, true, 'OR');
        $criteria->addSearchCondition('preview_text', $searchStr, true, 'OR');
        $criteria->addCondition('status=' . Post::STATUS_PUBLISHED);

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => $perPage,
            ),
            'criteria' => $criteria,
        ));


        $this->render('index', array(
            'dataProvider' => $dataProvider,
            'catName' => $string,
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $this->layout = 'bs_column1';
        //        print_r($_POST);
        $model = new Post;
        $this->performAjaxValidation($model);


        $model->update_time = date('Y-m-d H:i', strtotime('-3 days'));
        Yii::import("xupload.models.XUploadForm");
        $images = new XUploadForm;

        if (isset($_POST['Post'])) {
            $model->attributes = $_POST['Post'];
            if ($model->save()) {
                if (isset($_POST['Post']['categories'])) {
                    $this->saveCategories($_POST['Post']['categories'], $model->id);
                }
                // THIS is how you capture those uploaded images: remember that in your CMultiFile widget, you set 'name' => 'images'
                $this->saveDocuments(CUploadedFile::getInstances($model, 'documents'), $model->id);
                $this->saveVideo($model->videoLocal, $model->videoHref, $model->id);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('create', array(
            'model' => $model,
            'images' => $images,
        ));
    }

    public function saveVideo($local, $href, $model_id)
    {
        $model = Video::model()->findByAttributes(array('post_id' => $model_id));
        if (!isset($model)) {
            $model = new Video();
            $model->post_id = $model_id;
        }
        $model->local = $local;
        $model->href = $href;
        $model->save();
    }

    public function saveDocuments($documents, $model_id)
    {
        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 120);
        if (isset($documents) && count($documents) > 0) {
            foreach ($documents as $doc) {
                $id = uniqid();
                $path = Yii::getPathOfAlias('webroot') . '/files/docs/';
                /* @var $doc CUploadedFile */
                if ($doc->saveAs($path . $id)) {
                    $docDB = new DBDocument();
                    $docDB->id = $id;
                    $docDB->post_id = $model_id;
                    $docDB->file_name = $doc->name;
                    $docDB->type = $doc->extensionName;
                    $docDB->size = $doc->size;
                    $docDB->mime_type = $doc->type;
                    $docDB->save();
                }
            }
        }
    }

    public function actionAjaxDeleteImg($id = NULL)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $doc = BDImage::model()->findByPk($id);
            if ($doc === null) {
                throw new CHttpException(404, 'Запрашиваемый файл не найден.');
            }
            if ($doc->delete()) {
                return TRUE;
            } else {
                throw new CHttpException(666, 'Файл не удалился');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    public function actionAjaxDeleteDoc($id = NULL)
    {
        if (Yii::app()->request->isAjaxRequest) {
            $doc = DBDocument::model()->findByPk($id);
            if ($doc === null) {
                throw new CHttpException(404, 'Запрашиваемый файл не найден.');
            }
            if ($doc->delete()) {
                return TRUE;
            } else {
                throw new CException(666, 'Файл не удалился');
            }
        } else {
            throw new CHttpException(400, 'Invalid request. Please do not repeat this request again.');
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate()
    {
        $this->layout = 'bs_column1';
        $model = $this->loadModel();
        //                        var_dump($model->update_time);

        $this->performAjaxValidation($model);

        $model->update_time = date('Y-m-d H:i', $model->update_time);
        Yii::import("xupload.models.XUploadForm");
        $images = new XUploadForm;

        if (isset($_POST['Post'])) {
            $model->attributes = $_POST['Post'];
            if ($model->save()) {
                if (isset($_POST['Post']['categories'])) {
                    $this->saveCategories($_POST['Post']['categories'], $model->id);
                }
                $this->saveDocuments(CUploadedFile::getInstances($model, 'documents'), $model->id);
                $this->saveVideo($model->videoLocal, $model->videoHref, $model->id);
                $this->redirect(array('view', 'id' => $model->id));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'images' => $images,
        ));
    }

    public function saveCategories($cats, $model_id)
    {
        PostCategory::model()->deleteAll("post_id = $model_id");
        foreach ($cats as $cat) {
            $postCat = new PostCategory();
            $postCat->post_id = $model_id;
            $postCat->category_id = $cat;
            $postCat->save();
        }
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete()
    {
        // we only allow deletion via POST request
        $this->loadModel()->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(array('index'));
        }
    }

    /**
     * Lists all models.
     */
    public function actionIndex($cat = NULL, $tag = NULL)
    {

        //Yii::app()->user->setFlash('success', 'Сайт находится на стадии внедрения. Возможны временные неполадки. Благодарим Вас за понимание.');
        $perPage = Yii::app()->params['postsPerPage'];
        $viewName = 'index';
        $catModel = null;
        $tagModel = null;
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time(),
            'order' => 'update_time DESC',
            'with' => 'commentCount',
            'limit' => $perPage * 20,
        ));


        if (!isset($cat) && !isset($tag)) {
            $criteria->addColumnCondition(array('on_main' => '1'));
        }

        if (isset($cat)) {
            if ($cat == Category::DOC) {
                $this->redirect('/doc/index');
            }
            $viewCatName = 'cat/cat-' . $cat;
            if ($this->getViewFile($viewCatName)) {
                $viewName = $viewCatName;
            }
            $catModel = Category::model()->findByAttributes(array('id' => $cat));
            $cats = CHtml::listData(Category::model()->getAllChildren($catModel), 'id', 'id');
            $cats[] = $cat;
            $cr = new CDbCriteria();
            $cr->addInCondition('category_id', $cats);
            $cr->limit = $perPage * 20;
            $cr->order = 'post_id desc';

            $posts = PostCategory::model()->findAll($cr);

            $ids = CHtml::listData($posts, 'post_id', 'post_id');
            $criteria->addInCondition('id', $ids);
        }

        if (isset($tag)) {
            $tagModel = Tag::model()->findByAttributes(array('name' => $tag));
            if (!$tagModel) {
                throw new CHttpException(404, 'Записей с этим тегом не найдено.');
            }
            $viewTagName = 'tag/tag-' . $tagModel->id;
            if ($this->getViewFile($viewTagName)) {
                $viewName = $viewTagName;
            }
            //            var_dump($tagModel->id);
            $criteria->addSearchCondition('tags', $tag);

            // fix for Smolenskoe ROP (id: 173) todo: remove hardcode
            if ($tagModel->id == 173) {
                $criteria->addSearchCondition('tags', 'Московско-Смоленское РОП', true, 'AND', 'NOT LIKE');
            }
        }

        $dataProvider = new CActiveDataProvider('Post', array(
            'pagination' => array(
                'pageSize' => $perPage,
            ),
            'criteria' => $criteria,
        ));


        $this->render($viewName, array(
            'dataProvider' => $dataProvider,
            'catName' => $catModel ? $catModel->label : null,
            'tagModel' => $tagModel ? $tagModel : null,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $this->layout = 'bs_column1';
        $model = new Post('search');
        if (isset($_GET['Post']))
            $model->attributes = $_GET['Post'];
        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Suggests tags based on the current user input.
     * This is called via AJAX when the user is entering the tags input.
     */
    public function actionSuggestTags()
    {
        if (isset($_GET['q']) && ($keyword = trim($_GET['q'])) !== '') {
            $tags = Tag::model()->suggestTags($keyword);
            if ($tags !== array())
                echo implode("\n", $tags);
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel()
    {
        if ($this->_model === null) {
            if (isset($_GET['id'])) {
                if (Yii::app()->user->isGuest)
                    $condition = 'status=' . Post::STATUS_PUBLISHED . ' OR status=' . Post::STATUS_ARCHIVED . ' OR status=' . Post::STATUS_IMPORTANT;
                else
                    $condition = '';
                $this->_model = Post::model()->findByPk($_GET['id'], $condition);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая запись не найдена.');
        }
        return $this->_model;
    }

    /**
     * Creates a new comment.
     * This method attempts to create a new comment based on the user input.
     * If the comment is successfully created, the browser will be redirected
     * to show the created comment.
     * @param Post the post that the new comment belongs to
     * @return Comment the comment instance
     */
    protected function newComment($post)
    {
        $comment = new Comment;
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'comment-form') {
            echo CActiveForm::validate($comment);
            Yii::app()->end();
        }
        if (isset($_POST['Comment'])) {
            $comment->attributes = $_POST['Comment'];
            if ($post->addComment($comment)) {
                if ($comment->status == Comment::STATUS_PENDING)
                    Yii::app()->user->setFlash('commentSubmitted', 'Спасибо за Ваш комментарий! Он будет размещён здесь после одобрения администратором сайта.');
                $this->refresh();
            }
        }
        return $comment;
    }

    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'post-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionGetEvents()
    {
        $formatter = Yii::app()->dateFormatter;

        $res = array();
        $events = Post::model()->findAllByAttributes(array('is_event' => true));
        foreach ($events as $model) {
            if (!empty($model->event_to)) {
                $dates = [];
                $from = new DateTime($model->event_from);
                $to = new DateTime($model->event_to);
                $interval = $from->diff($to);
                for ($i = 0; $i <= $interval->format('%d'); $i++) {
                    $dates[] = array(
                        'at' => $from->format('c'),
                        'from' => $model->event_from,
                        'to' => $model->event_to,
                        'title' => $model->title,
                        'content' => $model->event_note,
                        'url' => $model->url,
                        'dateHuman' => $formatter->format('d MMMM', $from->format('c'))
                    );
                    $from->add(new DateInterval('P1D'));
                }
                $res = array_merge($res, $dates);
            } else {
                $res[] = array(
                    'at' => $model->event_from,
                    'from' => $model->event_from,
                    'to' => $model->event_to,
                    'title' => $model->title,
                    'content' => $model->event_note,
                    'url' => $model->url,
                    'dateHuman' => $formatter->format('d MMMM', $model->event_from)
                );
            }
        }
        echo CJSON::encode($res);
    }
}
