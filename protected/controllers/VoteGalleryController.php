<?php

class VoteGalleryController extends Controller
{

    public $layout = 'bs_column1';

    /**
     * @var CActiveRecord the currently loaded data model instance.
     */
    private $_model;

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow all users to access 'index' and 'view' actions.
                'actions' => array('index'),
                'users' => array('*'),
            ),
            array('allow', // allow authenticated users to access all actions
                'users' => array('@'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     */
    public function actionView()
    {
//        $this->layout = 'column1';
//        $post = $this->loadModel();
//        $post->updateViewCount();
//
//        $this->render('view', array(
//            'model' => $post,
//        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {

        $model = new VotingGallery();
        $this->performAjaxValidation($model);

        Yii::import("xupload.models.XUploadForm");
        $files = new XUploadForm;

        if (isset($_POST['VotingGallery'])) {
            $model->attributes = $_POST['VotingGallery'];

            if ($model->save()) {
                if (isset($_POST['submit-and-create'])) {
                    $this->redirect(array('create'));
                } else {
                    $this->redirect(array('admin'));
                }
            }
        }


        $this->render('create', array(
            'model' => $model,
            'files' => $files,
        ));
    }

    public function actionUpload()
    {
        Yii::import("xupload.models.XUploadForm");
        //Here we define the paths where the files will be stored temporarily
        $imgPath = Yii::app()->params['imgPath'];
        $path = realpath(Yii::app()->getBasePath() . "/.." . $imgPath) . "/";
        $publicPath = Yii::app()->getBaseUrl() . $imgPath;

        header('Content-type: application/json');

        //Here we check if we are deleting and uploaded file
        if (isset($_GET["_method"])) {
            if ($_GET["_method"] == "delete") {
                if ($_GET["file"][0] !== '.') {
                    $file = $path . $_GET["file"];
                    if (is_file($file)) {
                        unlink($file);
                    }
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumb = $path . "thumbs/$thumbDem/" . $_GET["file"];
                        if (is_file($thumb)) {
                            unlink($thumb);
                        }
                    }
                }
                echo json_encode(true);
            }
        } else {
            $model = new XUploadForm;
            $model->file = CUploadedFile::getInstance($model, 'file');

            //We check that the file was successfully uploaded
            if ($model->file !== null) {
                //Grab some data
                $model->mime_type = $model->file->getType();
                $model->size = $model->file->getSize();
                //(optional) Generate a random name for our file
                $filename = uniqid();
                $filename .= "." . $model->file->getExtensionName();
                $model->name = $filename;
                if ($model->validate()) {
                    $thumb = Yii::app()->phpThumb->create($model->file->getTempName());
                    $dem = explode('x', Yii::app()->params['galleryImageSize']);
                    $thumb->resize($dem[0], $dem[1]);
                    $thumb->rotateImage('EXIF');
                    $thumb->save($path . $filename);
                    chmod($path . $filename, 0777);
                    foreach (Yii::app()->params['thumbs'] as $thumbDem) {
                        $thumbPath = $path . "thumbs/$thumbDem";
                        $thumb = Yii::app()->phpThumb->create($path . $filename);
                        $dem = explode('x', $thumbDem);
                        $thumb->adaptiveResize($dem[0], $dem[1]);
                        if (!is_dir($thumbPath)) {
                            mkdir($thumbPath);
                            chmod($thumbPath, 0777);
                        }
                        $thumb->save("$thumbPath/$filename");
                        unset($thumb);
                    }
                    if (Yii::app()->user->hasState('images')) {
                        $userImages = Yii::app()->user->getState('images');
                    } else {
                        $userImages = array();
                    }
                    $userImages[] = array(
                        "path" => $publicPath,
                        //the same file or a thumb version that you generated
                        //  "thumb" => $path . "thumbs/".Yii::app()->params['thumbs'][0]."/$filename",
                        "filename" => $filename,
                        'size' => $model->size,
                        'mime' => $model->mime_type,
                        'name' => $model->name,
                    );
                    Yii::app()->user->setState('images', $userImages);

                    //Now we need to tell our widget that the upload was succesfull
                    //We do so, using the json structure defined in
                    // https://github.com/blueimp/jQuery-File-Upload/wiki/Setup
                    echo json_encode(array('files' => array(array(
                        "name" => $publicPath . $model->name,
                        "type" => $model->mime_type,
                        "size" => $model->size,
                        "url" => $publicPath . $filename,
                        "thumbnailUrl" => $publicPath . "thumbs/" . Yii::app()->params['thumbs'][0] . "/$filename",
                        "deleteUrl" => $this->createUrl("upload", array(
                            "_method" => "delete",
                            "file" => $filename
                        )),
                        "deleteType" => "POST"
                    ))));
                } else {
                    //If the upload failed for some reason we log some data and let the widget know
                    echo json_encode(array(
                        array("error" => $model->getErrors('file'),
                        )));
                    Yii::log("XUploadAction: " . CVarDumper::dumpAsString($model->getErrors()), CLogger::LEVEL_ERROR, "xupload.actions.XUploadAction"
                    );
                }
            } else {
                throw new CHttpException(500, "Could not upload file");
            }
        }
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     */
    public function actionUpdate()
    {
        $model = $this->loadModel();
        $this->performAjaxValidation($model);

        Yii::import("xupload.models.XUploadForm");
        $files = new XUploadForm;

        if (isset($_POST['VotingGallery'])) {
            $model->attributes = $_POST['VotingGallery'];
            if ($model->save()) {
                $this->redirect(array('index'));
            }
        }

        $this->render('update', array(
            'model' => $model,
            'files' => $files,
        ));
    }


    public function actionVote($id)
    {
//        header("Access-Control-Allow-Origin: *");
        header('Content-Type: application/json');


        $voted = Voting::model()->exists('voting_id=:voting_id and user_id=:user_id and for=:for', array(
            ':voting_id' => VotingGallery::VOTING_ID,
            ':for' => $id,
            ':user_id' => Yii::app()->user->id,
        ));

        $success = false;
        $message = '';

        if (!$voted) {
            $model = new Voting();
            $model->voting_id = VotingGallery::VOTING_ID;
            $model->for = $id;
            $model->user_id = Yii::app()->user->id;
            $model->time = time();

            if ($model->save()) {
                $success = true;
                $message = 'Вы проголосовали';
            } else {
                throw new CException('vote save error');
            }

        } else {
            $message = 'Вы уже проголосовали';
        }


        echo json_encode(array(
            'success' => $success,
            'message' => $message
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     */
    public function actionDelete()
    {
        // we only allow deletion via POST request
        $this->loadModel()->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax'])) {
            $this->redirect(array('index'));
        }
    }

    /**
     * Manages all models.
     */
    public function actionIndex()
    {
        $this->layout = 'column1-bare';
        $model = new VotingGallery('search');

        if (isset($_GET['VotingGallery']))
            $model->attributes = $_GET['VotingGallery'];


        $criteria = new CDbCriteria(array(
            'condition' => 'active=1',
            'with' => 'voteCountRel',
        ));

        $criteria->addCondition('active=1');

        if ($_GET['nomination']) {
            $criteria->addCondition("nomination=" . $_GET['nomination']);
        }

        if ($_GET['region']) {
            $criteria->addCondition("region=" . $_GET['region']);
        }


        $dataProvider = new CActiveDataProvider('VotingGallery', array(
            'pagination' => array(
                'pageSize' => 18,
            ),
            'criteria' => $criteria,
            'sort' => array(
                'defaultOrder' => array('vCount' => 'desc'),
                'attributes' => array(
                    'vCount' => array(
                        'asc' => '(SELECT count(*) as vCount FROM tbl_voting WHERE for = t.id and voting_id = "VotingGallery18")',
                        'desc' => '(SELECT count(*) as vCount FROM tbl_voting WHERE for = t.id and voting_id = "VotingGallery18") DESC',
                        'label' => 'vote count',
                        'default' => 'desc',
                    )),
            )
        ));


        $this->render('index', array(
            'model' => $model,
            'dataProvider' => $dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model = new VotingGallery('search');

        if (isset($_GET['VotingGallery']))
            $model->attributes = $_GET['VotingGallery'];


        $this->render('admin', array(
            'model' => $model,
        ));
    }


    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     */
    public function loadModel()
    {
        if ($this->_model === null) {
            if (isset($_GET['id'])) {
                $this->_model = VotingGallery::model()->findByPk($_GET['id']);
            }
            if ($this->_model === null)
                throw new CHttpException(404, 'Запрашиваемая запись не найдена.');
        }
        return $this->_model;
    }


    /**
     * Performs the AJAX validation.
     * @param CModel the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'post-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
