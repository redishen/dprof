<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SCounterWidget
 *
 * @author asfto_sheftelevichdm
 */
class SCounterWidget extends CWidget {

    public function run() {        
        $counter =  Yii::app()->SCounter;
        $online = $counter->getOnline();
        $today = $counter->getToday();
        $all = $counter->getAll();
            
        echo '&nbsp;<span title="Просмотров всего"><i class="icon-eye-open">&nbsp;</i>'. $all . '</span>';
        echo '&nbsp;<span title="Посетителей сегодня"><i class="icon-user">&nbsp;</i>'.$today. '</span>';
        echo '&nbsp;<span title="Сейчас на сайте"><i class="icon-user highlight-green">&nbsp;</i>'.$online. '</span>';
    }

}

?>
