<?php

Yii::import('SCounter.models.SCounterData');
Yii::import('SCounter.models.SCounterParams');
Yii::import('SCounter.models.SCounterSummary');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SCounter
 *
 * @author asfto_sheftelevichdm
 */
class SCounter extends CComponent
{

    public $controllers;
    public $actions;

    function init()
    {

    }


    public function migrate()
    {
        return;

        $time = strtotime('-12 month');
        $id = Yii::app()->db_sys->createCommand()
            ->select('id')
            ->from('scounter_data')
            ->where("time >= :time", array(':time' => $time))
            ->order('time')
            ->limit(1)
            ->queryScalar();
        var_dump($id);

        SCounterData::model()->deleteAll('id < :id', array(':id' => $id));
        SCounterParams::model()->deleteAll('row_id < :id', array(':id' => $id));
        return;

        $from = strtotime('2013-06-10');
        while ($from < time()) {
            $from = strtotime('+1 day', $from);
            $to = strtotime('+1 day', $from);

            $views = Yii::app()->db_sys->createCommand()
                ->select('count(*)')
                ->from('scounter_data')
                ->where("time BETWEEN :from AND :to", array(':from' => $from, ':to' => $to))
                ->queryScalar();

            $unique = Yii::app()->db_sys->createCommand()
                ->select('count(*)')
                ->from('scounter_data')
                ->group('ip')
                ->where("time BETWEEN :from AND :to", array(':from' => $from, ':to' => $to))
                ->queryScalar();

            $y = date('Y', $from);
            $m = date('m', $from);
            $d = date('d', $from);

            $model = new SCounterSummary();
            $model->year = $y;
            $model->month = $m;
            $model->day = $d;
            $model->views = $views;
            $model->unique = $unique;
            $model->save();

            echo date('Y-m-d', $from) . ' - ' . date('Y-m-d', $from) . PHP_EOL;
            echo 'v: ' . $views . ', u: ' . $unique . PHP_EOL;
            echo PHP_EOL;

//            SCounterData::model()->deleteAll('time BETWEEN :from AND :to', array(':from' => $from, ':to' => $to));
        }
    }

    /**
     * @deprecated  No longer used
     */
    static function getPostViews($id)
    {
        $res = Yii::app()->db_sys->createCommand()
            ->select('count(*)')
            ->from('scounter_data sd')
            ->join('scounter_params sp', 'sd.id=sp.row_id')
            ->where("param_name='id' AND param_value=:id", array(':id' => $id))
            ->queryScalar();
        return $res;
    }

    function getOnline()
    {
        $res = SCounterData::model()->count(array(
                'group' => 'ip',
                'condition' => 'time > :offLineTime',
                'params' => array(':offLineTime' => strtotime('-10 min')),
            )
        );
        return ++$res;
    }

    function getToday()
    {
        $res = SCounterData::model()->count(array(
                'group' => 'ip',
                'condition' => 'time > :todayTime',
                'params' => array(':todayTime' => strtotime('today')),
            )
        );
        $this->updateSummary($res);
        return $res;
    }

    function getAll()
    {
        $res = Yii::app()->db_sys->createCommand()
            ->select('SUM(views)')
            ->from('scounter_summary')
            ->queryScalar();

        return $res;
    }

    public function updateSummary($unique = false)
    {
        $y = date('Y');
        $m = date('m');
        $d = date('d');

        $model = SCounterSummary::model()->findByPk(array(
            'year' => $y,
            'month' => $m,
            'day' => $d,
        ));
        $found = !empty($model);

        if (!$found) {
            $model = new SCounterSummary();
            $model->year = $y;
            $model->month = $m;
            $model->day = $d;
        }

        if ($unique !== false) {
            if (!$found) {
                $model->unique = $unique;
                $model->save();
            } else {
                $model->unique = $unique;
                $model->update('unique');
            }
        } else {
            if (!$found) {
                $model->views = 1;
                $model->save();
            } else {
                SCounterSummary::model()->updateCounters(array('views' => 1),
                    'year = :y AND month = :m AND day = :d',
                    array(
                        ':y' => $y,
                        ':m' => $m,
                        ':d' => $d,
                    ));
            }
        }
    }

    function cleanUp()
    {
        $models = SCounterData::model()->findAll(
            array(
                'condition' => 'time < :time',
                'params' => array(
                    ':time' => Yii::app()->params['isWeb'] ? strtotime('-1 month') : strtotime('-12 month'),
                ),
                'limit' => 1000,
            )
        );

        $ids = array();
        foreach ($models as $model) {
            $ids[] = $model->id;
        }
        SCounterData::model()->deleteByPk($ids);
        SCounterParams::model()->deleteAllByAttributes(array('row_id' => $ids));
    }

    function refresh($controller, $action, $params)
    {
        if (in_array($controller, $this->controllers) && in_array($action, $this->actions)) {
            $row = new SCounterData();
            $row->ip = $_SERVER['REMOTE_ADDR'];
            $row->host_name = $_SERVER['REMOTE_HOST'];
            $row->action = $action;
            $row->controller = $controller;
            $row->is_web = Yii::app()->params['isWeb'];
            $row->time = time();

            if ($row->save()) {
                if (!empty($params)) {
                    foreach ($params as $paramName => $paramValue) {
                        $counterParams = new SCounterParams();
                        $counterParams->row_id = $row->id;
                        $counterParams->param_name = $paramName;
                        $counterParams->param_value = $paramValue;
                        $counterParams->save();
                    }
                }

                if ($row->id % 100 == 0) {
                    $this->cleanUp();
                }
            }

            $this->updateSummary();
        }
    }
}

?>
