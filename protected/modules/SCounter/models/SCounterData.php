<?php

/**
 * This is the model class for table "scounter_Data".
 *
 * The followings are the available columns in table 'scounter_Data':
 * @property integer $id
 * @property string $is_web
 * @property string $ip
 * @property string $host_name
 * @property string $controller
 * @property string $action
 * @property integer $time
 */
class SCounterData extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return SCounterData the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection() {
        return Yii::app()->db_sys;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'scounter_Data';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ip', 'required'),
            array('time', 'numerical', 'integerOnly' => true),
            array('is_web, host_name, controller, action', 'safe'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, is_web, ip, host_name, controller, action, time', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'is_web' => 'Is Web',
            'ip' => 'Ip',
            'host_name' => 'Host Name',
            'controller' => 'Controller',
            'action' => 'Action',
            'time' => 'Time',
        );
    }

    public function beforeDelete() {
        SCounterParams::model()->deleteAllByAttributes(array('row_id' => $this->id));
        return parent::beforeDelete();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('is_web', $this->is_web, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('host_name', $this->host_name, true);
        $criteria->compare('controller', $this->controller, true);
        $criteria->compare('action', $this->action, true);
        $criteria->compare('time', $this->time);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}