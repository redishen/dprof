<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncComponent
 *
 * @author asfto_sheftelevichdm
 */
class SyncComponent extends CComponent
{

    public $ftpSleep = 2;


    public $remoteUrl;

    public $localImagesPath;
    public $localDocsPath;
    public $remoteImagesPath;
    public $remoteDocsPath;

    protected $filesToDownload;

    public function init()
    {

        //        $ftp = Yii::app()->ftp;
        //
        //            var_dump($ftp->fileExists('/files/images/514b075fd32dd.JPG'));
        //        var_dump($ftp->size('/files/images/514b075fd32dd.JPG'));
        //        var_dump($ftp->fileExists('/files/images/0.JPG'));
        //        var_dump($ftp->size('/files/images/567bcc1a5e48e.JPG'));
    }

    static function l($msg)
    {
        echo $msg . PHP_EOL;
        Yii::log($msg, CLogger::LEVEL_INFO);
    }

    public function run($filesOnly = false)
    {
        if ($filesOnly) {

            // $images = BDImage::model()->findAll(array(
            //     'limit' => 500,
            //     'order' => 'file_name desc'
            // ));
            // foreach ($images as $model) {
            //     self::l('>>>> Image ' . $model->file_name . ' has been successfully saved.');
            //     $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . $model->file_name, 'local' => $this->localImagesPath . $model->file_name);
            //     $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . 'thumbs/150x150/' . $model->file_name, 'local' => $this->localImagesPath . 'thumbs/150x150/' . $model->file_name);
            //     $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . 'thumbs/200x200/' . $model->file_name, 'local' => $this->localImagesPath . 'thumbs/200x200/' . $model->file_name);
            // }

            $docs = DBDocument::model()->findAll(array(
                'limit' => 1000,
                'order' => 'file_name desc'
            ));
            foreach ($docs as $model) {
                self::l('>>>> Document with id #' . $model->id . ' has been successfully saved.');
                $this->filesToDownload[] = array('remote' => $this->remoteDocsPath . $model->id, 'local' => $this->localDocsPath . $model->id);
            }

            self::l('Ok buddy, let\'s sync files!');
            $this->downloadFiles();
            return;
        }
        self::l('Ok buddy, let\'s sync!');
        $latestId = Yii::app()->db->createCommand('select max(id) from tbl_post')->queryScalar();
        self::l('Latest id - ' . $latestId);
        self::l('Trying to get data from remote server...');
        $data = $this->getSyncData($latestId);
        if (!empty($data)) {
            self::l('Got data to sync! ' . count($data) . ' fresh posts! Starting...');
            $this->processData($data);
            self::l('Eah! Everything went ALRIGHT! THNX GOD...');
        } else {
            self::l('No data to sync( Bye-bye...');
        }
        //$this->syncData();
    }

    protected function getSyncData($latestId)
    {
        $url = $this->remoteUrl . 'latestId/' . $latestId;

        try {
            //            $json = file_get_contents();
            //  Initiate curl
            $ch = curl_init();
            // Disable SSL verification
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            // Will return the response, if false it print the response
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            // Set the url
            curl_setopt($ch, CURLOPT_URL, $url);
            // Execute
            $result = curl_exec($ch);
            // Closing
            curl_close($ch);

            $data = CJSON::decode($result);
        } catch (Exception $e) {
            echo $e;
        }
        return $data;
    }

    protected function processData($data)
    {
        foreach ($data as $row) {

            $this->saveModel($row);
            //            print_r($row);
        }
        $this->downloadFiles();
    }

    protected function saveModel($row)
    {
        self::l('*** Processing id #' . $row['attributes']['id']) . ' ***';
        $post = new Post('sync');
        $post->attributes = $row['attributes'];
        $error = false;
        $transaction = $post->dbConnection->beginTransaction();
        try {
            if ($post->save()) {
                if (!empty($row['relations']) && isset($row['relations'])) {
                    $relations = $row['relations'];

                    $error = $error || !$this->saveCategories($relations['postCategories']);
                    $error = $error || !$this->saveVideos($relations['postImages']);
                    $error = $error || !$this->saveImages($relations['postImages']);
                    $error = $error || !$this->saveDocs($relations['postDocuments']);
                }
            } else {
                $error = 'post';
            }
            if (!$error) {
                $transaction->commit();
                self::l('*** Model with id #' . $post->id . ' has been saved. ***');
            } else {
                $transaction->rollback();
                self::l('*** Error while saving model with id #' . $row['attributes']['id'] . ' ***');
            }
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    protected function saveCategories($postCategories)
    {
        if (empty($postCategories)) {
            return true;
        }
        foreach ($postCategories as $postCategory) {
            $model = new PostCategory('sync');
            $model->attributes = $postCategory;
            if ($model->save()) {
                self::l('>>>> Category with id #' . $model->category_id . ' has been successfully linked.');
            } else {
                return false;
            }
        }
        return true;
    }

    protected function saveVideos($postVideos)
    {
        if (empty($postVideos)) {
            return true;
        }
        foreach ($postVideos as $postVideo) {
            $model = new Video('sync');
            $model->attributes = $postVideo;
            if ($model->save()) {
                self::l('>>>> Video ' . $model->id . ' has been successfully saved.');
            } else {
                return false;
            }
        }
        return true;
    }

    protected function saveDocs($postDocuments)
    {
        if (empty($postDocuments)) {
            return true;
        }
        foreach ($postDocuments as $postDocument) {
            $model = new DBDocument('sync');
            $model->attributes = $postDocument;
            if ($model->save()) {
                self::l('>>>> Document with id #' . $model->id . ' has been successfully saved.');
                $this->filesToDownload[] = array('remote' => $this->remoteDocsPath . $model->id, 'local' => $this->localDocsPath . $model->id);
            } else {
                return false;
            }
        }
        return true;
    }

    protected function saveImages($postImages)
    {
        if (empty($postImages)) {
            return true;
        }
        foreach ($postImages as $postImage) {
            $model = new BDImage('sync');
            $model->attributes = $postImage;
            if ($model->save()) {
                self::l('>>>> Image ' . $model->file_name . ' has been successfully saved.');
                $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . $model->file_name, 'local' => $this->localImagesPath . $model->file_name);
                $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . 'thumbs/150x150/' . $model->file_name, 'local' => $this->localImagesPath . 'thumbs/150x150/' . $model->file_name);
                $this->filesToDownload[] = array('remote' => $this->remoteImagesPath . 'thumbs/200x200/' . $model->file_name, 'local' => $this->localImagesPath . 'thumbs/200x200/' . $model->file_name);
            } else {
                return false;
            }
        }
        return true;
    }


    protected function downloadFiles()
    {
        self::l('*** Downloading files... ***');
        self::l('Files to download: ' . count($this->filesToDownload));

        if (!count($this->filesToDownload)) return;

        try {
            /** @var GFtpApplicationComponent $ftp */
            $ftp = Yii::app()->ftp;
            foreach ($this->filesToDownload as $key => $file) {
                if (file_exists($file['local'])) {
                    self::l('>>> File found, skiping ' . $file['remote']);
                } elseif ($ftp->fileExists($file['remote'])) {
                    //                  self::l('+>> File downloading: ' . $file['remote']);
                    $ftp->get(FTP_BINARY, $file['remote'], $file['local']);
                } else {
                    self::l('>>> File not found: ' . $file['remote']);
                }

                if ($key % 10 == 0) {
                    self::l('>>> Files downloaded: ' . $key);
                    sleep($this->ftpSleep);
                }
            }
            self::l('All files are successfully downloaded!');
        } catch (GFtpException $e) {
            echo $e;
            die('FTP connection error');
        }
    }

    protected function syncData()
    {
        try {
            /* @var $gftp GFtpApplicationComponent */
            $localFiles = Yii::getPathOfAlias('application') . '/../files';
            $localDocsDir = $localFiles . '/docs/';
            $localImagesDir = $localFiles . '/images/';

            $gftp = Yii::app()->ftp;
            $dbSynced = $this->syncDb($gftp);
            echo "dbSynced - ", ($dbSynced ? 'yes' : 'no');

            $docsCopied = $this->syncDir($gftp, $localDocsDir, $this->docsPath);
            echo "\ndocsCopied - " . $docsCopied;
            $imgCopied = $this->syncDir($gftp, $localImagesDir, $this->imagesPath);
            echo "\nimgCopied - " . $imgCopied;
            $imgThumbs150Copied = $this->syncDir($gftp, $localImagesDir . 'thumbs/150x150/', $this->imagesPath . 'thumbs/150x150/');
            echo "\nimgThumbs150Copied - " . $imgThumbs150Copied;
            $imgThumbs200Copied = $this->syncDir($gftp, $localImagesDir . 'thumbs/200x200/', $this->imagesPath . 'thumbs/200x200/');
            echo "\nimgThumbs200Copied - " . $imgThumbs200Copied;
            echo "\n\n";
            //            echo "\n\nTHNX LORD, EVERITHING IS OK!\n\n";
        } catch (GFtpException $e) {
            echo $e;
            die('FTP connection error');
        }
    }

    protected function syncDb($ftp)
    {
        $remoteName = $this->dbPath . $this->dbName . '.db';
        $bupName = $this->dbBupPath . $this->dbName . '_' . date('Y-m-d') . '.db';
        $localDb = Yii::getPathOfAlias('application.data') . '/' . $this->dbName . '.db';
        $localModTime = filemtime($localDb);
        $remoteModTime = $ftp->mdtm($remoteName);
        if ($remoteModTime < $localModTime) {
            $ftp->rename($remoteName, $bupName);
            $ftp->put(FTP_BINARY, $localDb, $remoteName);
            return true;
        }
        return false;
    }

    protected function syncDir($ftp, $localDir, $remoteDir)
    {
        $remoteDocs = $ftp->ls($remoteDir, true);
        $remoteDocsPlain = array();
        foreach ($remoteDocs as $doc) {
            $remoteDocsPlain[] = $doc->filename;
        }

        $localDocs = scandir($localDir);
        unset($localDocs[0], $localDocs[1]);

        $docsCopied = 0;
        foreach ($localDocs as $localDocName) {
            if (!array_search($localDocName, $remoteDocsPlain)) {
                $ftp->put(FTP_BINARY, $localDir . $localDocName, $remoteDir . $localDocName);
                $docsCopied++;
            }
        }
        return $docsCopied;
    }
}
