<?php

class ExportController extends SyncController {

    public function actionCount() {
        $count = Post::model()->count();

        $this->sendResponse(200, CJSON::encode($count));
    }

//    public function actionCreate() {
//        $attr = Yii::app()->request->getPost('Post');
//
//        var_dump($attr);
//    }

    public function actionIndex($latestId, $limit = 25) {
        $models = Post::model()->findAll(
                array(
                    'condition' => 'id > :latestId',
                    'order' => 'id asc',
                    'limit' => $limit,
                    'with' => array('postImages', 'postDocuments', 'postCategories'),
                    'params' => array(
                        ':latestId' => $latestId
                    )
        ));
        $rows = array();
        foreach ($models as $model) {
            $rows[$model->id] = $model->toJSON(true);
        }
        $this->sendResponse(200, CJSON::encode($rows));
    }

}
