<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of VoteForm
 *
 * @author asfto_sheftelevichdm
 */
class VoteForm extends PVote {

    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules() {
        return array(
            array('poll_id, ip, key, region, branch, fio', 'required'),
            array('poll_id, ip, key, region', 'numerical', 'integerOnly' => true),
            array('branch', 'length', 'max' => 128),
            array('fio', 'length', 'max' => 256),
            // email has to be a valid email address
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'allowEmpty' => !CCaptcha::checkRequirements()),
        );
    }

    /**
     * Declares customized attribute labels.
     * If not declared here, an attribute would have a label that is
     * the same as its name with the first letter in upper case.
     */
    public function attributeLabels() {
        return array(
            'region' => 'Регион',
            'branch' => 'Подразделение',
            'fio' => 'Фамилия',
            'verifyCode' => 'Вы не робот?',
            'key' => 'Варианты ответов'
        );
    }

    //put your code here
}

?>
