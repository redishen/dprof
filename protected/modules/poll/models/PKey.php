<?php

/**
 * This is the model class for table "poll_key".
 *
 * The followings are the available columns in table 'poll_key':
 * @property integer $poll_id
 * @property integer $key
 * @property string $label
 *
 * The followings are the available model relations:
 * @property Poll $poll
 */
class PKey extends CActiveRecord {

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return PKey the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return CDbConnection database connection
     */
    public function getDbConnection() {
        return Yii::app()->{Yii::app()->getModule('poll')->dbConn};
      //  return Yii::app()->db_sys;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'poll_key';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('poll_id, key, label', 'required'),
            array('key', 'unique', 'criteria' => array('condition' => "poll_id={$this->poll_id}",)),
            array('poll_id, key', 'numerical', 'integerOnly' => true),
            array('label', 'length', 'max' => 255),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('poll_id, key, label', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'poll' => array(self::BELONGS_TO, 'Poll', 'poll_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'id',
            'poll_id' => 'Poll',
            'key' => 'Key',
            'label' => 'Label',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search() {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('poll_id', $this->poll_id);
        $criteria->compare('key', $this->key);
        $criteria->compare('label', $this->label, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}