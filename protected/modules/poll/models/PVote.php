<?php

/**
 * This is the model class for table "poll_vote".
 *
 * The followings are the available columns in table 'poll_vote':
 * @property integer $id
 * @property integer $poll_id
 * @property integer $ip
 * @property integer $key
 * @property boolean $is_web
 * @property integer $region
 * @property string $branch
 * @property string $fio
 *
 * The followings are the available model relations:
 * @property Poll $poll
 */
class PVote extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return PVote the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        

        /**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->{Yii::app()->getModule('poll')->dbConn};
	}
        
        public function beforeSave() {
            if ($this->isNewRecord) {
                $this->is_web = Yii::app()->params['isWeb'];
                $this->time = time();
            }
            return TRUE;
        }

        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'poll_vote';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('poll_id, ip, key', 'required'),
			array('poll_id, ip, key, region', 'numerical', 'integerOnly'=>true),
			array('branch', 'length', 'max'=>128),
			array('fio', 'length', 'max'=>256),
			array('is_web', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, poll_id, ip, key, is_web, region, branch, fio', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'poll' => array(self::BELONGS_TO, 'PPoll', 'poll_id'),
                    'region' => array(self::HAS_ONE, 'Lookup', 'id', 'on' => 'type = "Region"'),
//                    'poll' => array(self::BELONGS_TO, 'PPoll', 'poll_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'poll_id' => '№Опроса',
			'ip' => 'Ip-адрес',
			'key' => 'Ключ',
			'is_web' => 'Интернет',
			'region' => 'Регион',
			'branch' => 'Подразделение',
			'fio' => 'Фамилия',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('poll_id',$this->poll_id);
		$criteria->compare('ip',$this->ip);
		$criteria->compare('key',$this->key);
		$criteria->compare('is_web',$this->is_web);
		$criteria->compare('region',$this->region);
		$criteria->compare('branch',$this->branch,true);
		$criteria->compare('fio',$this->fio,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}