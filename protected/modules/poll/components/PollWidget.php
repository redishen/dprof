<?php

Yii::import('poll.models.*');
Yii::import('poll.components.CJuiLabelProgressBar');
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PollWidget
 *
 * @author asfto_sheftelevichdm
 */
class PollWidget extends CWidget {

    public $poll_id;
    

    public function run() {
        $poll = PPoll::model()->find($this->poll_id);

        $allCount = PVote::model()->countByAttributes(array('poll_id' => $this->poll_id));
        $polldata = array();
        foreach ($poll->keys as $key) {
            $count = PVote::model()->countByAttributes(array('poll_id' => $this->poll_id, 'key' => $key->key));
            $percent = $allCount != 0 ? round($count/$allCount*100, 2) : 0;
            $polldata[$key->key] = array('label' => $key->label, 'val' => $count, 'percent' => $percent);
        }
        
        $this->render('widget', array(
            'poll' => $poll,
            'polldata' => $polldata,
            'allCount' => $allCount
        ));
     }

}

?>
