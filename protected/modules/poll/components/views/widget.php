<?php ?>
<style>
    #voteLink {
        display: block;
        text-align: center;
        font-size: 28px;
        text-decoration: none;
    }
    
    
</style>
<div style="
     background-color: #FFF0F0;
     border-left: solid #FAA4A4 20px;
     margin-left: -20px;
     padding: 5px;
     ">
    <h4>
        <?php echo $poll->title; ?>
    </h4>
    <?php foreach ($polldata as $data): ?>
    <div style="padding: 0 0 0 5px; font-size: 14px;">
        <?php echo $data['label'] ?>
    </div>
        
        <?php
        $this->widget('poll.components.CJuiLabelProgressBar', array(
            'value' => $data['percent'],
            'label' => $data['percent'] . '%' . ' (голосов: ' . $data['val'] . ')',
            'htmlOptions' => array(
                'style' => 'height:15px; margin: 5px;',
            ),
        ));
        ?>
    


    <?php endforeach; ?>
    
    <p><strong>Всего проголосовавших:  <?php echo $allCount ?></strong></p>

<!--    <hr class="space">-->
    <?php echo CHtml::link('Голосовать', array('poll/poll/vote', 'id' => $poll->id), array('id' => 'voteLink')); ?>
</div>