<?php
/* @var $this VoteController */
/* @var $model PVote */

$this->breadcrumbs = array(
    'Pvotes' => array('index'),
    'Manage',
);

$this->menu = array(
    array('label' => 'List PVote', 'url' => array('index')),
    array('label' => 'Create PVote', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('pvote-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Pvotes</h1>

<p>
    You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
    or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<?php
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'pvote-grid',
    'dataProvider' => $provider,
    'filter' => $model,
    'columns' => array(
        'id',
//		'poll_id',
//		'ip',
        'key',
        'is_web',
        'region',
        'branch',
        'fio',
//		':datetime',
        'name' => array(
            'name' => 'time',
            'header' => 'time',
            'value' => 'Yii::app()->dateFormatter->format("d MMM y H:m",$data->time)'),
        array(
            'class' => 'CButtonColumn',
        ),
    ),
));
?>
