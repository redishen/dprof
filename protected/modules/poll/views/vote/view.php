<?php
/* @var $this VoteController */
/* @var $model PVote */

$this->breadcrumbs=array(
	'Pvotes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PVote', 'url'=>array('index')),
	array('label'=>'Create PVote', 'url'=>array('create')),
	array('label'=>'Update PVote', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PVote', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PVote', 'url'=>array('admin')),
);
?>

<h1>View PVote #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'poll_id',
		'ip',
		'key',
		'is_web',
		'region',
		'branch',
		'fio',
		'time',
	),
)); ?>
