<?php
/* @var $this VoteController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Pvotes',
);

$this->menu=array(
	array('label'=>'Create PVote', 'url'=>array('create')),
	array('label'=>'Manage PVote', 'url'=>array('admin')),
);
?>

<h1>Pvotes</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
