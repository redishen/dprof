<?php
/* @var $this VoteController */
/* @var $model PVote */

$this->breadcrumbs=array(
	'Pvotes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PVote', 'url'=>array('index')),
	array('label'=>'Create PVote', 'url'=>array('create')),
	array('label'=>'View PVote', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PVote', 'url'=>array('admin')),
);
?>

<h1>Update PVote <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>