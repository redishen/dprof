<?php
/* @var $this VoteController */
/* @var $model PVote */

$this->breadcrumbs=array(
	'Pvotes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PVote', 'url'=>array('index')),
	array('label'=>'Manage PVote', 'url'=>array('admin')),
);
?>

<h1>Create PVote</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>