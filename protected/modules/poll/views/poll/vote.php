<?php
/* @var $this PollController */
/* @var $model PPoll */

$this->breadcrumbs = array(
    'Голосование',
    $poll->title,
);
?>



<h2>Голосование: <i><?php echo $poll->title; ?></i> </h2>


<hr class="space">

<div class="form">
    <?php $form = $this->beginWidget('CActiveForm'); ?>

    <p class="note">Поля, отмеченные <span class="required">*</span> обязательны для заполнения.</p>

    <?php echo $form->errorSummary($model); ?>

    <fieldset>
        <div class="row">
            <?php echo $form->labelEx($model, 'region'); ?>
            <?php echo $form->dropDownList($model, 'region', Lookup::items('Region'), array('empty' => 'Выберите из списка')) ?>
            <?php echo $form->error($model, 'region'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'branch'); ?>
            <?php echo $form->textField($model, 'branch'); ?>
            <?php echo $form->error($model, 'branch'); ?>
        </div>

        <div class="row">
            <?php echo $form->labelEx($model, 'fio'); ?>
            <?php echo $form->textField($model, 'fio'); ?>
            <?php echo $form->error($model, 'fio'); ?>
        </div>
        <div class="hint">Ваши данные нигде не будут опубликованы, ввод необходим только для обоснования подлинности голосования.
    </fieldset>

    <fieldset>
        <div class="row">
            <?php echo $form->labelEx($model, 'key'); ?>
            <?php echo $form->radioButtonList($model, 'key', CHtml::listData($poll->keys, 'key', 'label')) ?>
            <?php echo $form->error($model, 'key'); ?>
        </div>
    </fieldset>
    <fieldset>
        <?php if (extension_loaded('gd')): ?>
            <div class="row">
                <?php echo $form->labelEx($model, 'verifyCode'); ?>
                <div>
                    <?php $this->widget('CCaptcha'); ?>
                    <?php echo $form->textField($model, 'verifyCode'); ?>
                </div>
                <?php echo $form->error($model, 'verifyCode'); ?>
                <div class="hint">Пожалуйста, введите код с картинки.
                </div>
            </div>
        <?php endif; ?>
    </fieldset>

    <div class="row submit">
        <?php echo CHtml::submitButton('Голосовать!'); ?>

    </div>
    <?php $this->endWidget(); ?>

</div>


<div class="clear"></div>