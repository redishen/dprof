<?php
/* @var $this PollController */
/* @var $model PPoll */

$this->breadcrumbs=array(
	'Ppolls'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PPoll', 'url'=>array('index')),
	array('label'=>'Manage PPoll', 'url'=>array('admin')),
);
?>

<h1>Create PPoll</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>