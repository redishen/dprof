<?php
/* @var $this PollController */
/* @var $model PPoll */

$this->breadcrumbs = array(
    'Ppolls' => array('index'),
    $model->title,
);

$this->menu = array(
    array('label' => 'List PPoll', 'url' => array('index')),
    array('label' => 'Create PPoll', 'url' => array('create')),
    array('label' => 'Update PPoll', 'url' => array('update', 'id' => $model->id)),
    array('label' => 'Delete PPoll', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage PPoll', 'url' => array('admin')),
    array('label' => 'Добавить пункт', 'url' => array('key/create', 'poll_id' => $model->id)),
);
?>

<h1>View PPoll #<?php echo $model->id; ?></h1>

<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'title',
        'status',
    ),
));
?>
<hr class="space">
<h3>Пункты <small><?php echo CHtml::link('Добавить', array('key/create', 'poll_id' => $model->id)) ?></small> </h3>

<ul>
    <?php foreach ($model->keys as $key): ?>

        <li>
            <?php echo $key->key . ' - ' . $key->label; ?>
            <?php echo CHtml::link('Изменить', array('key/update', 'id' => $key->id)) ?>
            <?php
            echo CHtml::linkButton('Удалить', array(
                'submit' => array('key/delete', 'id' => $key->id),
                'confirm' => "Вы уверены что хотите удалить пункт #{$key->label}?",
            ));
            ?>
        </li>
    <?php endforeach; ?>
</ul>