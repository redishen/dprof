<?php
/* @var $this PollController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ppolls',
);

$this->menu=array(
	array('label'=>'Create PPoll', 'url'=>array('create')),
	array('label'=>'Manage PPoll', 'url'=>array('admin')),
);
?>

<h1>Ppolls</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
