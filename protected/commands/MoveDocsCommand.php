<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncCommand
 *
 * @author asfto_sheftelevichdm
 */
class MoveDocsCommand extends CConsoleCommand
{

    public function run($args)
    {
        date_default_timezone_set('Etc/GMT+3');
        $criteria = new CDbCriteria(array(
            'condition' => 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time(),
            'order' => 'update_time DESC',
        ));

        $cat = 11;
        $catModel = Category::model()->findByAttributes(array('id' => $cat));
        $cats = CHtml::listData(Category::model()->getAllChildren($catModel), 'id', 'id');
        $cats[] = $cat;
        $cr = new CDbCriteria();
        $cr->addInCondition('category_id', $cats);
        $postCategories = PostCategory::model()->findAll($cr);

        $ids = CHtml::listData($postCategories, 'post_id', 'post_id');

        var_dump($ids);
        echo "\n";

        $criteria->addInCondition('id', $ids);
        $posts = Post::model()->findAll($criteria);
        var_dump(CHtml::listData($posts, 'id', 'title'));

        $move = array();
        $transaction = Post::model()->dbConnection->beginTransaction();
        try {
            foreach ($posts as $post) {
                if (empty($post->postDocuments)) {
                    echo "\n++skip++\n";
                    continue;
                }
                $model = new Post('doc');
                $model->setAttributes($post->getAttributes());
                $model->is_doc = true;
                $model->on_main = false;


                if ($model->save()) {
                    $move[] = array(
                        'from' => $post,
                        'to' => $model,
                    );
                    $post->related_posts = $model->id;
                    $post->setScenario('sync');
                    $post->update('related_posts');

                    $update = '';
                    if( $post->update_time) {
                        $update = date('Y-m-d', $post->update_time);
                    } else {
                        $update = date('Y-m-d', $post->create_time);
                    }
                    var_dump($update);
                    $model->update_time = $update;
                    if (!$model->update('update_time')) {
                        throw new Exception('Failed to update updatetime' . var_export($model->getErrors(), true));
                    }
                } else {
                    throw new Exception('Failed to save' . var_export($model->getErrors(), true));
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }

        echo "\n++ok++\n";

        foreach ($move as $moved) {
            $postCat = new PostCategory();
            if (preg_match('/пленум/i', $moved['to']->title) == 1) {
                $catDoc = 72;
            } elseif (preg_match('/президиум/i', $moved['to']->title) == 1) {
                $catDoc = 73;
            } else {
                $catDoc = Category::DOC;
            }
            $postCat->post_id = $moved['to']->id;
            $postCat->category_id = $catDoc;
            if ($postCat->save()) {
            } else {
                throw new Exception('Failed to save doc' . var_export($postCat->getErrors(), true));
            }
        }


        echo "\n++ok++\n";

        foreach ($move as $moved) {
            foreach ($moved['from']->postDocuments as $doc) {
                $doc->post_id = $moved['to']->id;
                if ($doc->save()) {
                    echo $doc->file_name . ' -- moved ' . PHP_EOL;
                } else {
                    throw new Exception('Failed to save doc' . var_export($doc->getErrors(), true));
                }
            }
        }


        echo "\n++ok++\n";
    }

}
