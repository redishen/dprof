<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncCommand
 *
 * @author asfto_sheftelevichdm
 */
class VoteExportCommand extends CConsoleCommand
{

    public function run($args)
    {
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Etc/GMT+4');

        $criteria = new CDbCriteria(array(
            'condition' => 'active=1',
            'with' => 'voteCountRel',
        ));

        $criteria->addCondition('active=1');

        $models = VotingGallery::model()->findAll($criteria);

        $res = [];
        foreach ($models as $model) {

            $res[] = [
                $model->id,
                $model->name,
                $model->voteCountRel,
                Lookup::item('Nomination', $model->nomination),
                Lookup::item('RegionMenu', $model->region),
                $model->image ? 'http://dprof.ru/' . $model->image->url : '',
            ];
        }

        $fp = fopen('file.csv', 'w');

        foreach ($res as $fields) {
            fputcsv($fp, $fields);
        }

        fclose($fp);

    }

}
