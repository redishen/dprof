<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncCommand
 *
 * @author asfto_sheftelevichdm
 */
class CounterMigrateCommand extends CConsoleCommand {

    public function run($args) {
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Etc/GMT+3');

        $counter = Yii::app()->SCounter;
        $counter->migrate();

    }

}
