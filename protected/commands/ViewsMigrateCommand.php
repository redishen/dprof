<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SyncCommand
 *
 * @author asfto_sheftelevichdm
 */
class ViewsMigrateCommand extends CConsoleCommand
{

    public function run($args)
    {
        ini_set('max_execution_time', '0');
        date_default_timezone_set('Etc/GMT+3');

        $list = Yii::app()->db_secondary->createCommand()
            ->select('id, view_count_local')
            ->from('tbl_post')
//            ->where("time >= :time", array(':time' => $time))
//            ->order('id')
//            ->limit(1)
            ->queryAll();
        foreach ($list as $row) {
            /** @var Post $model */
            $model = Post::model()->findByPk($row['id']);
            if ($model) {
                if (!$model->view_count_local) {
                    Yii::app()->db->createCommand()->update('tbl_post', array(
                        'view_count_local' => $row['view_count_local'],
                    ), 'id=:id', array(':id' => $row['id']));
                }
            }
        }

    }

}
