<?php
Yii::setPathOfAlias('vendor', dirname(__FILE__) . '/../vendor/');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Дорпрофжел на Московской ЖД',
    // preloading 'log' component
    'preload' => array('log', 'SCounter'),
    'language' => 'ru',
    'aliases' => array(
        'xupload' => 'ext.yii-file-upload',
        'yiinltcs' => 'ext.yiinalytics',
        'bootstrap' => 'vendor.drmabuse.yii-bootstrap-3-module',
        'wdgts' => dirname(__FILE__) . '/../widgets',
    ),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'ext.helpers.XHtml',
        // Twitter Bootstrap
        'bootstrap.behaviors.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*'
    ),
    'defaultController' => 'site',

    'modules' => array(
        'sync',
        'poll' => array(
            'dbConn' => 'db_poll',
        ),
        'SCounter',
    ),
    'components' => array(
        'bootstrap' => array(
            'class' => 'bootstrap.components.BsApi'
        ),
        'SCounter' => array(
            'class' => 'SCounter.components.SCounter',
            'controllers' => array('site', 'post', 'poll'),
            'actions' => array('index', 'view', 'login', 'page', 'contact', 'cat', 'login', 'downloaddoc', 'away', 'vote'),
        ),
        'cache' => array(
            'class' => 'system.caching.CDbCache',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'MetrikaWidget' => array(
                    'isActive' => FALSE
                ),
                'CBreadcrumbs' => array(
                    'encodeLabel' => false,
                ),
                'CJuiAccordion' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
                'CJuiDialog' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
                'CJuiButton' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
            ),
        ),
        'phpThumb' => array(
            'class' => 'ext.EPhpThumb.EPhpThumb',
        ),
        'user' => array(
            // enable cookie-based authentication
            'allowAutoLogin' => true,
        ),
        'db' => array(
            'connectionString' => 'sqlite:protected/data/blog.db',
            'tablePrefix' => 'tbl_',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            // показываем значения параметров
            'enableParamLogging' => true,
        ),
        'db_sys' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:protected/data/sys_db.db',
            'schemaCachingDuration' => 3600,
//            'enableProfiling' => true,
        // показываем значения параметров
//            'enableParamLogging' => true,
        ),
        'db_poll' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:protected/data/db_poll.db',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            // показываем значения параметров
            'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
//                'special/<view>' => 'site/special',
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'doc/download/<id:\w+>/<filename:.*?>' => 'doc/download',
                'doc/<id:\d+>/<title:.*?>' => 'doc/view',
                'posts/<tag:.*?>' => 'post/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning',
                ),
            ),
        ),
    ),
    // application-level parameters that can be accessed
    // using Yii::app()->params['paramName']
    'params' => array(
        'isLocal' => FALSE,
        'isWeb' => FALSE,
        'oldSiteLink' => 'http://dprof.msk.oao.rzd:8017/',
// this is displayed in the header section
        'title' => 'РосПрофЖел',
        'imageSize' => '800x600',
        'galleryImageSize' => '1600x1200',
        'thumbs' => array('200x200', '150x150'),
        'imgPath' => '/files/images/',
        'docPath' => '/files/docs/',
        // number of posts displayed per page
        'postsPerPage' => 10,
        'displayImportant' => 2,
        // maximum number of comments that can be displayed in recent comments portlet
        'recentCommentCount' => 10,
        // maximum number of tags that can be displayed in tag cloud portlet
        'tagCloudCount' => 20,
        // whether post comments need to be approved before published
        'commentNeedApproval' => true,
        // the copyright information displayed in the footer section
        'copyrightInfo' => 'Copyright &copy; 2009 by My Company.',
        'siteKey' => false,
        'resoEmails' => array(
            'mzd_rpz@reso.ru',
            'noreply@dprof.ru',
        ),
        'contactEmails' => array(
            'dprof.mzd@mail.ru',
            'noreply@dprof.ru',
            'sheftelevich.dm@gmail.com',
        ),
        'publicEmail' => 'dprof.mzd@mail.ru',
        'publicSupportEmail' => 'pressdprof@mail.ru'
    ),
    
);
