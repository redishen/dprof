<?php
#AUTOGENERATED BY HYBRIDAUTH 2.1.1-dev INSTALLER - Tuesday 15th of August 2017 07:57:43 PM

/*!
* HybridAuth
* http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
* (c) 2009-2012, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
*/

// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return
    array(
        'callback' => 'http://dprof.ru/user/login/oauth',

        "providers" => array(
            // openid providers

            "Facebook" => array(
                "enabled" => true,
                "keys" => array("id" => "335255666897586", "secret" => "1fddcdaa6f9019d84203730fb3017729"),
                "scope" => "email, public_profile",
            ),
            "Vkontakte" => array(
                "enabled" => true,
                "keys" => array("id" => "5464000", "secret" => "LwwwNcT2QE0eqSR2N8yx", 'service_token' => '4a43199d4a43199d4a43199d4d4a10465d44a434a43199d13d0f87263367556454eb6f0'),
                "scope" => "email, offline",
            ),
        ),

        // if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
        "debug_mode" => true,
        "debug_file" => "debug2.log"
    );
