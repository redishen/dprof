<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Dprof Console Application',
    'preload' => array('log', 'SCounter'),
    'language' => 'ru',
    'modules' => array(
        'user' => array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => true,

            # allow access for non-activated users
            'loginNotActiv' => false,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => false,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        'sync',
        'SCounter'
    ),
    'import' => array(
        'application.models.*',
//        'application.components.*',
//        'application.helpers.*',
    ),
    'commandMap' => array(
        'sync' => array(
            'class' => 'sync.commands.SyncCommand',
        ),
        'sync-files' => array(
            'class' => 'sync.commands.SyncFilesCommand',
        ),
    ),
    'components' => array(
        'SCounter' => array(
            'class' => 'SCounter.components.SCounter',
        ),
        'sync' => array(
            'class' => 'sync.components.SyncComponent',
            'remoteUrl' => 'http://dprof.ru/sync/export/index/',
//            'dbName' => 'blog',
//            'dbPath' => '/protected/data/',
//            'dbBupPath' => '/protected/data/BUP/',
            'remoteImagesPath' => '/files/images/',
            'localImagesPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../files/images/',
            'remoteDocsPath' => '/files/docs/',
            'localDocsPath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '../../files/docs/',
        ),
        'ftp' => array(
            'class' => 'ext.gftp.GFtpApplicationComponent',
            'connectionString' => 'ftp://u1195036_sync:4vpfMa0SXy+5@dprof.ru:21',
            'timeout' => 120,
            'passive' => true
        ),
        'db' => array(
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/blog.db',
            'tablePrefix' => 'tbl_',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
        'db_secondary' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/blog_secondary.db',
            'tablePrefix' => 'tbl_',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => false,
            'enableParamLogging' => false,
        ),
        'db_sys' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:' . dirname(__FILE__) . '/../data/sys_db.db',
            'schemaCachingDuration' => 3600,
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                ),
            ),
        ),
    ),
);
