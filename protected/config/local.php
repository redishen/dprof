<?php
Yii::setPathOfAlias('vendor', dirname(__FILE__) . '/../vendor/');

return array(
    'basePath' => dirname(__FILE__) . DIRECTORY_SEPARATOR . '..',
    'name' => 'Дорпрофжел на Московской ЖД:Local',
    // preloading 'log' component
    'preload' => array('log', 'SCounter'),
    'language' => 'ru',
    'aliases' => array(
        'xupload' => 'ext.yii-file-upload',
        'yiinltcs' => 'ext.yiinalytics',
        'bootstrap' => 'vendor.drmabuse.yii-bootstrap-3-module',
        'wdgts' => dirname(__FILE__) . '/../widgets',
    ),
    'import' => array(
        'application.models.*',
        'application.components.*',
        'application.helpers.*',
        'ext.helpers.XHtml',
        // Twitter Bootstrap
        'bootstrap.behaviors.*',
        'bootstrap.helpers.*',
        'bootstrap.widgets.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
    ),
    'defaultController' => 'site',

    'modules' => array(
        'user' => array(
            # encrypting method (php hash function)
            'hash' => 'md5',

            # send activation email
            'sendActivationMail' => false,

            # allow access for non-activated users
            'loginNotActiv' => true,

            # activate user on registration (only sendActivationMail = false)
            'activeAfterRegister' => true,

            # automatically login from registration
            'autoLogin' => true,

            # registration path
            'registrationUrl' => array('/user/registration'),

            # recovery password path
            'recoveryUrl' => array('/user/recovery'),

            # login form path
            'loginUrl' => array('/user/login'),

            # page after login
            'returnUrl' => array('/user/profile'),

            # page after logout
            'returnLogoutUrl' => array('/user/login'),
        ),
        'sync',
        'poll' => array(
            'dbConn' => 'db_poll',
        ),
        'SCounter',
        'gii' => array(
            'class' => 'system.gii.GiiModule',
            'password' => 'pass',
            'ipFilters' => array('127.0.0.1', '::1'),
        ),

    ),
    'components' => array(
        'user'=>array(
            // enable cookie-based authentication
            'class' => 'WebUser',
        ),
        'bootstrap' => array(
            'class' => 'bootstrap.components.BsApi'
        ),
        'yiinalytics' => array(
            'class' => 'yiinltcs.Yiinalytics',
            'engine' => 'piwik',
//            'engine' => 'metrika',
            'metrikaId' => 21075727,
            'piwikDomain' => 'dprof.msk.oao.rzd',
            'piwikUrl' => '//10.23.250.9/piwik/',
            'piwikId' => 1,
            'cachePeriod' => 1,
        ),
        'SCounter' => array(
            'class' => 'SCounter.components.SCounter',
            'controllers' => array('site', 'post', 'poll'),
            'actions' => array('index', 'view', 'login', 'page', 'contact', 'cat', 'login', 'downloaddoc', 'away', 'vote'),
        ),
        'cache' => array(
            'class' => 'system.caching.CDbCache',
        ),
        'widgetFactory' => array(
            'widgets' => array(
                'CBreadcrumbs' => array(
                    'encodeLabel' => false,
                ),
                'CJuiAccordion' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
                'CJuiDialog' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
                'CJuiButton' => array(
                    'themeUrl' => '/css/jui',
                    'theme' => 'redmond',
                ),
            ),
        ),
        'phpThumb' => array(
            'class' => 'ext.EPhpThumb.EPhpThumb',
        ),
//        'user' => array(
//            // enable cookie-based authentication
//            'allowAutoLogin' => true,
//        ),
        'db' => array(
            'connectionString' => 'sqlite:protected/data/blog.db',
            'tablePrefix' => 'tbl_',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            'enableParamLogging' => true,
        ),
        'db_sys' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:protected/data/sys_db.db',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => false,
            'enableParamLogging' => false,
        ),
        'db_poll' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'sqlite:protected/data/db_poll.db',
            'schemaCachingDuration' => 3600,
            'enableProfiling' => true,
            // показываем значения параметров
            'enableParamLogging' => true,
        ),
        'errorHandler' => array(
            // use 'site/error' action to display errors
            'errorAction' => 'site/error',
        ),
        'urlManager' => array(
            'showScriptName' => false,
            'urlFormat' => 'path',
            'rules' => array(
//                'special/<view>' => 'site/special',
                'post/<id:\d+>/<title:.*?>' => 'post/view',
                'doc/download/<id:\w+>/<filename:.*?>' => 'doc/download',
                'doc/<id:\d+>/<title:.*?>' => 'doc/view',
                'posts/<tag:.*?>' => 'post/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ),
        ),
        'log' => array(
            'class' => 'CLogRouter',
            'routes' => array(
                array(
                    'class' => 'CFileLogRoute',
                    'levels' => 'error, warning, info',
                ),
            ),
        ),
    ),
    'params' => array(
        'isLocal' => true,
        'isWeb' => true,
// this is displayed in the header section
        'title' => 'РосПрофЖел',
        'oldSiteLink' => 'http://old.dprof.ru/',
        'imageSize' => '800x600',
        'galleryImageSize' => '1600x1200',
        'thumbs' => array('200x200', '150x150'),
        'imgPath' => '/files/images/',
        'docPath' => '/files/docs/',
        // number of posts displayed per page
        'postsPerPage' => 10,
        'displayImportant' => 2,
        // maximum number of comments that can be displayed in recent comments portlet
        'recentCommentCount' => 10,
        // maximum number of tags that can be displayed in tag cloud portlet
        'tagCloudCount' => 20,
        // whether post comments need to be approved before published
        'commentNeedApproval' => true,
        // the copyright information displayed in the footer section
        'copyrightInfo' => 'Copyright &copy; 2009 by My Company.',
        'siteKey' => array(
            'public' => '6LdCDsAUAAAAAF378Xb2F38wp59s-4EyyEULoxJK',
            'private' => '6LdCDsAUAAAAAPfenCycpIJA9XDAVMIdjoztojXv',
        ),
        'resoEmails' => [
            'mzd_rpz@reso.ru',
            'noreply@dprof.ru',
        ],
        'contactEmails' => array(
            'dprof.mzd@mail.ru',
            'noreply@dprof.ru',
            'sheftelevich.dm@gmail.com',
        ),
        'publicEmail' => 'dprof.mzd@mail.ru',
        'publicSupportEmail' => 'pressdprof@mail.ru'
    )
);
