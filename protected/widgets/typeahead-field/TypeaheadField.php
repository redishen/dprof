<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
Yii::import('ext.bootstrap-typeahead.TbTypeAhead');

/**
 * Description of TypeaheadField
 *
 * @author asfto_sheftelevichdm
 */
class TypeaheadField extends TbTypeAhead {

    public $model;
    public $attribute;
    public $textAttribute;
    public $filterAttribute;
    public $url;
    public $options = array();
    public $defaultOptions = array();
    public $defaultBloodhoundOptions = array();
    public $defaultDatasetOptions = array();

    public function run() {
        $this->prepareOptions();

        $this->renderField();
        $this->registerClientScript();
        $this->registerCss();
    }

    public function prepareOptions() {
        $modelClass = get_class($this->model);
        $this->htmlOptions['id'] = $modelClass . '_' . $this->textAttribute;
    }

    public function renderField() {
        list($name, $id) = $this->resolveNameID();

        $model = $this->model;
        $attribute = $this->attribute;
        $modelClass = get_class($model);
        $options = $this->htmlOptions;
//        echo BsHtml::activeTextField($this->model, $this->textAttribute, $this->htmlOptions);

        $hiddenField = BsHtml::activeHiddenField($model, $attribute, array(
                    'id' => $modelClass . '_' . $attribute,
        ));

        $errorWrapper = BsHtml::tag('p', array(
                    'id' => $modelClass . '_' . $attribute . '_em_',
                    'style' => 'display:none',
                    'class' => 'help-block'
                        ), '');


        $helpOptions = BsArray::popValue('helpOptions', $options, array());
        $helpOptions['type'] = BsHtml::HELP_TYPE_BLOCK;
        $options['helpOptions'] = $helpOptions;

        $controlOptions = array_merge(array(
            'after' => "\n" . $hiddenField . "\n" . $errorWrapper,
                ), BsArray::popValue('controlOptions', $options, array()));
        $options['controlOptions'] = $controlOptions;

        $labelOptions = array(
            'class' => 'control-label',
        );
        $options['labelOptions'] = $labelOptions;

        if ($model->hasErrors($attribute)) {
            $error = BsHtml::tag('p', array(
                        'id' => $modelClass . '_' . $attribute . '_em_',
                        'class' => 'help-block'
                            ), $model->getError($attribute));
            $options['error'] = $error;
            $options['groupOptions']['class'] .= ' has-error';
        }

        echo BsHtml::activeTextFieldControlGroup($model, $this->textAttribute, $options);
    }

    /**
     * @return array the name and the ID of the input.
     * @throws CException in case input name and ID cannot be resolved.
     */
    protected function resolveNameID() {
        if ($this->name !== null)
            $name = $this->name;
        elseif (isset($this->htmlOptions['name']))
            $name = $this->htmlOptions['name'];
        elseif ($this->hasModel())
            $name = CHtml::activeName($this->model, $this->attribute);
        else
            throw new CException(Yii::t('yii', '{class} must specify "model" and "attribute" or "name" property values.', array('{class}' => get_class($this))));

        if (($id = $this->getId(false)) === null) {
            if (isset($this->htmlOptions['id']))
                $id = $this->htmlOptions['id'];
            else
                $id = CHtml::getIdByName($name);
        }

        return array($name, $id);
    }

    public function registerClientScript() {
        list($name, $id) = $this->resolveNameID();

        $options = $this->options;


        $options = CJavaScript::encode($options);

        $eventsScript = array();
        foreach ($this->events as $event => $expression) {
            $eventsScript[] = "{$id}.on('typeahead:{$event}',{$expression})";
        }
        $hiddenSelector = '#' . get_class($this->model) . '_' . $this->attribute;
        $eventsScript[] = "{$id}.on('typeahead:select',function (e, data) {var value = jQuery(this).parents('form').find('input$hiddenSelector'); value.val(data['id']); value.trigger('input');})";
        $eventsScript[] = "{$id}.on('typeahead:close',function () {if(jQuery(this).val() == '') jQuery(this).parents('form').find('input$hiddenSelector').val('')})";
        $eventsScript = implode("\n", $eventsScript);

        $cs = Yii::app()->getClientScript();

        $typeahead = YII_DEBUG === true ? 'typeahead.bundle.js' : 'typeahead.bundle.min.js';
        $cs->registerScriptFile($this->_assetsUrl . '/js/' . $typeahead, CClientScript::POS_END);

        $bloodhoundOpt = array(
            'remote' => array(
                'url' => $this->url,
                'wildcard' => '%QUERY',
                'rateLimitWait' => '1',
            ),
        );

        if ($this->filterAttribute) {
            $filterSelector = '#' . get_class($this->model) . '_' . $this->filterAttribute;
            
            Yii::app()->clientScript->registerScript(__CLASS__ . $this->getId() . '_filter', "
var obj = jQuery('#$id');
obj.parents('form').on('input', '{$filterSelector}', function() {
  obj.typeahead('open').focus();
});", CClientScript::POS_READY);

            $bloodhoundOpt['remote']['replace'] = new CJavaScriptExpression("
function(url, uriEncodedQuery) {
    var newUrl = url.replace('%QUERY', encodeURIComponent(uriEncodedQuery));
    newUrl += '&filter=' + jQuery('#$id').parents('form').find('{$filterSelector}').val();
    return newUrl;
}");
        }

        $bloodhoundOpt = array_merge($this->defaultBloodhoundOptions, $bloodhoundOpt);
        $bloodhoundOpt = CJavaScript::encode($bloodhoundOpt);

        $dataset = array(
            'name' => $id . '-dataset',
            'source' => new CJavaScriptExpression("new Bloodhound({$bloodhoundOpt})"),
        );
        $dataset = array_merge($this->defaultDatasetOptions, $dataset);
        $dataset = CJavaScript::encode($dataset);


        $options = array_merge($this->defaultOptions, $this->options);
        $options = CJavaScript::encode($options);

        Yii::app()->clientScript->registerScript(__CLASS__ . $this->getId(), "var {$id} = $('#" . $id . "').typeahead({$options}, {$dataset})\n$eventsScript", CClientScript::POS_READY);
    }

}
