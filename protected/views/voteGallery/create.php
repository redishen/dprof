<?php
$this->pageTitle = 'Новая запись';
$this->breadcrumbs = array(
    'Новая запись',
);
?>

<?php
echo $this->renderPartial('_form', array(
    'model' => $model,
    'files' => $files,
));
?>
