<? $pagination = $dataProvider->pagination ?>


<? if ($index % 3 == 0): ?>
    <div class="voting__row row">
<? endif; ?>
    <div class="voting__item col-md-4"
         data-image="<?= $data->image->url ?>"
         data-id="<?= $data->id ?>"
         data-name="<?= $data->name ?>"
         data-region="<?= Lookup::item('RegionMenu', $data->region) ?>"
         data-nomination="<?= Lookup::item('Nomination', $data->nomination)?>"
         data-vote-count="<?= $data->voteCountRel ?>"
         data-has-voted="<?= $data->hasVoted ?>"
    >
        <div class="voting__preview">
            <img class="voting__thumb" src="<?= $data->image->url ?>">
            <div class="voting__overlay"></div>
        </div>
        <div class="voting__heading"><?= CHtml::encode($data->name) ?></div>
        <div class="voting__body">
            <div class="voting__detail">Регион: <?= Lookup::item('RegionMenu', $data->region) ?></div>
            <div class="voting__detail">Номинация: <?= Lookup::item('Nomination', $data->nomination) ?></div>
            <div class="voting__detail">Голосов: <?= $data->voteCountRel ?></div>
            <div class="voting__buttons">
                <button class="dk-button voting__control voting__control_action_show-modal btn btn-warning" href="#"
                        data-id="0" data-toggle="modal" data-target="#voteModal">Посмотреть
                </button>
            </div>
        </div>
    </div>

<? if ($index % 3 == 2 || $index + 1 == ($pagination->itemCount - $pagination->currentPage * $pagination->pageSize) ): ?>
    </div>
<? endif; ?>