<?php
$this->pageTitle = array('Изменить', CHtml::encode($model->name));
$this->breadcrumbs = array(
    'Управление' => array('admin'),
    $model->name,
    'Изменить',
);
?>

<?php
echo $this->renderPartial('_form', array(
    'model' => $model,
    'files' => $files,
));
?>
