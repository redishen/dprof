<?php
$this->pageTitle = 'Управление записями';
$this->breadcrumbs = array(
    'Управление записями',
);
?>
<div class="row">
    <div class="col-xs-12">
        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'post-grid',
            'dataProvider' => $model->search(),
            'columns' => array(
                array(
                    'name' => 'name',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->name), $data->url)'
                ),
                array(
                    'name' => 'region',
                    'value' => 'Lookup::item("RegionMenu",$data->region)',
                    'filter' => Lookup::items('RegionMenu'),
                ),
                array(
                    'name' => 'nomination',
                    'value' => 'Lookup::item("Nomination",$data->nomination)',
                    'filter' => Lookup::items('Nomination'),
                ),
                array(
                    'name' => 'active',
                    'type' => 'boolean',
                ),
                array(
                    'name' => 'created',
                    'type' => 'datetime',
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                ),
            ),
        ));
        ?>
    </div>
</div>

