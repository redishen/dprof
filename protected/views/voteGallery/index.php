<?
$nominations = Lookup::items('Nomination');
array_unshift($nominations, 'Все');
$selectedNomination = $_GET['nomination'] ? $_GET['nomination'] : 0;
$regions = Lookup::items('RegionMenu');
array_unshift($regions, 'Все');
$selectedRegion = $_GET['region'] ? $_GET['region'] : 0;
?>
<div class="bs">
    <div class="voting voting_type_photo i-bem" data-bem='{"voting":{"voteUrl":"<?= Yii::app()->createAbsoluteUrl('voteGallery/vote') ?>"}}'>
        <h1 class="page-heading">Фотоконкурс к 15-летию ОАО «РЖД»</h1>
        <div class="container-fluid">
            <form class="voting__form container-fluid">
                <div class="row">
                    <div class="col col-md-4">
                        <label class="voting__label">Номинация</label>
                        <select class="voting__select" name="nomination">
                            <? foreach ($nominations as $key => $nomination): ?>
                                <option value="<?= $key ?>" <?= $selectedNomination == $key ? 'selected="selected"' : '' ?>><?= $nomination ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class="col col-md-4">
                        <label class="voting__label">Регион</label>
                        <select class="voting__select" name="region">
                            <? foreach ($regions as $key => $region): ?>
                                <option value="<?= $key ?>" <?= $selectedRegion == $key ? 'selected="selected"' : '' ?>><?= $region ?></option>
                            <? endforeach; ?>
                        </select>
                    </div>
                    <div class="col col-md-4">
                        <label class="voting__label"></label>
                        <button class="btn btn-primary">Показать</button>
                    </div>
                </div>
            </form>
            <div class="voting__items">
                <?php
                $this->widget('zii.widgets.CListView', array(
                    'dataProvider' => $dataProvider,
                    'itemView' => '_preview',
                    'pagerCssClass' => 'voting__pager',
                    'ajaxUpdate' => false,
                    'pager' => array(
                        'class' => 'CLinkPager',
                        'firstPageCssClass' => 'voting__pager-item',
                        'hiddenPageCssClass' => 'voting__pager-item_hidden',
                        'internalPageCssClass' => 'voting__pager-item',
                        'lastPageCssClass' => 'voting__pager-item',
                        'nextPageCssClass' => 'voting__pager-item',
                        'previousPageCssClass' => 'voting__pager-item',
                        'selectedPageCssClass' => 'voting__pager-item_active',
                    ),
                    'template' => "{items}\n{pager}",
                    'emptyText' => 'Нет элементов по выбранному фильтру',
                    'viewData' => array(
                        'dataProvider' => $dataProvider
                    )
                ));
                ?>
            </div>
        </div>
<!--        <div class="dk-modal dk-modal_overlay dk-modal_type_info dk-modal_alert voting__modal voting__modal_action_login modal fade i-bem" data-bem='{"dk-modal":{}}' id="loginModal" tabindex="-1" role="dialog" aria-hidden>-->
<!--            <div class="dk-modal__inner modal-dialog-centered modal-lg modal-dialog">-->
<!--                <div class="dk-modal__content modal-content">-->
<!--                    <div class="dk-modal__body modal-body">-->
<!--                        <div class="dk-modal__results text-center">-->
<!--                            <div class="title-box">-->
<!--                                <h3 class="dk-modal__results-text">Для голосования необходимо авторизоваться на сайте</h3>-->
<!--                            </div>-->
<!--                            <div class="dk-modal__results-content">-->
<!--                                <div class="voting__auth">-->
<!--                                    <div id="hoauthWidgetyw0" class="hoauthWidget">-->
<!--                                        <p>-->
<!--                                            <a href="/user/login/oauth/provider/Facebook" class="zocial  facebook">Войти через Facebook</a>-->
<!--                                        </p>-->
<!--                                        <p>-->
<!--                                            <a href="/user/login/oauth/provider/Vkontakte" class="zocial  vkontakte">Войти через Vkontakte</a>-->
<!--                                        </p>-->
<!--                                    </div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                            <div class="dk-button dk-button_color_blue btn btn-secondary" href="#" data-dismiss="modal">Закрыть</div>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="dk-modal__overlay">-->
<!--                        <div class="dk-loading">-->
<!--                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="10" viewBox="0 0 36 10">-->
<!--                                <path id="loading" class="cls-1" d="M609,425a5,5,0,1,1-5,5A5,5,0,0,1,609,425Zm13,0a5,5,0,1,1-5,5A5,5,0,0,1,622,425Zm13,0a5,5,0,1,1-5,5A5,5,0,0,1,635,425Z" transform="translate(-604 -425)" />-->
<!--                            </svg>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
        <div class="dk-modal dk-modal_overlay dk-modal_type_info dk-modal_alert voting__modal voting__modal_action_vote modal fade i-bem" data-bem='{"dk-modal":{}}' id="voteModal" tabindex="-1" role="dialog" aria-hidden>
            <div class="dk-modal__inner modal-dialog-centered modal-lg modal-dialog">
                <div class="dk-modal__content modal-content">
                    <div class="dk-modal__body modal-body">
                        <div class="dk-modal__results text-center">
                            <div class="title-box">
                                <h3 class="dk-modal__results-text" data-name="name">Молодежь за достойные условия труда</h3>
                            </div>
                            <div class="dk-modal__results-content">
                                <div class="row">
                                    <div class="col col-md-8">
                                        <div class="voting__modal-image"><img class="voting__image img img-fluid" src="http://dprof.lo/files/voting/1.jpg">
                                        </div>
                                    </div>
                                    <div class="col col-md-4">
                                        <div class="voting__modal-details">
                                            <div class="voting__detail">
                                                <div class="voting__detail-label">Регион:</div>
                                                <div class="voting__detail-value" data-name="region">Московско-Курское РОП</div>
                                            </div>
                                            <div class="voting__detail">
                                                <div class="voting__detail-label">Номинация:</div>
                                                <div class="voting__detail-value" data-name="nomination">Молодежь за достойные условия труда</div>
                                            </div>
                                            <div class="voting__detail">
                                                <div class="voting__detail-label">Голосов:</div>
                                                <div class="voting__detail-value" data-name="vote-count">0</div>
                                            </div>
                                        </div>
<!--                                        <button class="dk-button voting__control voting__control_action_vote btn btn-warning" href="#">Я голосую за эту работу</button>-->
                                        <div class="dk-button btn btn-secondary" href="#" data-dismiss="modal">Закрыть</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dk-modal__overlay">
                        <div class="dk-loading">
                            <svg xmlns="http://www.w3.org/2000/svg" width="36" height="10" viewBox="0 0 36 10">
                                <path id="loading" class="cls-1" d="M609,425a5,5,0,1,1-5,5A5,5,0,0,1,609,425Zm13,0a5,5,0,1,1-5,5A5,5,0,0,1,622,425Zm13,0a5,5,0,1,1-5,5A5,5,0,0,1,635,425Z" transform="translate(-604 -425)" />
                            </svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

