<?
/**
 * @var Banner $model
 * @var BsActiveForm $form
 *
 * */
?>
<div class="form col-xs-10 col-xs-offset-1">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->textFieldControlGroup($model, 'name', array('maxlength' => 255)); ?>

    <div class="form-group">
        <?php echo $form->label($model, 'image'); ?>
        <?php
        $this->widget('xupload.FileUpload', array(
                'url' => $this->createUrl('upload'),
                'showForm' => false,
                'model' => $files,
                'attribute' => 'file',
                'multiple' => false,
                'autoUpload' => true,
                'formView' => 'application.views.post._uploadform',
                'htmlOptions' => array('id' => 'post-form'),
                'options' => array(
                    'maxFileSize' => 300000000,
                    'disableImageResize' => 'js:/Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)',
                    'previewMaxWidth' => 150,
                    'previewMaxHeight' => 150,
                    'previewCrop' => true
                )
            )
        );
        ?>
    </div>


    <?php
    echo $form->dropDownListControlGroup($model, 'region', Lookup::items('RegionMenu'));
    ?>

    <?php
    echo $form->dropDownListControlGroup($model, 'nomination', Lookup::items('Nomination'));
    ?>

    <?= $form->checkBoxControlGroup($model, 'active'); ?>


    <? if ($model->isNewRecord): ?>
        <?php echo BsHtml::inputSubmitButton('Сохранить и добавить', array('name' => 'submit-and-create')); ?>
    <? endif; ?>

    <?php echo BsHtml::inputSubmitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('id' => 'submit')); ?>

    <?php $this->endWidget(); ?>
</div>
