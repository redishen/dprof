<?php
$this->pageTitle = Yii::app()->name . ' - ' . Yii::t('ui', 'Manage Menus');

$this->breadcrumbs = array(
    Yii::t('ui', 'Menus') => array('index'),
    Yii::t('ui', 'Manage'),
);

$this->menu = array(
    array('label' => Yii::t('ui', 'New'), 'url' => $this->createReturnableUrl('create', array('id' => $model->id))),
    array('label' => Yii::t('ui', 'Tree'), 'url' => '#', 'linkOptions' => array('onclick' => '$("#tree-dialog").dialog("open");return false;')),
    array('label' => Yii::t('ui', 'Menu'), 'url' => array('adminMenu')),
);
?>

<h2><?php echo Yii::t('ui', 'Manage Menus'); ?></h2>

<div class="actionBar">
    <?php $this->widget('ext.widgets.amenu.XActionMenu', array(
        'items' => $this->menu,
    )); ?>
    <?php echo $this->renderPartial('_search'); ?>
</div><!-- actionBar -->

<div class="breadcrumbs">
    <?php echo $model->breadcrumbs; ?>
</div><!-- breadcrumbs -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'menu-grid',
    'dataProvider' => $dataProvider,
    'hideHeader' => true,
    'enableSorting' => false,
    'summaryText' => false,
    'columns' => array(
        'id',
        array(
            'type' => 'raw',
            'name' => 'parent_id',
            'value' => 'CHtml::link(CHtml::encode($data->parent_id), array("admin","id"=>$data->id))',
        ),
        'label',
        'position',
        array(
            'class' => 'CButtonColumn',
            'viewButtonUrl' => '$this->grid->controller->createReturnableUrl("view",array("id"=>$data->id))',
            'updateButtonUrl' => '$this->grid->controller->createReturnableUrl("update",array("id"=>$data->id))',
            'template' => '{view} {update} {delete}',
        ),
    ),
)); ?>

