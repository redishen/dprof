<?php

Yii::app()->clientScript->registerScript('formTree', "
	$('#menu-treeview').on('click', 'a',function(){
		var id=$(this).attr('id');
		$('#parent-id').val(id);
		$.ajax({
			url:'" . $this->createUrl('/category/ajaxPath') . "',
			data:{'id':id},
			cache:false,
			success:function(data){return $('#cat').append('<option value='+id+'>'+data+'</option>');}
		});
		$('#tree-dialog').dialog('close');
		return false;
	});

");
Yii::app()->clientScript->registerScript('submitSelect', "
    $('#submit').mouseup(function(){
        $('#cat option').each(function(){
        this.selected=true;
    });
    return false;
});
    ");

Yii::app()->clientScript->registerScript('updateChange', '$("#postType").change(
            function(){
            if(this.value != 3)
            $("#updTime").slideUp();
            else
            $("#updTime").slideDown();
            })');
?>

<div class="form col-xs-10 col-xs-offset-1">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <fieldset>
        <?php echo $form->textFieldControlGroup($model, 'title', array('maxlength' => 128)); ?>

        <?php
        echo $form->dropDownListControlGroup($model, 'categories', $model->categoriesList, array(
            'multiple' => true,
            'id' => 'cat',
            'class' => 'col-xs-6',
        ));
        ?>
        <div class="form-group">
            <?php
            echo BsHtml::button('', array(
                'icon' => BsHtml::GLYPHICON_PLUS,
                'onclick' => '$("#tree-dialog").dialog("open");return false;',
            ))
            ?>
            <?php
            echo BsHtml::button('', array(
                'icon' => BsHtml::GLYPHICON_MINUS,
                'onclick' => '$("#cat :selected").remove();return false;',
            ))
            ?>
        </div>

        <div class="form-group">
            <?php echo $form->label($model, 'tags'); ?>
            <?php
            $this->widget('CAutoComplete', array(
                'model' => $model,
                'attribute' => 'tags',
                'url' => array('suggestTags'),
                'multiple' => true,
                'htmlOptions' => array('size' => 50, 'class' => 'form-control'),
            ));
            ?>
            <p class="hint">Введите, разделяя запятыми.</p>
            <?php echo $form->error($model, 'tags'); ?>
        </div>

        <?php
        echo $form->textAreaControlGroup($model, 'preview_text', array(
            'rows' => 5,
        ));
        ?>


        <div class="form-group">
            <?= $form->label($model, 'content') ?>
            <div>
                <?php
                // in view
                $this->widget('ext.tinymce.TinyMce', array(
                    'model' => $model,
                    'attribute' => 'content',
                    'htmlOptions' => array(
                        'rows' => 20,
                    ),
                ));
                ?>
                <?= $form->error($model, 'content') ?>
            </div>
        </div>
    </fieldset>
    <fieldset>
        <legend>Связи</legend>
        <?php echo $form->textFieldControlGroup($model, 'related_posts', array('maxlength' => 128, 'help' => 'ID постов через запятую без пробелов')); ?>
    </fieldset>


    <fieldset>
        <legend>Изображения</legend>
        <?php if (!empty($model->postImages)): ?>
            <div class="form-group">
                <?php foreach ($model->postImages as $image): ?>
                    <div class="col-xs-4 thumbnail">
                        <?= CHtml::link('/files/images/' . $image->file_name, Yii::app()->request->baseUrl . '/files/images/' . $image->file_name) ?>
                        <br>
                        <?=
                        BsHtml::image('/files/images/thumbs/150x150/' . $image->file_name, '', array(
                            'type' => 'responsible'
                        ));
                        ?>
                        <!--<br>-->
                        <?=
                        BsHtml::ajaxButton('Удалить', array(
                            'ajaxDeleteImg',
                            'id' => $image->file_name), array(
                            'success' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("Удалено")}, this)'),
                            'error' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("ОШИБКА УДАЛЕНИЯ")}, this)')), array(
                                'confirm' => 'Вы действительно хотите удалить элемент?',
                                'size' => 'xs',
                                'class' => 'pull-right',
                                'icon' => BsHtml::GLYPHICON_TRASH,
                                'style' => 'margin-top: 5px',
                            )
                        );
                        ?>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            </div>
        <?php endif; ?>


        <div class="form-group">
            <?php echo $form->label($model, 'images'); ?>
            <?php
            $this->widget('xupload.FileUpload', array(
                    'url' => $this->createUrl('upload'),
                    'showForm' => false,
                    'model' => $images,
                    'attribute' => 'file',
                    'multiple' => true,
                    'autoUpload' => true,
                    'formView' => 'application.views.post._uploadform',
                    'htmlOptions' => array('id' => 'post-form'),
                    'options' => array(
                        'maxFileSize' => 20000000,
                        'acceptFileTypes' => 'js:/(\.|\/)(gif|jpe?g|png)$/i',
                        'disableImageResize' => 'js:/Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)',
                        'previewMaxWidth' => 150,
                        'previewMaxHeight' => 150,
                        'previewCrop' => true
                    )
                )
            );
            ?>
        </div>
    </fieldset>

    <fieldset>
        <legend>Документы</legend>
        <?php if (!empty($model->postDocuments)): ?>
            <div class="form-group">
                <ul class="list-unstyled list_type_doc">
                    <?php foreach ($model->postDocuments as $doc): ?>
                        <li class="row list__item" data-id="<?= $doc->id ?>" data-name="<?= $doc->file_name ?>">
                            <div class="col-xs-5 list__label ">
                                <?= $doc->file_name ?>
                            </div>

                            <div class="col-xs-2 pull-right list__actions">
                                <?=
                                BsHtml::button('Переименовать', array(
                                        'class' => 'list__button_action_rename',
                                        'size' => 'xs',
                                        'icon' => BsHtml::GLYPHICON_PENCIL,
                                    )
                                );
                                ?>
                                <?=
                                BsHtml::ajaxButton('Удалить', array(
                                    'ajaxDeleteDoc',
                                    'id' => $doc->id),
                                    array(
                                        'success' => new CJavaScriptExpression('$.proxy(function(){$(this).parents(".list__item").remove()}, this)'),
                                        'error' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("ОШИБКА УДАЛЕНИЯ")}, this)')), array(
                                        'confirm' => 'Вы действительно хотите удалить элемент?',
                                        'size' => 'xs',
                                        'icon' => BsHtml::GLYPHICON_TRASH,
                                        'class' => 'list__button_action_delete',
                                    )
                                );
                                ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?php
            echo $form->fileField($model, 'documents[]', array('multiple' => 'multiple'));
            ?>
        </div>
    </fieldset>

    <fieldset>
        <legend>Видео</legend>
        <?php echo $form->textFieldControlGroup($model, 'videoLocal', array('maxlength' => 128)); ?>
        <?php echo $form->textFieldControlGroup($model, 'videoHref', array('maxlength' => 128)); ?>
    </fieldset>


    <fieldset>
        <legend>Событие</legend>
        <?php
        echo $form->checkBoxControlGroup($model, 'is_event');
        ?>
        <script>
            $(document).ready(function () {
                var clpse = $('#collapseEvent');
                clpse.collapse();

                $("[name='Post[is_event]']").bind('click dblclick', function (evt) {
                    console.log(evt.type);

                    if ($(this).is(":checked")) {
                        clpse.collapse("show")
                    } else {
                        clpse.collapse("hide")
                    }
                })
            })
        </script>
        <div id="collapseEvent" class="collapse <?= $model->is_event ? '' : 'in' ?>">
            <div class="form-group">
                <?= $form->label($model, 'event_from') ?>
                <?php
                $this->widget('CMaskedTextField', array(
                    'model' => $model,
                    'attribute' => 'event_from',
                    'mask' => '9999-99-99 99:99',
                    'htmlOptions' => array('class' => 'form-control'),
                ));
                ?>
                <?= $form->error($model, 'event_to') ?>
            </div>
            <div class="form-group">
                <?= $form->label($model, 'event_to') ?>
                <?php
                $this->widget('CMaskedTextField', array(
                    'model' => $model,
                    'attribute' => 'event_to',
                    'mask' => '9999-99-99 99:99',
                    'htmlOptions' => array('class' => 'form-control'),
                ));
                ?>
                <?= $form->error($model, 'event_to') ?>
            </div>
            <?php
            echo $form->textAreaControlGroup($model, 'event_note', array(
                'rows' => 2,
            ));
            ?>
        </div>

    </fieldset>

    <fieldset>
        <legend>Параметры размещения</legend>
        <?php
        echo $form->checkBoxControlGroup($model, 'on_main');
        ?>

        <?php
        echo BsHtml::dropDownListControlGroup('postType', 0, array('Сегодня', 'Вчера', 'Завтра', 'Дата'), array('label' => 'Разместить'))
        ?>

        <div class="form-group" id='updTime' style='display: none'>
            <?= $form->label($model, 'update_time') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'update_time',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'update_time') ?>
        </div>

        <?php
        echo $form->dropDownListControlGroup($model, 'status', Lookup::items('PostStatus'));
        ?>
    </fieldset>


    <?php echo BsHtml::inputSubmitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('id' => 'submit')); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<?php
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id' => 'tree-dialog',
    'options' => array(
        'title' => 'Выберите из списка',
        'width' => 500,
        'height' => 500,
        'autoOpen' => false,
        'modal' => true,
    )
));
echo $this->renderPartial('/category/_tree');
$this->endWidget('zii.widgets.jui.CJuiDialog');
?>

<script>
    $(function () {
        $(document).on('click', '.list__button_action_rename', function () {
            var that = $(this),
                item = that.parents('.list__item'),
                label = item.children('.list__label'),
                id = item.data('id'),
                name = item.data('name');

            var wrap = $('<div class="col-xs-5 list__actions_type_rename"></div>'),
                input = $('<input type="text" class="form-control input-sm list__input" value="' + name + '"/>'),
                ok = $('<button class="btn btn-default btn-xs list__button_action_ok" ><span class="glyphicon glyphicon-ok"></span></button>'),
                no = $('<button class="btn btn-default btn-xs list__button_action_no" ><span class="glyphicon glyphicon-remove"></span></button>');

            that.attr('disabled', true);

            wrap.append(input);
            wrap.append(ok);
            wrap.append(no);
            label.after(wrap);


            no.click(function () {
                wrap.remove();
                that.removeAttr('disabled');
            });

            ok.click(function (e) {
                e.preventDefault();
                $.ajax({
                    url: '/doc/ajaxRenameDoc',
                    data: {
                        id: id,
                        name: input.val()
                    }
                }).done(function (data) {
                    label.html(input.val());
                    item.data('name', input.val());
                    wrap.remove();
                    that.removeAttr('disabled');
                }).fail(function (data) {
                    wrap.remove();
                    that.text(data.responseText)
                });
            })


        })
    });
</script>
