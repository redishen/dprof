<div class="post">
    <div class="title" >
        <?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
        <?php echo CHtml::link($data->viewCount, '', array('id' => 'counter', 'title' => "Просмотры - {$data->viewCount}")) ?>
    </div>
    <div class="content">
        <?php if (!empty($data->postVideos) && !empty($data->postVideos[0]->href) && !empty($data->postVideos[0]->local)): ?>
            <div class="post__video_preview">
                <?php
                $this->widget('VideoWidget', array(
                    'videoModel' => $data->postVideos[0]
                ));
                ?>
            </div> 
        <?php endif; ?>

        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput' => false));

        echo $data->preview_text;

        echo ' ' . CHtml::link('<i>читать далее</i>', $data->url);
        $this->endWidget();
        ?>

    </div>
    <!--    <div >
    <?php if (!empty($data->tagLinks)) : ?>
                                        <b>Тэги:</b>
        <?php echo implode(', ', $data->tagLinks); ?>
                                        <br/>
    <?php endif; ?>
        </div>-->
</div>