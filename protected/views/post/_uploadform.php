<!-- The file upload form used as target for the file upload widget -->
<?php if ($this->showForm) echo CHtml::beginForm($this->url, 'post', $this->htmlOptions); ?>
<!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
<div class="row fileupload-buttonbar">
    <div class="col-lg-7">
        <!-- The fileinput-button span is used to style the file input field as button -->
        <span class="btn btn-success fileinput-button">
            <i class="glyphicon glyphicon-plus"></i>
            <span>Добавить файлы...</span>
            <?php
            if ($this->hasModel()) :
                echo BsHtml::activeFileField($this->model, $this->attribute, $htmlOptions) . "\n";
            else :
                echo BsHtml::fileField($name, $this->value, $htmlOptions) . "\n";
            endif;
            ?>
        </span>
        <!-- The global file processing state -->
        <span class="fileupload-process"></span>
    </div>
    <!-- The global progress state -->
    <div class="col-lg-5 fileupload-progress fade">
        <!-- The global progress bar -->
        <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            <div class="progress-bar progress-bar-success" style="width:0%;"></div>
        </div>
        <!-- The extended global progress state -->
        <div class="progress-extended">&nbsp;</div>
    </div>
</div>
<!-- The table listing the files available for upload/download -->
<table role="presentation" class="table table-striped"><tbody class="files"></tbody></table>
    <?php if ($this->showForm) echo CHtml::endForm(); ?>


<script>

//    $(function () {
//        'use strict';
//
//        // Initialize the jQuery File Upload widget:
//        $('#fileupload').fileupload({
//            // Uncomment the following to send cross-domain cookies:
//            //xhrFields: {withCredentials: true},
//            url: 'server/php/'
//        });
//
//        // Enable iframe cross-domain access via redirect option:
//        $('#fileupload').fileupload(
//                'option',
//                'redirect',
//                window.location.href.replace(
//                        /\/[^\/]*$/,
//                        '/cors/result.html?%s'
//                        )
//                );
//
//        if (window.location.hostname === 'blueimp.github.io') {
//            // Demo settings:
//            $('#fileupload').fileupload('option', {
//                url: '//jquery-file-upload.appspot.com/',
//                // Enable image resizing, except for Android and Opera,
//                // which actually support image resizing, but fail to
//                // send Blob objects via XHR requests:
//                disableImageResize: /Android(?!.*Chrome)|Opera/
//                        .test(window.navigator.userAgent),
//                maxFileSize: 999000,
//                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
//            });
//            // Upload server status check for browsers with CORS support:
//            if ($.support.cors) {
//                $.ajax({
//                    url: '//jquery-file-upload.appspot.com/',
//                    type: 'HEAD'
//                }).fail(function () {
//                    $('<div class="alert alert-danger"/>')
//                            .text('Upload server currently unavailable - ' +
//                                    new Date())
//                            .appendTo('#fileupload');
//                });
//            }
//        } else {
//            // Load existing files:
//            $('#fileupload').addClass('fileupload-processing');
//            $.ajax({
//                // Uncomment the following to send cross-domain cookies:
//                //xhrFields: {withCredentials: true},
//                url: $('#fileupload').fileupload('option', 'url'),
//                dataType: 'json',
//                context: $('#fileupload')[0]
//            }).always(function () {
//                $(this).removeClass('fileupload-processing');
//            }).done(function (result) {
//                $(this).fileupload('option', 'done')
//                        .call(this, $.Event('done'), {result: result});
//            });
//        }
//
//    });
</script>