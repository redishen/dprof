<div class="post">
    <div class="post__title">
        <?php echo CHtml::link(CHtml::encode($data->title), $data->url, array('class' => 'post__link')); ?>

        <?php if (!Yii::app()->user->isGuest) : ?>
            <?php
            $this->widget('zii.widgets.jui.CJuiButton', array(
                'name' => 'button' . $data->id,
                'caption' => 'Изменить',
                'options' => array(
                    'icons' => array(
                        'primary' => 'ui-icon-pencil',
                    ),
                ),
                'url' => array('update', 'id' => $data->id),
                'buttonType' => 'link',
                'htmlOptions' => array('class' => 'post__btn_action_edit'),
            ));
            ?>
        <?php endif; ?>
    </div>


    <div class="post__author">
        размещено <?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $data->update_time); ?>
    </div>

    <div class="content">
        <strong>
            <?php
            $this->beginWidget('CMarkdown', array('purifyOutput' => false));
            echo $data->preview_text;
            $this->endWidget();
            ?>
        </strong>
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput' => true));
        echo $data->content;
        $this->endWidget();
        ?>

        <?php if (!empty($data->postVideos) && !empty($data->postVideos[0]->href) && !empty($data->postVideos[0]->local)): ?>
            <div class="post__video_detail">
                <?php
                $this->widget('VideoWidget', array(
                    'videoModel' => $data->postVideos[0]
                ));
                ?>
            </div>
        <?php endif; ?>

        <?php if (!empty($data->postDocuments)): ?>
            <h2 class="heading heading_type_attach" id="files"><i class="fa fa-files-o" aria-hidden="true"></i> Приложенные
                документы </h2>
            <div class="list_type_doc">
                <? foreach ($data->postDocuments as $doc): ?>
                    <a class="list__item" target="_blank"
                       href="<?= $doc->getUrl() ?>">
                        <div class="list__preview" style="background-image: url('<?= $doc->previewUrl ?>');"></div>
                        <div class="list__label">
                            <?= $doc->file_name ?>
                        </div>
                        <div class="list__action">
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </div>
                    </a>
                <? endforeach; ?>
            </div>
        <?php endif; ?>
    </div>

    <?php if ($related = $data->relatedPosts): ?>
        <div class="post__related">
            <h2 class="heading heading_type_related"><i class="fa fa-link" aria-hidden="true"></i> Связанные записи
            </h2>
            <div class="list_type_related">
                <? foreach ($related as $post): ?>
                    <a class="list__item"
                       href="<?= $post->getUrl() ?>">
                        <div class="list__preview">
                            <? if ($post->is_doc): ?>
                                <i class="fa  fa-file-archive-o" aria-hidden="true"></i>
                            <? else: ?>
                                <i class="fa  fa-file-text-o" aria-hidden="true"></i>
                            <? endif; ?>

                        </div>
                        <div class="list__label">
                            <div class="list__heading">
                                <?= $post->title ?>
                            </div>
                            <div class="list__subheading">
                                <? if ($post->is_doc): ?>
                                    Документы >
                                <? else: ?>
                                    Анонсы и новости >
                                <? endif; ?>
                                <? foreach ($post->getCategoriesList() as $cat): ?>
                                    <?= $cat ?>
                                <? endforeach; ?>

                            </div>
                        </div>

                    </a>
                <? endforeach; ?>
            </div>
        </div>
    <?php endif; ?>


    <div class="nav">
        <b>Тэги:</b>
        <?php echo implode(', ', $data->tagLinks); ?>
        <br/>

        <?php echo "Просмотры -  {$data->viewCount}"; ?> |
        Последний раз обновлено <?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $data->update_time); ?>
        <span style="text-align: right">
            <?php
            $this->widget('ext.mPrint.mPrint', array(
                'title' => $data->title,
                'element' => '.post',
                'exceptions' => array(
                    '.nav',
                    '.title',
                ),
                'publishCss' => true,
            ));
            ?>
        </span>
    </div>
</div>
