<?php
//$this->breadcrumbs = array(
//    $model->title,
//);

header("Access-Control-Allow-Origin: *");
$this->breadcrumbs = $model->getBreadcrumbs();

$this->pageTitle = $model->title . ' - ' . Yii::app()->name;
?>

<?php
$this->renderPartial('_view', array(
    'data' => $model,
));
?>

<?php if (!empty($model->postImages)): ?>
    <div id="galery">
        <h1>Галерея</h1>
        <?php
        foreach ($model->getImagesLinks() as $image) {
            $this->widget('ext.lyiightbox.LyiightBox2', array(
                'thumbnail' => $image['smallest'],
                'image' => $image['biggest'],
                    //'title' => 'Sample Image 1.'
            ));
        }
        ?>
    </div>
<?php endif; ?>
