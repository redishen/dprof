<?php if (isset($catName)): ?>
    <h1 class="heading heading_type_cat"><?php echo CHtml::encode($catName); ?></h1>
    <?php $this->pageTitle = $catName . ' - ' . Yii::app()->name;?>
<?php elseif (!empty($_GET['tag'])): ?>
    <h1 class="heading heading_type_tag">Записи, отмеченные тэгом <i>"<?php echo CHtml::encode($_GET['tag']); ?>"</i></h1>
    <?php $this->pageTitle = 'Тэг: ' . $_GET['tag'] . ' - ' . Yii::app()->name;?>
<?php else: ?>
    <h1 class="heading heading_type_cat">Анонсы и новости</h1>
    <?php $this->pageTitle = 'Новости - ' . Yii::app()->name;?>
<?php endif; ?>

<?php
$this->widget('ext.widgets.DListView.DListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_preview',
    'template' => "{items}\n{pager}",
    'emptyText' => 'Нет статей по этой категории'
));
?>
<br>
<br>