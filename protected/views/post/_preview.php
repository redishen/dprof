<?php ?>
<div class="post">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
        <?php echo CHtml::link($data->viewCount, '', array('id' => 'counter', 'title' => "Просмотры - {$data->viewCount}")) ?>
    </div>
    <div class="author">
        размещено <?php echo Yii::app()->dateFormatter->format('d MMMM yyyy', $data->update_time); ?>
    </div>
    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput' => false));
        echo $data->preview_text;
        $this->endWidget();
        ?>
    </div>
    <div class="nav">
        <?php if (!empty($data->tagLinks)) : ?>
            <b>Тэги:</b>
            <?php echo implode(', ', $data->tagLinks); ?>
            <br/>
        <?php endif; ?>

        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'name' => 'button' . $data->id,
            'caption' => 'Подробнее',
            'url' => $data->url,
            'buttonType' => 'link',
            'htmlOptions' => array('class' => 'btn-more',),
            'options' => array(
                'icons' => array(
//                        'primary' => 'ui-icon-arrow-1-e',
                    'secondary' => 'ui-icon-arrow-1-e',
                ),
            ),
        ));
        ?> | Обновлён <?php echo date('F j, Y', $data->update_time); ?>
    </div>
</div>
