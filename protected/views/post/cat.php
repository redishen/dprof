<?php  if (isset($catName)): ?>
<h1 ><i><?php echo CHtml::encode($catName);?></i></h1>
<?php endif; ?>
<?php if (!empty($_GET['tag'])): ?>
    <h1>Записи, отмеченные тэгом <i>"<?php echo CHtml::encode($_GET['tag']); ?>"</i></h1>
<?php endif; ?>

<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_preview',
    'template' => "{items}\n{pager}",
    'emptyText' => 'Нет статей по этой категории'
));
?>