<?php 
$postId = 1142;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title"><small>Первичная профсоюзная организация </small><br>АО «Центральная ППК»</h1>

<p class="reg-contacts">
    ФАКС 4-02-65 доб. 7233<br>
    cppk-profkom@list.ru<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/cppk/budeeva.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/cppk/budeeva.jpg" border="0"></a>
    <h3 class="reg-preson-name">Будеева <small>Ирина Валентиновна</small></h3>
    председатель первичной профсоюзной организации ОАО «Центральная ППК»
    <div class="clear"></div>
</div>

<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
