<?php
$tagId = 155;
$postId = 5613;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;

?>
<h1 class="page-title">Студенческая <br><small>первичная профсоюзная организация РУТ (МИИТ)</small></h1>

<p class="reg-contacts">
    ВК: <a href="https://vk.com/profmiit" target="_blank">https://vk.com/profmiit</a><br>
    Инстаграм: <a href="https://www.instagram.com/prof_miit" target="_blank">https://www.instagram.com/prof_miit</a><br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<?php
$persons = array(
    array(
        'surname' => 'Климчук',
        'name' => 'Руслан Иванович',
        'pos' => 'председатель СППО РУТ (МИИТ)',
        'pic' => 'klimchuk',
    ),
    array(
        'surname' => 'Потапова',
        'name' => 'Юлия Сергеевна',
        'pos' => 'заместитель председателя СППО РУТ (МИИТ)',
        'pic' => 'potapova',
    ),
    array(
        'surname' => 'Синицын',
        'name' => 'Николай Дмитриевич',
        'pos' => 'заместитель председателя СППО РУТ (МИИТ)',
        'pic' => 'sinitsin',
    ),
    array(
        'surname' => 'Червоненко',
        'name' => 'Алина Олеговна',
        'pos' => 'председатель ФПОС ИУЦТ',
        'pic' => 'chervonenko',
    ),
    array(
        'surname' => 'Трошина',
        'name' => 'Кристина Алексеевна',
        'pos' => 'председатель ФПОС ИТТСУ',
        'pic' => 'troshina',
    ),
    array(
        'surname' => 'Пятигорский',
        'name' => 'Борис Алексеевич',
        'pos' => 'председатель ФПОС ИЭФ',
        'pic' => 'piatigorsky',
    ),
    array(
        'surname' => 'Спешков',
        'name' => 'Андрей Андреевич',
        'pos' => 'председатель ФПОС ЮИ',
        'pic' => 'speshkov',
    ),
    array(
        'surname' => 'Колузаев',
        'name' => 'Вадим Анатольевич',
        'pos' => 'председатель ФПОС ИПСС        ',
        'pic' => 'koluzaev',
    ),
    array(
        'surname' => 'Шмитов',
        'name' => 'Максим Вадимович',
        'pos' => 'председатель ФПОС ИМТК',
        'pic' => 'shmitov',
    ),
    array(
        'surname' => 'Лемешева',
        'name' => 'Елена Александровна',
        'pos' => 'председатель информационно-справочной комиссии        ',
        'pic' => 'lemesheva',
    ),
    array(
        'surname' => 'Гусихина',
        'name' => 'Екатерина Сергеевна',
        'pos' => 'председатель социально-образовательной комиссии        ',
        'pic' => 'gusihina',
    ),

);

?>

<?php foreach($persons as $person):?>
    <div class="reg-person">
        <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" border="0"></a>
        <h3 class="reg-preson-name"><?php echo $person['surname']; ?> <small><?php echo $person['name']; ?></small></h3>
            <?php echo $person['pos']; ?>
        <div class="clear"></div>
    </div>
<?php endforeach;?>


<h2 class="reg-subtitle">Новости СППО РУТ (МИИТ)</h2>

<hr class="space" />

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
