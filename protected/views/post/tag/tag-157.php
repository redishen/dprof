<?php
$postId = 1134;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Московско-Смоленское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    25047, Москва, пл. Тверской заставы, 5 корп. 1<br>
    e-mail: terkom6@mail.ru<br>
    факс: 8-499-623-80-63<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/d.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/d.jpg" border="0"></a>
    <h3 class="reg-preson-name">Домбровский <small>Игорь Андреевич</small></h3>
    заместитель председателя Дорпрофжел – руководитель Московско-Смоленского регионального обособленного подразделения Дорпрофжел на Московской ж.д.
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/goriacheva-n-e.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/goriacheva-n-e.jpg" border="0"></a>
    <h3 class="reg-preson-name">Горячева <small>Наталья Евгеньевна</small></h3>
    правовой инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/kulistikov-v-s.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/kulistikov-v-s.jpg" border="0"></a>
    <h3 class="reg-preson-name">Кулистиков <small>Вячеслав Сергеевич</small></h3>
    технический инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/volobueva-v-a.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/volobueva-v-a.jpg" border="0"></a>
    <h3 class="reg-preson-name">Волобуева <small>Валентина Александровна</small></h3>
    заместитель заведующего отделом организационной и кадровой работы Дорпрофжел
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/dubnikova-n-v.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/dubnikova-n-v.jpg" border="0"></a>
    <h3 class="reg-preson-name">Дубникова <small>Наталья Вячеславовна</small></h3>
    ведущий специалист (по Калужскому региону)
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/spasibkina-s-m.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/spasibkina-s-m.jpg" border="0"></a>
    <h3 class="reg-preson-name">Спасибкина <small>Светлана Михайловна</small></h3>
    заместитель заведующего финансовым отделом Дорпрофжел на Московско-Смоленском регионе
    <div class="clear"></div>
</div>
<!---->
<!--<div class="reg-person">-->
<!--    <a class="reg-preson-pic" href="/files/site/reg/m-s/dejkun-o-n.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/dejkun-o-n.jpg" border="0"></a>-->
<!--    <h3 class="reg-preson-name">Дейкун <small>Ольга Николаевна</small></h3> -->
<!--    специалист – кассир-->
<!--    <div class="clear"></div>-->
<!--</div>-->

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/galibina-t-a.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/galibina-t-a.jpg" border="0"></a>
    <h3 class="reg-preson-name">Жалыбина <small>Татьяна Александровна</small></h3>
    ведущий специалист бухгалтер
    <div class="clear"></div>
</div>

<!--<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-s/" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-s/" border="0"></a>
    <h3 class="reg-preson-name"> <small></small></h3> 
    
    <div class="clear"></div>
</div>-->




<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space" />

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
