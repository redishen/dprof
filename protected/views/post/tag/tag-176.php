<?php 
$tagId = 176;
$postId = 1140;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Брянское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    241020, г.Брянск ул. 2-я Аллея д.22<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<?php
$persons = array(
    array(
        'surname' => 'Слабыня',
        'name' => 'Анатолий Федорович',
        'pos' => 'руководитель Брянского регионального обособленного подразделения Дорпрофжел на Московской ж.д.',
        'pic' => 's',
    ),
);
?>

<?php foreach($persons as $person):?>
    <div class="reg-person">
        <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" border="0"></a>
        <h3 class="reg-preson-name"><?php echo $person['surname']; ?> <small><?php echo $person['name']; ?></small></h3>
            <?php echo $person['pos']; ?>
        <div class="clear"></div>
    </div>
<?php endforeach;?>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/saricheva.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/saricheva.jpg" border="0"></a>
    <h3 class="reg-preson-name">Сарычева <small>Елена Николаевна </small></h3>
    заместитель заведующего отделом организационной и кадровой работы
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/bogdanov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/bogdanov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Богданов <small>Иван Иванович</small></h3> 
    правовой инспектор труда Профсоюза
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/efimov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/efimov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Ефимов <small>Александр Михайлович</small></h3> 
    технический инспектор труда Профсоюза
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/morozova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/morozova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Морозова <small>Валентина Яковлевна</small></h3> 
    заместитель главного бухгалтера ДОРПРОФЖЕЛ по Брянскому региону
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/shugaeva.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/shugaeva.jpg" border="0"></a>
    <h3 class="reg-preson-name">Шугаева <small>Галина Петровна</small></h3> 
    бухгалтер
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/kazekina.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/kazekina.jpg" border="0"></a>
    <h3 class="reg-preson-name">Казекина <small>Оксана Викторовна</small></h3> 
    специалист
    <div class="clear"></div>
</div>



<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
