<?php 
$postId = 1139;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Орловско-Курское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    302006, г. Орел, Привокзальная площадь 1<br>
    Тел.: +8 (4862) 46-73-14<br>
    Факс: +8 (4862) 46-73-14<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/borodin.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/borodin.jpg" border="0"></a>
    <h3 class="reg-preson-name">Бородин <small>Василий Михайлович</small></h3>
    заместитель председателя Дорпрофжел – руководитель Орловско-Курского регионального обособленного подразделения Дорпрофжел на Московской ж.д.
    <div class="clear"></div>
</div>

<!-- <div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/bespalov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/bespalov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Беспалов <small>Александр Николаевич</small></h3>
    заместитель председателя Дорпрофжел по Курскому региону
    <div class="clear"></div>
</div> -->

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/chelishov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/chelishov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Челышов <small>Юрий Федорович</small></h3>
    заместитель руководителя Орловско-Курского регионального обособленного подразделения – председатель ППО локомотивного депо Курск
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/lazareva-s-u.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/lazareva-s-u.jpg" border="0"></a>
    <h3 class="reg-preson-name">Лазарева <small>Светлана Юрьевна</small></h3>
    правовой инспектор
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/uspenskaya.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/uspenskaya.jpg" border="0"></a>
    <h3 class="reg-preson-name">Успенская <small>Татьяна Владимировна</small></h3> 
    заместитель заведующего финансовым отделом Дорпрофжел на Орловско-Курском регионе
    <div class="clear"></div>
</div>


<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/kremsa.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/kremsa.jpg" border="0"></a>
    <h3 class="reg-preson-name">Кремса <small>Екатерина Юрьевна</small></h3>
    специалист
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/kotova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/kotova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Котова <small>Нина Васильевна</small></h3> 
    ведущий специалист
    <div class="clear"></div>
</div>


<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/malova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/malova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Малова <small>Любовь Александровна</small></h3>
    специалист
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/o-k/pankratov-o-v.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/o-k/pankratov-o-v.jpg" border="0"></a>
    <h3 class="reg-preson-name">Панкратов <small>Антон Владимирович</small></h3>
    специалист
    <div class="clear"></div>
</div>


<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
