<?php 
$tagId = 158;
$postId = 1136;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Рязанское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    390013 г.Рязань, ул. Вокзальная. д.28<br>
    rop-62@yandex.ru<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/riabov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/riabov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Рябов <small>Константин Николаевич</small></h3> 
    заместитель председателя Дорпрофжел – руководитель Рязанского регионального обособленного подразделения Дорпрофжел на Московской ж.д.
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/alehin.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/alehin.jpg" border="0"></a>
    <h3 class="reg-preson-name">Алехин <small>Вадим Олегович</small></h3> 
    правовой инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/nasonov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/nasonov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Насонов <small>Виктор Викторович</small></h3> 
    технический инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/volkova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/volkova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Волкова <small>Анна Игоревна</small></h3> 
    главный специалист
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/kopylova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/kopylova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Копылова<small> Елена Ивановна</small></h3> 
    заместитель заведующего финансовым отделом Дорпрофжел на Рязанском регионе
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/jukova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/jukova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Жукова <small>Анна Петровна</small></h3>
    специалист
    <div class="clear"></div>
</div>


<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
