<?php 
$postId = 1135;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Московско-Курское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    107140, Москва, ул.Верхняя Красносельская  д. 10 к.3<br>
    e-mail: trknod1@mail.ru<br>
    факс: 8-499-266-62-90<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/zuikov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/zuikov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Зуйков <small>Валерий Александрович</small></h3> 
    Заместитель председателя Дорпрофжел — руководитель Московско-Курского регионального обособленного подразделения Дорпрофжел на Московской ж.д.
    <div class="clear"></div>
</div>


<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/t.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/t.jpg" border="0"></a>
    <h3 class="reg-preson-name">Ткаченко <small>Вячеслав Владимирович</small></h3> 
    Заместитель руководителя Московско-Курского  регионального обособленного подразделения
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/k.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/k.jpg" border="0"></a>
    <h3 class="reg-preson-name">Квариани <small>Наталья Игоревна</small></h3> 
    Заместитель заведующего отделом социальной сферы
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/grishina.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/grishina.jpg" border="0"></a>
    <h3 class="reg-preson-name">Гришина <small>Елена Игоревна</small></h3> 
    Заместитель заведующего отделом организационной и кадровой работы
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/clinosova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/clinosova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Клиносова <small>Ирина Владимировна</small></h3> 
    Главный специалист
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/t/kuznetcova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/t/kuznetcova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Кузнецова<small> Екатерина Вячеславовна </small></h3>
    заместитель главного бухгалтера Дорпрофжел по Московско-Курскому региону
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/alexanderov.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/alexanderov.jpg" border="0"></a>
    <h3 class="reg-preson-name">Александров <small>Сергей Анатольевич</small></h3>
    Технический инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/m-k/g.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/m-k/g.jpg" border="0"></a>
    <h3 class="reg-preson-name">Гусева <small>Маргарита Владимировна</small></h3>
    Правовой инспектор труда Профсоюза.
    <div class="clear"></div>
</div>

<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
