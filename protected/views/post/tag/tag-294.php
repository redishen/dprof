<?php 
$postId = 1141;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Обособленное подразделение  <br><small>Московской Дирекции по ремонту пути и Дирекции по эксплуатации и ремонту путевых машин</small></h1>

<p class="reg-contacts">
    111398, г. Москва, Кусковский тупик, д.1<br>
    Телефон: 8-499-266-80-60, факс: 8-499-266-80-72<br>
    Е-mail: oppomdrpdpm@mail.ru<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/drp-dpm/emelianova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/drp-dpm/emelianova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Емельянова <small>Зинаида Ивановна</small></h3> 
    председатель 
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/drp-dpm/sova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/drp-dpm/sova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Сова <small>Елена Витальевна </small></h3> 
    главный специалист-бухгалтер
    <div class="clear"></div>
</div>
<div class="reg-person">
    <h3 class="reg-preson-name">Бузило <small>Светлана Дмитриевна</small></h3> 
    специалист-управделами 
    <div class="clear"></div>
</div>



<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
