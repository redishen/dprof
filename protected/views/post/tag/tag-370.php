<?php
$postId = 1362;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Первичная профсоюзная организация <br>
    <small>Московского филиала АО «Федеральная пассажирская компания»</small>
</h1>

<p class="reg-contacts">
    129272, Москва, ул. Верземнека, д. 4<br>
    e-mail: 4992668548@mail.ru<br>
    факс: 8-499-266-84-71<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/mfpk/1.jpg" rel="lightbox[_default]" title=""><img
                src="/files/site/reg/mfpk/1.jpg" border="0"></a>
    <h3 class="reg-preson-name">Дворецкий
        <small>Александр Викторович</small>
    </h3>
    председатель
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/mfpk/2.jpg" rel="lightbox[_default]" title=""><img
                src="/files/site/reg/mfpk/2.jpg" border="0"></a>
    <h3 class="reg-preson-name">Изотова
        <small>Оксана Магомедовна</small>
    </h3>
    заместитель председателя
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/mfpk/3.jpg" rel="lightbox[_default]" title=""><img
                src="/files/site/reg/mfpk/3.jpg" border="0"></a>
    <h3 class="reg-preson-name">Картавенко
        <small>Надежда Валентиновна</small>
    </h3>
    главный бухгалтер
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/mfpk/4.jpg" rel="lightbox[_default]" title=""><img
                src="/files/site/reg/mfpk/4.jpg" border="0"></a>
    <h3 class="reg-preson-name">Папсуева
        <small>Татьяна Сергеевна</small>
    </h3>
    заместитель главного бухгалтера
    <div class="clear"></div>
</div>


<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>