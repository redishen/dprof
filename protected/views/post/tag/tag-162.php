<?php
$postId = 1137;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Тульское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    300041 г. Тула, ул. Путейская, д. 12<br>
    Ter2101@yandex.ru<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/t/s.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/t/s.jpg" border="0"></a>
    <h3 class="reg-preson-name">Соколов <small>Алексей Алексеевич</small></h3> 
    руководитель Тульского регионального обособленного подразделения Дорпрофжел на Московской ж.д.
    <div class="clear"></div>
</div>

<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/t/elsova.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/t/elsova.jpg" border="0"></a>
    <h3 class="reg-preson-name">Ельсова <small>Анастасия Александровна</small></h3> 
    заместитель заведующего отдела организационной и кадровой работы Дорпрофжел по Тульскому региону
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/t/zavatin.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/t/zavatin.jpg" border="0"></a>
    <h3 class="reg-preson-name">Заватин <small>Алексей Валерьевич</small></h3> 
    правовой инспектор труда Профсоюза
    <div class="clear"></div>
</div>
<div class="reg-person">
    <a class="reg-preson-pic" href="/files/site/reg/t/emetc.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/t/emetc.jpg" border="0"></a>
    <h3 class="reg-preson-name">Емец <small>Валентина Ивановна</small></h3>
    заместитель главного бухгалтера Дорпрофжел по Тульскому региону
    <div class="clear"></div>
</div>



<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
