<?php 

$tagId = 173;
$postId = 1138;
$post = Post::model()->findByPk($postId, 'status=' . Post::STATUS_PUBLISHED . ' AND update_time < ' . time());

$this->pageTitle = $tagModel->name . ' - ' . Yii::app()->name;
?>
<h1 class="page-title">Смоленское <br><small>региональное обособленное подразделение Дорпрофжел</small></h1>

<p class="reg-contacts">
    214000, Смоленск, ул. М.Жукова, 16<br>
    e-mail: terkomsm@yandex.ru<br>
    факс: 8-481-239-47-58<br>
    <a href="<?php echo $post->url;?>#files">все контакты</a>
</p>

<h2 class="reg-subtitle">О подразделении</h2>
<p>
    <?php echo $post->preview_text;?>
    <a href="<?php echo $post->url;?>">Подробнее...</a>
</p>

<h2 class="reg-subtitle">Сотрудники</h2> 

<?php
$persons = array(
    array(
        'surname' => 'Волосенков',
        'name' => 'Виктор Федорович',
        'pos' => 'заместитель председателя Дорпрофжел - руководитель  Смоленского регионального обособленного подразделения Дорпрофжел на Московской ж.д.',
        'pic' => 'v',
    ),
    array(
        'surname' => 'Сорокин',
        'name' => 'Александр Алексеевич',
        'pos' => 'Технический инспектор',
        'pic' => 's',
    ),
    array(
        'surname' => 'Шабанова',
        'name' => 'Инна Иосифовна',
        'pos' => 'Заместитель главного бухгалтера ДОРПРОФЖЕЛ по Смоленскому региону',
        'pic' => 'sh',
    ),
    array(
        'surname' => 'Барченкова',
        'name' => 'Елена Петровна',
        'pos' => 'заместитель заведующего организационным отделом ДОРПРОФЖЕЛ по Смоленскому региону',
        'pic' => 'b',
    ),
    array(
        'surname' => 'Кирпиченкова',
        'name' => 'Елена Анатольевна',
        'pos' => 'специалист',
        'pic' => 'k',
    ),
    // array(
    //     'surname' => 'Барченкова',
    //     'name' => 'Елена',
    //     'pos' => '',
    //     'pic' => '',
    // ),
);

?>

<?php foreach($persons as $person):?>
    <div class="reg-person">
        <a class="reg-preson-pic" href="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" rel="lightbox[_default]" title=""><img src="/files/site/reg/<?php echo $tagId; ?>/<?php echo $person['pic']; ?>.jpg" border="0"></a>
        <h3 class="reg-preson-name"><?php echo $person['surname']; ?> <small><?php echo $person['name']; ?></small></h3>
            <?php echo $person['pos']; ?>
        <div class="clear"></div>
    </div>
<?php endforeach;?>

<h2 class="reg-subtitle">Новости региона</h2>

<hr class="space"/>

<div class="reg-posts">

    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '//post/_preview_minimal',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории'
    ));
    ?>

</div>
