<h1 style="text-align: center"><?php echo CHtml::encode($catName); ?></h1>
<?php $this->pageTitle = $catName . ' - ' . Yii::app()->name; ?>


<p>
    08 октября 2018 года  общее собрания членов КПК "ЦЕНТР" принято решение о ликвидации кредитного потребительского кооператива "ЦЕНТР".
</p>
<p>
    Руководителем ликвидационной комиссии назначен П.Б. Гинзбург
</p>
<p>
    Телефон для справок: <strong>+7(499)266-13-07</strong>
</p>

    
<div class="clear"></div>

<hr class="space"/>

<?php
$this->widget('ext.widgets.DListView.DListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_preview',
    'template' => "{items}\n{pager}",
    'emptyText' => 'Нет статей по этой категории'
));
?>
<br>
<br>
