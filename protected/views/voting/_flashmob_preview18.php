

<div id="<?= $id ?>" class="section">
    <h2>Видео-работа <?= $name ?></h2>
    <div class="vid">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $tube ?>" frameborder="0"
                allowfullscreen></iframe>
    </div>
    <div class="vote">
        <p>
            <span>Чтобы проголосовать, нажмите:</span>
        </p>
        <a class="vote-btn disabled" style="" href="<?= $this->createUrl('vote', array('vote' => $id)) ?>">Я голосую за эту работу!</a>

<!--        <p>-->
<!--            <span style="font-weight: bold;">Голосование окончено</span>-->
<!--        </p>-->
        <div class="stat">
            Проголосовало:
            <?=
            Voting::model()->count('voting_id=:voting_id and for=:for', array(
                ':voting_id' => $this->vid,
                ':for' => $id,
            ))
            ?>
        </div>
    </div>
    <div class="clear"></div>
</div>
