<?
$this->pageTitle = "Я проголосовал(а) за видео-работу $name на сайте Дорпрофжел на МЖД";

Yii::app()->clientScript->registerMetaTag("Я проголосовал(а) за видео-работу $name на сайте Дорпрофжел на МЖД", 'og:title');
Yii::app()->clientScript->registerMetaTag('Конкурсный флешмоб, посвященный 180-летию железным дорогам России', 'og:description');
Yii::app()->clientScript->registerMetaTag('http://dprof.ru/files/site/carousel/main-pic-1.jpg', 'og:image');

?>


<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>

<style>

    div.wrapper {
        margin: 60px 0;
        background-color: #F5F5F5;
        padding: 35px 40px;
        border-radius: 10px;
    }

    div.wrapper h2 {
        font-size: 25px;
        margin-bottom: 28px;
        margin-top: 8px;
    }

    /*.vote {*/
    /*float: right;*/
    /*width: 30%;*/
    /*}*/

    div.wrapper .ya-share2__container_size_m .ya-share2__icon {
        height: 50px;
        width: 50px;
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 50px 50px;
    }

    div.wrapper p {
        font-size: 20px;
    }

    div.wrapper .votes {
        margin-bottom: 40px;
    }

</style>

<div id="<?= $id ?>" class="wrapper">
            <p>
                <span style="color: red">Голосоваие закончилось, идет подсчет голосов.</span>
            </p>
    <? if ($status == 'success'): ?>
        <h2 style="margin-top: 50px;
    font-size: 40px;">
            Вы <b style="color: red">успешно проголосовали</b> за видео-работу <?= $name ?></h2>
    <? elseif ($status == 'voted'): ?>
        <h2 style="margin-top: 50px;
    font-size: 40px;">
            Вы <b style="color: red">уже проголосовали</b> за видео-работу <?= $name ?>. <small>Повторное голосование не учитывается</small></h2>
    <? elseif ($status == 'error'): ?>
        <h2 style="margin-top: 50px;
    font-size: 40px;">
            <b style="color: red">Ошибка при голосовании</b>, пожалуйста, сообщите администратору</h2>
   <? elseif ($status == 'closed'): ?>
        <h2 style="margin-top: 50px;
    font-size: 40px;">
            <b style="color: red">Голосование закрыто</b>, идет подсчет голосов.</h2>
    <? endif; ?>


    <div class="vote" style="margin-top: 30px">
        <p style="margin-bottom: 10px;">
            Поделиться в социальной сети:
        </p>
        <div class="votes">
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki"
                 data-url="http://dprof.ru/voting/flashmobDay17"
                 data-title="Я проголосовал(а) за видео-работу <?= $name ?> на сайте Дорпрофжел на МЖД"
                 data-image="http://dprof.ru/files/site/carousel/main-pic-1.jpg"
                 data-description="Конкурсный флешмоб, посвященный 180-летию железным дорогам России.">
            </div>
        </div>
    </div>
    <div class="clear"></div>

    <a href="#" style="font-size: 20px; text-align: center; display: block;"
            onclick="window.open('', '_self', ''); window.close();">[ ЗАКРЫТЬ ОКНО ]</a>
</div>
