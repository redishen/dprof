<?php

/**
 * @var ResoForm $model
 * @var boolean $success
 *
 */

header("Access-Control-Allow-Origin: *");
$this->breadcrumbs = false;

$heading = 'Льготное страхование для членов РОСПРОФЖЕЛ';
$this->pageTitle = "$heading - " . Yii::app()->name;

?>


<div class="bs">
    <div class="reso i-bem" data-bem='{"reso":{}}'>
        <div class="container-fluid">
            <div class="reso__cover">
                <div class="reso__cover-inner">
                    <h1 class="reso__cover-text">Льготное страхование<br> для членов РОСПРОФЖЕЛ</h1><a class="dk-button reso__btn" href="#form">Оставить заявку</a>
                </div>
            </div>
            <div class="reso__block">
                <div class="reso__block-header">РЕСО-Гарантия - официальный партнер РОСПРОФЖЕЛ</div>
                <div class="reso__block-text">Члены РОСПРОФЖЕЛ и их близкие могут рассчитывать на полноценную страховую защиту РЕСО-Гарантия на оптимальных условиях.</div>
            </div>
            <div class="reso__highlight reso__highlight_color_yellow">
                <div class="reso__list-heading">Скидки распространяются на:</div>
                <ul class="reso__list">
                    <li>Автострахование (КАСКО, ОСАГО, «Доктор РЕСО в ДТП», «Зелёная карта»)</li>
                    <li>Страхование квартир и домов</li>
                    <li>Страхование от критических заболеваний (с лечением за рубежом)</li>
                    <li>Страхование от несчастных случаев</li>
                    <li>Инвестиционное страхование жизни</li>
                </ul>
            </div>
            <div class="reso__block">
                <div class="reso__block-header">Специально для членов РОСПРОФЖЕЛ действуют сокращенные сроки урегулирования страховых случаев.</div>
                <div class="reso__block-text">Перед покупкой полиса предъявите электронный профсоюзный билет.</div>
            </div>
            <div class="reso__highlight reso__highlight_color_grey">
                <div class="reso__list-heading">РЕСО-Гарантия – один из крупнейших в России универсальных страховщиков. Компания оказывает клиентам более 100 видов страховых услуг.</div>
                <ul class="reso__list">
                    <li>950 офисов продаж</li>
                    <li>10 млн клиентов по всей стране</li>
                    <li>45 млрд 144 млн рублей – выплаты по страховым случаям за 2018 год</li>
                    <li>Работа в режиме онлайн, круглосуточно</li>
                    <li>Приложение РЕСО Мобайл для удаленного обслуживания и оплаты полисов</li>
                    <li>«Народная марка/Марка №1 в России» в номинации «Страховая компания»</li>
                </ul>
            </div>
            <form class="reso__form col-md-8 col-md-offset-2" method="post" action="#form" id="form">
                <div class="reso__form-header">Оставьте заявку и персональный менеджер свяжется с вами</div>

                <? if($model->hasErrors()): ?>
                    <div class="reso__form-errors alert alert-danger" role="alert">
                        <?= CHtml::errorSummary($model) ?>
                    </div>
                <? endif; ?>

                <? if($success): ?>
                    <div class="reso__form-errors alert alert-success" role="alert">Ваша заявка успешно отправлена</div>
                <? else: ?>

                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Ваша фамилия</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[surname]" type="text" value="<?= $model->surname ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Имя</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[name]" type="text" value="<?= $model->name ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Отчество</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[second_name]" type="text" value="<?= $model->second_name ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Контактный телефон</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[phone]" type="text" value="<?= $model->phone ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Адрес email</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[email]" type="text" value="<?= $model->email ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Номер профсоюзного билета</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[card_number]" type="text" value="<?= $model->card_number ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Выберите продукт</label>
                    </div>
                    <div class="col col-md-6">
                        <select class="reso__select form-control" name="ResoForm[product]" value="<?= $model->product ?>">
                            <option>Автострахование</option>
                            <option>Страхование квартир и домов</option>
                            <option>Страхование дач и домов</option>
                            <option>Страхование критических заболеваний</option>
                            <option>Страхование от несчастных случаев</option>
                            <option>"Инвестиционное страхование жизни</option>
                        </select>
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="reso__label">Ваш город</label>
                    </div>
                    <div class="col col-md-6">
                        <input class="reso__input form-control" name="ResoForm[city]" type="text" value="<?= $model->city ?>">
                    </div>
                </div>
                <div class="row form-group">
                    <div class="col col-md-4"></div>
                    <div class="col col-md-6">
                        <div class="dk-recaptcha">
                            <div class="g-recaptcha" data-sitekey="6LdCDsAUAAAAAF378Xb2F38wp59s-4EyyEULoxJK"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col col-md-12 text-center">
                        <div class="reso__form-confirm">Нажимая кнопку «Отправить» я даю <a target="_blank" href="http://www.reso.ru/regulations/personal-agreement-2.html">согласие</a> на обработку своих персональных данных.
                            <br>С <a target="_blank" href="http://www.reso.ru/regulations/safety-of-personal.html">политикой обеспечения безопасности персональных данных </a> СПАО "РЕСО-Гарантия" ознакомлен.</div>
                        <button class="reso__btn">Отправить</button>
                        <div class="reso__note text-muted">Узнать подробности программы страхования и приобрести страховой полис можно в Первичной профсоюзной организации или в ближайшем филиале РЕСО-Гарантия. Полный список офисов продаж <a target="_blank" href="http://www.reso.ru/About/Contacts/">на сайте СПАО "РЕСО-Гарантия"</a>
                        </div>
                    </div>
                </div>
                <? endif; ?>
            </form>
        </div>
    </div>
</div>
