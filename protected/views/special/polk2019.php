<?php
//$this->breadcrumbs = array(
//    $model->title,
//);
/**
 * @var Polk2019Post[] $posts
 */


header("Access-Control-Allow-Origin: *");
$this->breadcrumbs = false;

$heading = 'БЕССМЕРТНЫЙ ПОЛК. НАША ПАМЯТЬ — ИСТОРИЯ СТРАНЫ';
$this->pageTitle = "$heading - " . Yii::app()->name;

?>


<div class="bs">
    <div class="container-fluid">
        <div class="polk i-bem" data-bem='{"polk":{}}'>
            <div class="post">
                <div class="post__title">
                    <?php echo CHtml::encode($heading); ?>
                </div>

                <div class="content">
                    <strong>
                        <p>В память о железнодорожниках и членах их семей, кто отдал свои жизни ради мира на земле, кто
                            ковал победу в тылу, кто восстанавливал страну из руин, кто пережил голод и истязания врага.<br>
                            Вечная Вам память и низкий поклон!</p>
                    </strong>
                </div>

            </div>

            <?php if (!empty($images)): ?>
                <div class="dk-slider">
                    <?php
                    foreach ($images as $image) :
                        ?>
                        <div class="dk-slider__slide">
                            <img src="<? echo $image ?>">
                        </div>
                    <?
                    endforeach;
                    ?>
                </div>
            <?php endif; ?>

            <div class="polk__list">
                <div class="polk__list-title">
                    По алфавиту
                </div>
                <div class="polk__list-items">
                    <?
                    foreach ($posts as $post): ?>
                        <div class="polk__item">
                            <a class="polk__item-title" href="<?= Yii::app()->createUrl('/special/polk2019View', [
                                'id' => $post->id,
                                'title' => $post->title,
                            ]) ?>"><?= CHtml::encode($post->title) ?>
                            </a>
                        </div>
                    <? endforeach; ?>

                </div>

            </div>


        </div>
    </div>
</div>
