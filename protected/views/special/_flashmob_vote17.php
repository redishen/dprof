<?
$this->pageTitle = "Я проголосовал(а) за видео-работу $name на сайте Дорпрофжел на МЖД";

Yii::app()->clientScript->registerMetaTag("Я проголосовал(а) за видео-работу $name на сайте Дорпрофжел на МЖД", 'og:title');
Yii::app()->clientScript->registerMetaTag('Конкурсная флешмоб – эстафета «СПАСИБО ДЕДУ ЗА ПОБЕДУ 2017!», приуроченная к празднованию Дня Победы.', 'og:description');
Yii::app()->clientScript->registerMetaTag('http://dprof.ru/files/site/carousel/main-pic-1.jpg', 'og:image');

?>


<script type="text/javascript" src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js" charset="utf-8"></script>
<script type="text/javascript" src="//yastatic.net/share2/share.js" charset="utf-8"></script>

<style>

    div.wrapper {
        margin: 60px 0;
        background-color: #F5F5F5;
        padding: 35px 40px;
        border-radius: 10px;
    }

    div.wrapper h2 {
        font-size: 25px;
        margin-bottom: 28px;
        margin-top: 8px;
    }

    /*.vote {*/
        /*float: right;*/
        /*width: 30%;*/
    /*}*/

    div.wrapper .ya-share2__container_size_m .ya-share2__icon {
        height: 80px;
        width: 80px;
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 80px 80px;
    }

    div.wrapper p {
        font-size: 20px;
    }

    div.wrapper .votes {
        margin-bottom: 40px;
    }

</style>

<div id="<?= $id ?>" class="wrapper">
    <h2>Я голосую за видео-работу <?= $name ?></h2>
    <div class="vote">
        <p>
            <span>Чтобы проголосовать:</span>
        </p>
        <p>
            1. Поделитесь этой записью в вашей социальной сети:
        </p>
        <p>
            <span style="color: red">Голосоваие закончилось, идет подсчет голосов.</span>
        </p>
<!--        <div class="votes">-->
<!--            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki"-->
<!--                 data-url="http://dprof.ru/special/flashmob9may17/vote/--><?//= $id ?><!--"-->
<!--                 data-title="Я проголосовал(а) за видео-работу --><?//= $name ?><!-- на сайте Дорпрофжел на МЖД"-->
<!--                 data-image="http://dprof.ru/files/site/carousel/main-pic-1.jpg"-->
<!--                 data-description="Конкурсная флешмоб – эстафета «СПАСИБО ДЕДУ ЗА ПОБЕДУ 2017!», приуроченная к празднованию Дня Победы.">-->
<!--            </div>-->
<!--        </div>-->
        <p>
            2. Добавьте теги <strong>#дорпрофжел, #мжд, #флешмоб17</strong> к вашей записи.
        </p>
        <p>
            Внимание! Записи без тегов <strong>НЕ БУДУТ</strong> учитываться при подсчете голосов.
        </p>
        <p style="">
            3. <a href="#" onclick="window.close()">Закройте это окно.</a>
        </p>
    </div>
    <div class="clear"></div>
</div>
