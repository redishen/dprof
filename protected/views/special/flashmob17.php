<?php
$this->pageTitle = 'Голосование за лучший ролик флешмоб – эстафеты на сайте Дорпрофжел на МЖД';

Yii::app()->clientScript->registerMetaTag('Голосование за лучший ролик флешмоб – эстафеты на сайте Дорпрофжел на МЖД', 'og:title');
Yii::app()->clientScript->registerMetaTag('Конкурсная флешмоб – эстафета «СПАСИБО ДЕДУ ЗА ПОБЕДУ!», приуроченная к празднованию Дня Победы.', 'og:description');
Yii::app()->clientScript->registerMetaTag('http://dprof.ru/files/site/carousel/main-pic-1.jpg', 'og:image');

$this->breadcrumbs = array(
    'Голосование за лучший ролик флешмоб – эстафеты',
);

?>

<script src="https://yastatic.net/share2/share.js" async="async" charset="utf-8"></script>
<script src="https://yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
<script src="https://yastatic.net/share2/share.js" async="async"></script>

<style>
    .vote-btn {
        display: inline-block;
        white-space: nowrap;
        vertical-align: top;
        color: #fff;
        overflow: hidden;
        position: relative;
        background-color: #D32F2F;
        border-radius: 2px;
        padding: 9px 18px;
        font-size: 20px;
        text-decoration: none;
        cursor: pointer;
    }

    .vote-btn:hover {
        color: #fff;
        background-color: #E53935;
    }

    div.section {
        margin-bottom: 45px;
        background-color: #F5F5F5;
        padding: 25px 30px;
        border-radius: 2px;
    }

    div.section h2 {
        font-size: 25px;
        margin-bottom: 28px;
        margin-top: 8px;
    }

    .vote {
        float: right;
        width: 30%;

    }

    .vid {
        float: left;
        width: 70%;

    }

    .stat {
        margin-top: 20px;
        float: left;
        position: relative;
    }

    .stat .ya-share2__container_size_m .ya-share2__icon {
        height: 32px;
        width: 32px;
        background-repeat: no-repeat;
        background-position: center center;
        background-size: 32px 32px;
    }

    .stat .ya-share2__container_size_m .ya-share2__counter {
        font-size: 16px;
        line-height: 32px;
        padding: 0 10px;
    }

    .stat .ya-share_over {
        height: 40px;
        width: 100%;
        margin-top: -35px;
        position: absolute;
    }


</style>

<script>
    $(document).ready(function () {
        $('.vote-btn').click(function(e) {
            e.preventDefault();
            var left  = ($(window).width()/2)-(900/2),
                top   = ($(window).height()/2)-(600/2),
                popup = window.open ($(this).prop('href'), "Голосовать", "width=800, height=600, top="+top+", left="+left);
            return false;
        })

        $('body').on('click', '.ya-share2__link', function (e) {
            $(this).unbind('click');
            $(this).prop('href', '#');
            e.preventDefault();
            return false;
        })

    });
</script>


<h1 style="margin-bottom: 40px;">Голосование за лучший ролик флешмоб – эстафеты <br>«СПАСИБО ДЕДУ ЗА ПОБЕДУ 2017!»</h1>

<? foreach ($data as $key => $item) {
    $this->renderPartial('_flashmob_preview17', array(
        'id' => $key,
        'tube' => $item['tube'],
        'name' => $item['name'],
    ));
} ?>

