<?php
//$this->breadcrumbs = array(
//    $model->title,
//);
/**
 * @var CActiveDataProvider $dataProvider
 */


$this->breadcrumbs = [
        CHtml::link('Бессмертный полк', ['special/polk2019'])
];

$this->pageTitle = $model->title . ', Бессмертный полк —' . Yii::app()->name;

//$dataProvider->get
?>


<div class="post">
    <div class="title" style="margin-bottom: 32px">
        <?= CHtml::encode($model->title); ?>
    </div>
    <div class="cover" style="text-align: center">
        <img style="max-width: 100%" src="<?= $model->postImages[0]->getUrl() ?>"/>
    </div>
</div>
