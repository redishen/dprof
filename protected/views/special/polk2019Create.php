<?php
$this->pageTitle = 'Добавить записи в Бессмертный Полк 🏳️';
$this->breadcrumbs = array(
    'Новые записи',
);

$freqTags = Tag::model()->findAll(array(
    'order' => 'frequency DESC',
    'limit' => 32

));
?>

<div class="form col-xs-10 col-xs-offset-1">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <fieldset>
        <div class="form-group">
            <?php echo $form->label($model, 'tags'); ?>
            <?php
            $this->widget('CAutoComplete', array(
                'model' => $model,
                'attribute' => 'tags',
                'url' => array('post/suggestTags'),
                'multiple' => true,
                'htmlOptions' => array('size' => 50, 'class' => 'form-control form__autocomplete'),
            ));
            ?>
            <p class="hint">
                <? foreach ($freqTags as $tag): ?>
                    <a href="#" class="form__autocomplete-tag"><?= $tag->name ?></a>
                <? endforeach; ?>

            </p>
            <p class="hint">Введите, разделяя запятыми.</p>
            <?php echo $form->error($model, 'tags'); ?>
        </div>

    </fieldset>

    <fieldset>
        <legend>Карточки</legend>
        <?php if (!empty($model->postImages)): ?>
            <div class="form-group">
                <?php foreach ($model->postImages as $image): ?>
                    <div class="col-xs-4 thumbnail">
                        <?= CHtml::link('/files/images/' . $image->file_name, Yii::app()->request->baseUrl . '/files/images/' . $image->file_name) ?>
                        <br>
                        <?=
                        BsHtml::image('/files/images/thumbs/150x150/' . $image->file_name, '', array(
                            'type' => 'responsible'
                        ));
                        ?>
                        <!--<br>-->
                        <?=
                        BsHtml::ajaxButton('Удалить', array(
                            'ajaxDeleteImg',
                            'id' => $image->file_name), array(
                            'success' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("Удалено")}, this)'),
                            'error' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("ОШИБКА УДАЛЕНИЯ")}, this)')), array(
                                'confirm' => 'Вы действительно хотите удалить элемент?',
                                'size' => 'xs',
                                'class' => 'pull-right',
                                'icon' => BsHtml::GLYPHICON_TRASH,
                                'style' => 'margin-top: 5px',
                            )
                        );
                        ?>
                    </div>
                <?php endforeach; ?>
                <div class="clearfix"></div>
            </div>
        <?php endif; ?>


        <div class="form-group">
            <?php echo $form->label($model, 'images'); ?>
            <?php
            $this->widget('xupload.FileUpload', array(
                    'url' => $this->createUrl('upload'),
                    'showForm' => false,
                    'model' => $images,
                    'attribute' => 'file',
                    'multiple' => true,
                    'autoUpload' => true,
                    'formView' => 'application.views.post._uploadform',
                    'htmlOptions' => array('id' => 'post-form'),
                    'options' => array(
                        'maxFileSize' => 20000000,
                        'acceptFileTypes' => 'js:/(\.|\/)(gif|jpe?g|png)$/i',
                        'disableImageResize' => 'js:/Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)',
                        'previewMaxWidth' => 150,
                        'previewMaxHeight' => 150,
                        'previewCrop' => true
                    )
                )
            );
            ?>
        </div>
    </fieldset>

    <fieldset>
        <legend>Связи</legend>
        <?php echo $form->textFieldControlGroup($model, 'related_posts', array('readonly' => true, 'maxlength' => 128, 'help' => 'ID постов через запятую без пробелов')); ?>
    </fieldset>

    <?php echo BsHtml::inputSubmitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('id' => 'submit')); ?>

    <?php $this->endWidget(); ?>

</div><!-- form -->

<script>
    $(function () {
        $(document).on('click', '.form__autocomplete-tag', function () {
            var $that  = $(this),
                $input = $('.form__autocomplete');

            if($input.val().length) {
                $input.val($input.val() + ', ' + $that.text());
            } else {
                $input.val($that.text());
            }

        })
    });
</script>

