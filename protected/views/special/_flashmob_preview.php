

<div id="<?= $id ?>" class="section">
    <h2>Видео-работа <?= $name ?></h2>
    <div class="vid">
        <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $tube ?>" frameborder="0"
                allowfullscreen></iframe>
    </div>
    <div class="vote">
        <p>
            <span>Чтобы проголосовать, нажмите:</span>
        </p>
        <a class="vote-btn" href="/special/flashmob9may/vote/<?= $id ?>">Я голосую за эту работу!</a>

        <div class="stat">
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,moimir" data-counter=""
                 data-url="http://dprof.ru/special/flashmob9may/vote/<?= $id ?>"
                 data-title="Я проголосовал(а) за видео-работу <?= $name ?> на сайте Дорпрофжел на МЖД"
                 data-image="http://dprof.ru/files/site/carousel/main-pic-1.jpg"
                 data-description="Конкурсная флешмоб – эстафета «СПАСИБО ДЕДУ ЗА ПОБЕДУ!», приуроченная к празднованию Дня Победы.">
            </div>
            <div class="ya-share_over"></div>
        </div>
    </div>
    <div class="clear"></div>
</div>
