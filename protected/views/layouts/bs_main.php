<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="ru" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?php
        /* JavaScripts */
        Yii::app()->clientScript
                ->registerCoreScript('jquery', CClientScript::POS_END)
                ->registerCoreScript('jquery.ui', CClientScript::POS_END)
                ->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js', CClientScript::POS_BEGIN)
                ->registerScriptFile(Yii::app()->baseUrl . '/js/metisMenu.min.js', CClientScript::POS_END)
                ->registerScriptFile(Yii::app()->baseUrl . '/js/sb-admin-2.js', CClientScript::POS_END)
//                ->registerScript('tooltip', "$('[data-toggle=\"tooltip\"]').tooltip();
//        $('[data-toggle=\"popover\"]').popover()"
//                        , CClientScript::POS_READY)
                ->registerScript('popover', "
                    jQuery('body').popover({'selector':'a[rel=popover]'});
                    jQuery('body').tooltip({'selector':'a[rel=tooltip]'});"
                        , CClientScript::POS_END);
        ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ?>/css/sb-admin-2.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->baseUrl ?>/css/bootstrap-style.min.css">
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
            <script src="<?php echo Yii::app()->baseUrl ?>/js/html5shiv.min.js"></script>
            <script src="<?php echo Yii::app()->baseUrl ?>/js/respond.min.js"></script>
        <![endif]-->

        <?php if (is_array($this->pageTitle)): ?>
            <title><?= $this->pageTitle[1] . ' | ' . $this->pageTitle[0]; ?></title>
        <?php else: ?>
            <title><?= Yii::app()->name . ' | ' . CHtml::encode($this->pageTitle); ?></title>
        <?php endif; ?>
    </head>

    <body>
        <div id="wrapper">

            <?php echo $content; ?>
        </div>

        <?php
        $this->widget('ext.jgrowl.JGrowl');
        ?>
    </body>
</html>
