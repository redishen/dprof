<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/bs_main'); ?>

<!-- Navigation -->
<?php
$this->widget('bootstrap.widgets.BsNavbar', array(
    'collapse' => true,
    'brandLabel' => BsHtml::icon(BsHtml::GLYPHICON_HOME),
    'brandUrl' => Yii::app()->homeUrl,
    'position' => BsHtml::NAVBAR_POSITION_FIXED_TOP,
    'htmlOptions' => array(
        'class' => 'top-nav',
    ),
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.BsNav',
            'type' => 'navbar',
            'activateParents' => true,
            'items' => array(
                array(
                    'label' => 'Новости',
                    'url' => array(
                        '/post'
                    ),
                    'active' => Yii::app()->controller->uniqueId == 'post',
                    'items' => array(
                        array(
                            'label' => 'Создать',
                            'url' => array(
                                '/post/create'
                            ),
                        ),
                        array(
                            'label' => 'Создать записи БП',
                            'url' => array(
                                '/special/polk2019Create'
                            ),
                        ),
                        array(
                            'label' => 'Управление',
                            'url' => array(
                                '/post/admin'
                            ),
                        ),
                        array(
                            'label' => 'Просмотр',
                            'url' => array(
                                '/post/index'
                            ),
                        ),
                    ),
                ),
                array(
                    'label' => 'Документы',
                    'url' => array(
                        '/doc'
                    ),
                    'active' => Yii::app()->controller->uniqueId == 'doc',
                    'items' => array(
                        array(
                            'label' => 'Создать',
                            'url' => array(
                                '/doc/create'
                            ),
                        ),
                        array(
                            'label' => 'Управление',
                            'url' => array(
                                '/doc/admin'
                            ),
                        ),
                        array(
                            'label' => 'Просмотр',
                            'url' => array(
                                '/doc/index'
                            ),
                        ),
                    ),
                ),
                array(
                    'label' => 'Баннеры',
                    'url' => array(
                        '/banner'
                    ),
                    'active' => Yii::app()->controller->uniqueId == 'banner',
                    'items' => array(
                        array(
                            'label' => 'Создать',
                            'url' => array(
                                '/banner/create'
                            ),
                        ),
                        array(
                            'label' => 'Управление',
                            'url' => array(
                                '/banner/index'
                            ),
                        ),
                    ),
                ),
                array(
                    'label' => 'Фотоконкурс',
                    'url' => array(
                        '/voteGallery'
                    ),
                    'active' => Yii::app()->controller->uniqueId == 'voteGallery',
                    'items' => array(
                        array(
                            'label' => 'Создать',
                            'url' => array(
                                '/voteGallery/create'
                            ),
                        ),
                        array(
                            'label' => 'Управление',
                            'url' => array(
                                '/voteGallery/admin'
                            ),
                        ),
                    ),
                ),
            ),
        ),
        array(
            'class' => 'bootstrap.widgets.BsNav',
            'type' => 'navbar',
            'activateParents' => true,
            'items' => array(
                array(
                    'label' => 'Вход',
                    'url' => array(
                        '/site/login'
                    ),
                    'icon' => BsHtml::GLYPHICON_USER,
                    'visible' => Yii::app()->user->isGuest
                ),
                array(
                    'label' => ' Выход (' . (Yii::app()->user->isGuest ? '' : Yii::app()->user->name) . ')',
                    'url' => array(
                        '/site/logout'
                    ),
                    'icon' => BsHtml::GLYPHICON_USER,
                    'visible' => !Yii::app()->user->isGuest
                )
            ),
            'htmlOptions' => array(
                'pull' => BsHtml::NAVBAR_NAV_PULL_RIGHT
            )
        )
    )
));
?>

<!-- Page Content -->
<div id="page-wrapper-col1">
    <div class="container">
        <!--[if lt IE 8]>
        <div class="alert alert-danger" style="padding: 40px;">
            <p>ВНИМАНИЕ! У вас установлена старая версия Internet Explorer. Для правильной работы системы необходим более современный браузер. Работая со старой версией браузера, <strong>вы подвергаете ваш компьютер риску заражения вирусами</strong>.
            </p>
            <p>Подробную информацию о веб-браузерах и используемой вами версии можно найти на сайте <a href="http://whatbrowser.org/" target="_blank">WhatBrowser.org</a>.
            </p>
            <p>Инструкции по обновлению браузера приведены на соответствующих справочных ресурсах:</p>
            <ul>
                <li><a target="_blank" href="http://www.google.com/chrome">Google Chrome</a></li>
                <li><a target="_blank" href="http://www.mozilla.com/firefox/upgrade.html">Firefox</a></li>
                <li><a target="_blank" href="http://www.microsoft.com//windows/internet-explorer/default.aspx">Internet Explorer</a></li>
            </ul>
        </div>
        <![endif]-->
        <div class="row">
            <?php if (!empty($this->pageTitle)): ?>
                <?php if (is_array($this->pageTitle)): ?>
                    <?= BsHtml::pageHeader($this->pageTitle[0], $this->pageTitle[1]) ?>
                <?php else: ?>
                    <?= BsHtml::pageHeader($this->pageTitle) ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <div class="row">
            <?php if (isset($this->breadcrumbs)): ?>
                <?php
                $this->widget('bootstrap.widgets.BsBreadcrumb', array(
                    'links' => $this->breadcrumbs,
                    'encodeLabel' => false,
                    'homeLink' => BsHtml::openTag('li') . BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_HOME), '/') . BsHtml::closeTag('li')
                ));
                ?><!-- breadcrumbs -->
            <?php endif; ?>
        </div>
        <?php echo $content; ?>
    </div>
    <!-- /.container-fluid -->
</div>
<!-- /#page-wrapper -->
<?php if (isset($this->breadcrumbs)): ?>
    <?php
    $this->widget('bootstrap.widgets.BsBreadCrumb', array(
        'links' => $this->breadcrumbs,
        'encodeLabel' => false,
        'htmlOptions' => array('class' => 'breadcrumb breadcrumb-fixed-bottom col1'),
        'homeLink' => BsHtml::openTag('li') . BsHtml::link(BsHtml::icon(BsHtml::GLYPHICON_HOME), '/') . BsHtml::closeTag('li')
    ));
    ?><!-- breadcrumbs -->
<?php endif; ?>

<?php $this->endContent(); ?>