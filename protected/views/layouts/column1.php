<?php $this->beginContent('//layouts/main'); ?>
<div class="container">
    <div id="content">
        <?php
        $this->widget('zii.widgets.CBreadcrumbs', array(
            'links' => $this->breadcrumbs,
        ));
        ?><!-- breadcrumbs -->
        <?php echo $content; ?>
        <hr/>
        <!--<hr class="space"/>-->
        <div >
            <div  class="span-4 prepend-1">      
                <a title="Написать нам"  href="mailto:<?php echo Yii::app()->params['publicEmail']?>" ><img width="35px" alt="Написать нам" src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/mail.png"/></a>
                <a target="_blank" title="Мы на YouTube" href="http://www.youtube.com/user/dprofmsk/feed" ><img width="35px" alt="Ны на YouTube"  src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/youtube.png"/></a>
                <a target="_blank" title="Мы ВКонтакте" href="http://vk.com/mzd_molodej" ><img width="35px" alt="Ны ВКонтакте" src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/vk.png"/></a>
            </div>
            <div class="span-7 prepend-11 last">
                <p>
                    <small>
                        Москва, 107996, Краснопрудная улица, дом 20 <br>
                        приёмная +7 (499) 266 0617, факс +7 (499) 266 0745
                    </small>
                </p>
            </div>
        </div>

        <div class="clear"></div>
    </div><!-- content -->
</div>
<?php $this->endContent(); ?>