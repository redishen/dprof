<?php $this->beginContent('//layouts/main-bem'); ?>
<div class="container">
    <div class="span-6" id="sidebar">
        <div class="clear">&nbsp;</div>

        <?php //if (!Yii::app()->user->isGuest) $this->widget('ext.widgets.juimenu.CJuiMenu', array('items' => $this->menu,)); ?>
        <?php if (Yii::app()->getModule('user') && Yii::app()->getModule('user')->isAdmin()) $this->widget('UserMenu'); ?>

        <?php $this->widget('MenuPortlet'); ?>

        <a class="sidebar__doc" href="/doc/index?cat=72" >
                <i class="fa fa-file-text-o icon" aria-hidden="true"></i>
                Документы
        </a>

        <?php $this->widget('RegionMenuPortlet'); ?>

        <?php
        $this->widget('TagCloud', array(
            'maxTags' => Yii::app()->params['tagCloudCount'],
        ));
        ?>

        <?php
        $this->widget('Banners', array(
            'id' => 'bn',
        ));
        ?>
    </div>
    <div class="span-17 last">
        <div class="clear">&nbsp;</div>
        <div id="content">

            <?php
            $this->widget('zii.widgets.CBreadcrumbs', array(
                'links' => $this->breadcrumbs,
            ));
            ?><!-- breadcrumbs -->

            <?php echo $content; ?>

        </div><!-- content -->
    </div>
    <hr/>
    <div class="row">
        <div  class="span-4 prepend-1">      
            <a title="Написать нам"  href="mailto:<?php echo Yii::app()->params['publicEmail']?>" ><img width="35px" alt="Написать нам" src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/mail.png"/></a>
            <a target="_blank" title="Мы на YouTube" href="http://www.youtube.com/user/dprofmsk/feed" ><img width="35px" alt="Ны на YouTube"  src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/youtube.png"/></a>
            <a target="_blank" title="Мы ВКонтакте" href="http://vk.com/mzd_molodej" ><img width="35px" alt="Ны ВКонтакте" src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/social/vk.png"/></a>
        </div>
        <div class="span-7 prepend-12 last">
            <p>
                <small>
                    Москва, 107996, Краснопрудная улица, дом 20 <br>
                    приёмная +7 (499) 266 0617, факс +7 (499) 266 0745
                </small>
            </p>
        </div>
    </div>

    <div class="clear"></div>

</div>
<?php $this->endContent(); ?>