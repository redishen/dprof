<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="language" content="ru"/>
    <link rel="shortcut icon" href="<?php echo Yii::app()->request->baseUrl; ?>/css/img/favicon.ico"/>
    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css"
          media="screen, projection"/>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css"
          media="print"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css"/>

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css"
          media="screen, projection"/>
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.min.css?141217"/>



    <?php /* JavaScripts */
    Yii::app()->clientScript
        ->registerCoreScript('jquery', CClientScript::POS_END)
        ->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap.min.js', CClientScript::POS_END)
    ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datepicker.min.js', CClientScript::POS_END); ?>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/bootstrap-datepicker.ru.min.js', CClientScript::POS_END); ?>

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body class="page">
<?php $this->widget('ext.widgets.yandex.MetrikaWidget'); ?>

<?php Yii::app()->clientScript->registerScript('olool', " jQuery(window).bind('scroll', function() {
            if (jQuery(window).scrollTop() > $('#header').height()) {
                jQuery('#mainmenu').addClass('fixed'); 
            }
            else {
                jQuery('#mainmenu').removeClass('fixed');
            }
        });
        $('#search-input').focus(function() {
            this.style.backgroundColor = '#F0F0F0';
            this.value = '';
        } )
        $('#search-input').blur(function() {
            this.style.backgroundColor = '#FFFFFF';
            if (this.value != '') {
                this.onfocus = function() {
                    this.style.backgroundColor = '#F0F0F0';
                };
            } else
                this.value = 'Поиск по сайту';
        })
        
") ?>

<div class="bgShadow">
    <div class="shadowTop"></div>
    <div class="shadowTopSub">
        <div class="shadowFooter">
            <div class="container" id="page">

                <div id="header">

                    <div id="logo">
                        <?php //echo CHtml::encode(Yii::app()->name); ?>
                        <div id="logo-pic">
                            <img src="<?php echo Yii::app()->request->baseUrl; ?>/css/img/logo-title-big.png"/></div>
                        <div id="site-title">
                            ДОРОЖНАЯ ТЕРРИТОРИАЛЬНАЯ ОРГАНИЗАЦИЯ РОСПРОФЖЕЛ<br/>
                            НА МОСКОВСКОЙ ЖЕЛЕЗНОЙ ДОРОГЕ
                        </div>
                        <div id="site-title-transparent">
                        </div>

                        <div id="site-contacts">
                            <strong>"Горячая линия"<br/>
                                +7 (499) 266-23-81<br/>
                                +7 (499) 266-38-44 <br/>
                                пн-пт с 9-00 до 18-00<br/></strong>
                        </div>
                        <div id="site-contacts-transparent">
                        </div>


                    </div>
                    <?php $this->widget('HeaderSlideWidget'); ?>
                    <div id="logoPic">
                    </div>

                </div><!-- header -->


                <div id="content-begin">
                    <div id="mainmenu" class="mainFixed">
                        <?php
                        echo CHtml::beginForm(array('post/search'));
                        $this->widget('zii.widgets.CMenu', array(
                            'firstItemCssClass' => 'first-menu',
                            'encodeLabel' => false,
                            'items' => array(
                                array('label' => 'Главная', 'url' => array('/site/index')),
                                array('label' => 'Новости', 'url' => array('/post/index')),
                                array('label' => 'Электронная приемная', 'url' => array('/site/contact')),
                                array('label' => 'О нас', 'url' => array('/site/page', 'view' => 'about')),
                                array('label' => 'Контакты', 'url' => array('/site/page', 'view' => 'contact')),
                                array('label' => '<input name = "search-str" id = "search-input" value="Поиск по сайту" />', 'itemOptions' => array('style' => 'float: right; margin-top: -3px;')),
                            ),
                        ));
                        echo CHtml::endForm();
                        ?>
                    </div><!-- mainmenu -->
                </div>


                <?php if (Yii::app()->user->hasFlash('success')): ?>
                    <?php $this->widget('ext.widgets.flash.Flashes'); ?>
                <?php endif; ?>
                <?php echo $content; ?>

                <div id="footer">
                    Copyright &copy; <?php echo date('Y'); ?> "Дорожный комитет РОСПРОФЖЕЛ Московской железной
                    дороги"<br/>
                    При использовании материалов <a href="http://www.dprof.ru">активная ссылка</a> на сайт обязательна.
                    <? if (!Yii::app()->user->isGuest): ?>
                        <?= CHtml::link('Выйти (' . Yii::app()->user->name . ')', array('/site/logout')) ?>

                    <? else: ?>
                        <?= CHtml::link('Войти', array('/site/login')) ?>

                    <? endif; ?>
                    <br/>
                    <?php echo Yii::powered(); ?>
                </div><!-- footer -->

            </div><!-- page -->
        </div>
    </div>
</div>
<?php //$this->widget('yiinltcs.YiinalyticsScript'); ?>
<?php if (!Yii::app()->params['isWeb'] && !Yii::app()->params['isLocal']): ?>
    <!-- Piwik -->
    <script type="text/javascript">
        var _paq = _paq || [];
        _paq.push(['trackPageView']);
        _paq.push(['enableLinkTracking']);
        (function () {
            var u = "//10.23.250.9/piwik/";
            _paq.push(['setTrackerUrl', u + 'piwik.php']);
            _paq.push(['setSiteId', 1]);
            var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
            g.type = 'text/javascript';
            g.async = true;
            g.defer = true;
            g.src = u + 'piwik.js';
            s.parentNode.insertBefore(g, s);
        })();
    </script>
    <noscript><p><img src="//10.23.250.9/piwik/piwik.php?idsite=1" style="border:0;" alt=""/></p></noscript>
    <!-- End Piwik Code -->
<?php endif; ?>
</body>


</html>