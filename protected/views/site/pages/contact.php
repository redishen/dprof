<?php
$this->pageTitle = Yii::app()->name . ' - Контакты';
$this->breadcrumbs = array(
    'Контакты',
);
?>

<h1 style="text-align: left;">
    ДОРПРОФЖЕЛ<br>
    НА МОСКОВСКОЙ ЖЕЛЕЗНОЙ ДОРОГЕ</h1>


<hr />
<p>
    <span style="font-size:16px;"><strong>Адрес:</strong></span> <span style="font-size:14px;">Москва, 107996, Краснопрудная улица, дом 20.</span></p>
<p>
    <span style="font-size:16px;"><strong>Телефон приёмной Дорпрофжел:&nbsp;</strong></span><span style="font-size:14px;">+7 (499) 266 0617</span></p>
<p>
    <span style="font-size:16px;"><strong>Факс:</strong></span> <span style="font-size:14px;">+7 (499) 266 0745</span></p>
<p>
    <span style="font-size:16px;"><strong>Электронная почта:</strong></span> <span style="font-size:14px;"><a title="Написать нам"  href="mailto:<?php echo Yii::app()->params['publicEmail']?>" ><?php echo Yii::app()->params['publicEmail']?></a></span></p>

<p>
    <strong><span style="font-size:16px;">Как к нам проехать:</span></strong></p>
<p>
    <strong><span style="font-size:16px;"><img alt="Карта проезда" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/map.gif"/></span></strong></p>
<p style="text-align: center;">
    По вопросам функционирования и наполненности сайта пишите: <strong><?php echo Yii::app()->params['publicSupportEmail']?></strong></p>
