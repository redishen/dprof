<?php
$this->pageTitle = Yii::app()->name . ' - О нас';
$this->breadcrumbs = array(
    'О нас',
);
?>

<style>
    .html_carusel .buttons {
        float: right;
        /*position: absolute;*/
    }

    div.html_carusel .pagination a {
        text-decoration: none;
/*        border-bottom: 1px dashed;*/
        font-size: 120%;
    }

    div.html_carusel .pagination a.selected {
        border-bottom: 1px solid;
        font-size: 150%;
        color: lightcoral;
    }

    .html_carusel #foo {
        margin: 40px auto;
        /*border: 1px solid #ccc;*/

    }

    .html_carusel .slide{
        /*        margin: 40px auto;*/
        /*border: 1px solid #ccc;*/

    }



</style>

<script>
//    $(document).ready(function() {
//        $("#logo").slideUp(1000);
//    })
</script>

<div class="html_carusel">
    <div class="buttons">
        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'name' => 'button_prev',
            'id' => '_prev',
            'caption' => 'ПРЕД.',
            'url' => '#',
            'buttonType' => 'link',
//            'htmlOptions' => array('class' => 'btn-more',),
            'options' => array(
                'icons' => array(
                    'primary' => 'ui-icon-carat-1-w',
                ),
            ),
        ));
        ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'name' => 'button_next',
            'id' => '_next',
            'caption' => 'СЛЕД.',
            'url' => '#',
            'buttonType' => 'link',
//            'htmlOptions' => array('class' => 'btn-more',),
            'options' => array(
                'icons' => array(
                    'secondary' => 'ui-icon-carat-1-e',
                ),
            ),
        ));
        ?>
    </div>

    <div id="foo" class="span-22">

        <div class="slide span-23">
            <h1>Профсоюз на защите прав и интересов человека труда</h1>
            <hr/>
            <p>
                Деятельность профсоюза широка и многогранна. Его главное предназначение обеспечение социальной защиты работников. Надежным гарантом социальной защищенности являются коллективные договоры ОАО &quot;РЖД&quot; и Московской железной дорогифили ала ОАО &quot;РЖД&quot;.</p>
            <p>
                Благодаря утвердившемуся на Московской железной дороге принципу социального партнерства создан и успешно действует механизм социальной защиты работников.</p>

            <div class="span-10">
                <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about01.gif" style="width: 300px; height: 403px; float: left;" /><strong>
            </div>
            <div class="span-10 last">
                Коллективные договоры ОАО&laquo;РЖД&raquo; Московской железной дороги - филиала ОАО &laquo;РЖД&raquo; включают в себя обязательства сторон по обеспечению гарантий работников в области:</strong>
                <ul>
                    <li>
                        Оплата труда</li>
                    <li>
                        Занятости</li>
                    <li>
                        Условий и охраны труда</li>
                    <li>
                        Жилищного обслуживания</li>
                    <li>
                        Культуры и спорта</li>
                    <li>
                        Оздоровления и отдыха</li>
                    <li>
                        Образования и подготовки кадров</li>
                    <li>
                        Соблюдения прав членов профсоюза</li>
                </ul>
            </div>

        </div>
        <div class="slide span-23">
            <h1>Преимущества членов РОСПРОФЖЕЛ</h1>
            <hr/>
            <ul>
                <li>
                    бесплатные юридические консультации и помощь по правовым вопросам;</li>
                <li>
                    защита от не обоснованного увольнения по инициативе работодателя;</li>
                <li>
                    индивидуальная защита приреализации коллективного договора и его положений;</li>
                <li>
                    консультации и помощь в сложных жизненных ситуациях;</li>
                <li>
                    бесплатное представительство профсоюзами интересов и отстаивание их в судах и комиссиях по трудовым спорам;</li>
                <li>
                    содействие реализации законодательных гарантий и прав молодежи научебу, труд и жилье, достойный доход, полноценный отдых и досуг;</li>
                <li>
                    гарантированная защита при расследовании несчастного случая на производстве и связанного с производством;</li>
            </ul>
            <div class="span15 prepend-4" >
                <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about02-1.gif"/>
            </div>
            <div class="span15 prepend-4" >
                <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about02-2.gif"/>
            </div>
            <ul>
                <li>
                    дополнительные гарантии в случаях производственного травматизма и профзаболевания;</li>
                <li>
                    страхование от несчастных случаев в быту за счет средств профсоюза;</li>
                <li>
                    дополнительный контроль за соблюдением трудового законодательства, охраны труда, техники безопасности, выдачи спецодежды и средств индивидуальной защиты;</li>
                <li>
                    возможность участия в культурномассовых и спортивных мероприятиях, организуемых профсоюзом;</li>
                <li>
                    возможность получения путевок в лечебные учреждения, дома отдыха и пансионаты, новогодних подарков для детей, приобретенных за счет средств профсоюза;</li>
                <li>
                    получение образования на льготных условиях через систему профсоюзных высших и средних учебных заведений;</li>
                <li>
                    материальная помощь и моральное поощрение профсоюза.</li>
            </ul>


        </div>
        <div class="slide span-23">
            <h1>Условия и охрана труда</h1>
            <hr/>
            <div class="span-14">
                <p>
                    Практическая деятельность профсоюза постоянно направлена на реализацию неотъемлемого права своих членов на здоровые и безопасные условия труда, получение льгот и компенсаций за вредный труд, возмещение в повышенном размере ущерба, причиненного здоровью на производстве, обеспечение работников спецодеждой, спецобувью и предохранительными приспособлениями. 
                </p>



                <div class="span-7">
                    <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about03-1.jpg"/>
                </div>
                <div class="span-6 last">
                    <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about03-4.jpg"/>
                </div>
            </div>
            <div class="span-6 last">
                <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about03-2.jpg"/>
                <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about03-3.jpg"/>

            </div>

        </div>
        <div class="slide span-23">
            <h1>Отдых и оздоровление работников</h1>
            <hr/>
            <div class="span-14">
                <p>Важным слагаемым социальной защищенности железнодорожников является организация отдыха в дорожных пансионатах и санаториях профилакториях.
                </p> 
                <p>"Московский железнодорожник" на Черноморском побережье и "Березовая Роща" в Подмосковье излюбленные пансионаты железнодорожников.
                </p>
                <p>Открыты двери в санаториях профилакториях "Красный Бор" в Смоленской области и "Синезерки", "Унеча" в Брянской области.
                </p>

                <div class="span-7">
                    <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about04-3.jpg"/>
                </div>
                <div class="span-6 last">
                    <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about04-2.jpg"/>
                    <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about04-4.jpg"/>
                </div>
            </div>
            <div class="span-6 last">
                <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about04-1.jpg"/>
            </div>
        </div>
        <div class="slide span-23">
            <h1>Физкультура и спорт</h1>
            <hr/>
            <div class="span-14">
                <p><strong>Профсоюз</strong> проявляет большую заботу о физическом воспитании железнодорожников и развитии спорта.
                </p> 
                <div class="span-7">
                    <img alt="" style="margin-bottom: 10px"  src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about05-3.jpg"/>
                    <p><strong>К услугам железнодорожников</strong> спортивные залы, стадионы, спортплощадки, лыжныебазы, легкоатлетический манеж, бассейн, тир, база рыбака и охотника. 
                    </p>
                </div>
                <div class="span-6 last">
                    <p>Дорожный физкультурно спортивный клуб <strong>"Локомотив"</strong> объединяет 7 клубов на отделениях Московской железной дороги и 3 клуба в дирекциях.
                    </p>
                    <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about05-5.jpg"/> 
                </div>
            </div>
            <div class="span-6 last">
                <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about05-1.jpg"/>
                <img alt="" style="margin: 10px" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about05-4.jpg"/>
            <!--    <img alt="" style="margin: 10px" src="/files/site/pic/about05-2.jpg"/>-->
            </div>

        </div>
        <div class="slide span-23">
            <h1>Культура</h1>
            <hr/>
            <div class="span-14">

                <div class="span-7">
                    <p><strong>Широкие возможности</strong> созданы для организации культурного отдыха и развития талантов железнодорожников и членов их семей. Этой цели служат имеющиеся на Московской железной дороге <strong>дворцы и дома культуры</strong>, в которых работают <strong>440</strong> кружков и занимается свыше <strong>9000</strong> железнодорожников и членов семей.
                    </p> 
                    <p>Периодически за счёт профсоюза проводятся <strong>экскурсии</strong> по памятным местам России и зарубежных стран. 
                    </p>
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about06-3.jpg"/></p>
                </div>
                <div class="span-6 last">
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about06-4.jpg"/></p>
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about06-1.jpg"/></p>

                </div>
            </div>
            <div class="span-6 last">
                <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about06-5.jpg"/></p>
                <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about06-2.jpg"/> </p>

            </div>

        </div>
        <div class="slide span-23">
            <h1>Забота о ветеранах труда</h1>
            <hr/>
            <div class="span-14">

                <div class="span-7">
                    <p>Профсоюзные комитеты вместе хозяйственными руководителями проявляют особую заботу о ветеранах труда и неработающих пенсионерах. На Московской железной дороге их <strong>более 80 тысяч</strong>.
                    </p> 
                    <p>Ветеранское движение на Московской железной дороге возглавляет <strong>Совет ветеранов войны и труда</strong>. Аналогичные Советы созданы на отделениях и в дирекциях. Ветераны труда <strong>ведут большую общественную работу</strong> в трудовых коллективах, среди молодёжи.
                    </p>
                    <p>Ветераны получают, наряду государственной пенсией, негосударственную пенсию из <strong>НПФ "Благосостояние"</strong> или ежемесячную материальную помощь из фонда <strong>"Почет"</strong>. 
                    </p>

                </div>
                <div class="span-6 last">
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about07-2.jpg"/></p>
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about07-3.jpg"/></p>

                </div>
            </div>
            <div class="span-6 last">
                <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about07-1.jpg"/></p>
                <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about07-4.jpg"/></p>


            </div>

        </div>
        <div class="slide span-23">
            <h1>Дети - наше будущее</h1>
            <hr/>

            <div class="span-14">
                <p>
                    <strong>Забота о детях</strong> железнодорожников, организация их оздоровления и отдыха одно из <strong>приоритетных </strong>направлений совместной работы хозяйственных руководителей и профсоюзов.</p>
                <p>
                    На Московской железной дороге действует<strong> 47</strong> государственных до школьных образовательных учреждений, которые посещают <strong>4867 </strong>ребенка, из них около <strong>4000 </strong>дети железнодорожников.</p>

                <div class="span-7">
                    <p>
                        На Московской железной дороге функционируют <strong>10</strong> детских оздоровительных лагерей, что полностью удовлетворяет потребность в проведении летнего отдыха детворы.</p>
                    <p>
                        Все дети на время пребывания в лагерях застрахованы в страховом обществе<strong> &quot;ЖАСО&quot;</strong>.</p>

                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about08-3.jpg"/></p>
                </div>
                <div class="span-6 last">
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about08-4.jpg"/></p>
                    <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about08-2.jpg"/></p>

                </div>
            </div>
            <div class="span-6 last">
                <p><img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/pic/about08-1.jpg"/></p>

            </div>

        </div>

    </div>
    <div class="clear"></div>
    <div class="pagination" ></div>

</div>


<?php
$this->widget('ext.carouFredSel.ECarouFredSel', array(
    'id' => 'carousel',
    'target' => '#foo',
    'config' => array(
        'auto' => FALSE,
        'auto' => array(
            'pauseOnHover' => TRUE,
            ),
//        'width' => 900,
//        'height' => "auro",
//        'mousewheel' => true,
        'direction' => 'left',
        'pauseOnHover' => TRUE,
        'pagination' => array(
            'container' => '.pagination',
            'anchorBuilder' => new CJavaScriptExpression("function( nr ) {
                        var str  = '<a href=#><span>'
			switch(nr) {
			case 1: str += 'Профсоюз на защите прав и интересов человека труда'; break;
			case 2: str += 'Преимущества членов РОСПРОФЖЕЛ'; break;
			case 3: str += 'Условия и охрана труда'; break;
			case 4: str += 'Отдых и оздоровление работников'; break;
                        case 5: str += 'Физкультура и спорт'; break;
                        case 6: str += 'Культура'; break;
                        case 7: str += 'Забота о ветеранах труда'; break;
                        case 8: str += 'Дети - наше будущее'; break;
			}
                        str += '</span><br></a>'
                        return str;
			}"),
        ),
        'prev' => array(
            'button' => '#_prev',
            'key' => 'right',
        ),
        'next' => array(
            'button' => '#_next',
            'key' => 'left',
        ),
        'items' => array(
            'visible' => 1,
            'minimum' => 1,
//            'width' => 900,
//            'height' => "100%",
        ),
        'scroll' => array(
            'items' => 1,
            'easing' => 'swing',
            'fx' => 'slide',
//                        'duration' => 800,
//                        'pauseDuration' => 1500,
//                        'pauseOnHover' => false,
//                        'fx' => 'crossfade',
        ),
    ),
));
?>