<?php
$this->pageTitle = 'О нас - ' . Yii::app()->name;

$this->breadcrumbs = array(
    'О нас',
);
?>

<style>
    .html_carusel .buttons {
        float: right;
        /*        margin-bottom: -50px;*/
        position: static;
        right: 0;
        z-index: 10;
    }

    div.html_carusel .pagination a {
        text-decoration: none;
        /*        border-bottom: 1px dashed;*/
        font-size: 120%;
    }

    div.html_carusel .pagination a.selected {
        border-bottom: 1px solid;
        font-size: 150%;
        color: lightcoral;
    }

    .html_carusel #foo {
        margin: 0px 0px;
        /*border: 1px solid #ccc;*/

    }

    .html_carusel .slide{
        /*        margin: 40px auto;*/
        /*border: 1px solid #ccc;*/

    }



</style>

<script>
//    $(document).ready(function() {
//        $("#logo").slideUp(1000);
//    })
</script>

<div class="html_carusel">
    <div class="buttons">
        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'name' => 'button_prev',
            'id' => '_prev',
            'caption' => 'Назад',
            'url' => '#',
            'buttonType' => 'link',
//            'htmlOptions' => array('class' => 'btn-more',),
            'options' => array(
                'icons' => array(
                    'primary' => 'ui-icon-carat-1-w',
                ),
            ),
        ));
        ?>
        <?php
        $this->widget('zii.widgets.jui.CJuiButton', array(
            'name' => 'button_next',
            'id' => '_next',
            'caption' => 'Далее',
            'url' => '#',
            'buttonType' => 'link',
//            'htmlOptions' => array('class' => 'btn-more',),
            'options' => array(
                'icons' => array(
                    'secondary' => 'ui-icon-carat-1-e',
                ),
            ),
        ));
        ?>
    </div>

    <div id="foo" class="span-22">
        <div class="slide span-23">
            <h1>Руководство Дорпрофжел на Московской ЖД</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/01.jpg" style="width: 950px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Социальное партнёрство</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/02.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Преимущества членов РОСПРОФЖЕЛ</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/03.jpg" style="width: 885px; height: 800px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Правовая защита членов РОСПРОФЖЕЛ</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/04.jpg" style="width: 920px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Условия и охрана труда</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/05.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Отдых и оздоровление работников. Экскурсионные программы</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/06.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Физкультура и спорт</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/07.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Забота о ветеранах</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/08.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Дети – наше будущее</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/09.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>
        <div class="slide span-23">
            <h1>Профсоюз и молодежь – крепче сплава не найдешь</h1>
            <hr/>
            <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/about/10.jpg" style="width: 910px; height: 600px; float: left;" />
        </div>




    </div>
    <div class="clear"></div>
    <!--<hr class="space">-->
    <div class="pagination" ></div>

</div>


<?php
$this->widget('ext.carouFredSel.ECarouFredSel', array(
    'id' => 'carousel',
    'target' => '#foo',
    'config' => array(
        'auto' => FALSE,
//        'auto' => array(
//            'pauseOnHover' => TRUE,
//        ),
//        'width' => 900,
//        'height' => "auro",
//        'mousewheel' => true,
        'direction' => 'left',
        'pauseOnHover' => TRUE,
        'pagination' => array(
            'container' => '.pagination',
            'anchorBuilder' => new CJavaScriptExpression("function( nr ) {
                        var str  = '<a href=#><span>'
			switch(nr) {
			case 1: str += 'Руководство Дорпрофжел на Московской ЖД'; break;
                        case 2: str += 'Социальное партнёрство'; break;
			case 3: str += 'Преимущества членов РОСПРОФЖЕЛ'; break;
			case 4: str += 'Правовая защита членов РОСПРОФЖЕЛ'; break;
                        case 5: str += 'Условия и охрана труда'; break;
                        case 6: str += 'Отдых и оздоровление работников. Экскурсионные программы'; break;
                        case 7: str += 'Физкультура и спорт'; break;
                        case 8: str += 'Забота о ветеранах'; break;
                        case 9: str += 'Дети – наше будущее'; break;
                        case 10: str += 'Профсоюз и молодежь – крепче сплава не найдешь'; break;
			}
                        str += '</span><br></a>'
                        return str;
			}"),
        ),
        'prev' => array(
            'button' => '#_prev',
            'key' => 'right',
        ),
        'next' => array(
            'button' => '#_next',
            'key' => 'left',
        ),
        'items' => array(
            'visible' => 1,
            'minimum' => 1,
//            'width' => 900,
//            'height' => "100%",
        ),
        'scroll' => array(
            'items' => 1,
            'easing' => 'swing',
            'fx' => 'slide',
//                        'duration' => 800,
//                        'pauseDuration' => 1500,
//                        'pauseOnHover' => false,
//                        'fx' => 'crossfade',
        ),
    ),
));
?>