<?php
$this->pageTitle = 'Главная - ' . Yii::app()->name;
?>

<div class="span-11 last">

</div>


<div class="page__counter">
    <strong>
        <span class="counter__part" title="Просмотров всего"><i class="fa fa-eye" aria-hidden="true"></i><span class="counter__views counter__views_all"><i class="fa fa-refresh fa-spin fa-fw"></i></span></span>
        <span class="counter__part" title="Посетителей сегодня"><i class="fa fa-user-o" aria-hidden="true"></i><span class="counter__views counter__views_today"><i class="fa fa-refresh fa-spin fa-fw"></i></span></span>
        <span class="counter__part" title="Сейчас на сайте"><i class="fa fa-user-o highlight-green"></i><span class="counter__views counter__views_online"><i class="fa fa-refresh fa-spin fa-fw"></i></span></span>
    </strong>
    <script>
        $(function() {
            var counterAll = $('.counter__views_all'),
                counterToday = $('.counter__views_today'),
                counterOnline = $('.counter__views_online');

            $.ajax({
                dataType: 'json',
                url: '/site/getCounters',
            }).done(function(data) {
                counterAll.text(data.all);
                counterToday.text(data.today);
                counterOnline.text(data.online);
            }).fail(function() {
                counterAll.text('–');
                counterToday.text('–');
                counterOnline.text('–');
            });
        });
    </script>
</div>

<? /** @var CDateFormatter $formatter */
if ($banner = Banner::getCurrent()) : ?>
    <?
    $formatter = Yii::app()->dateFormatter;
    $hasLink = !empty($banner->link);
    $hasDate = !empty($banner->date_from);
    ?>
    <div class="banner banner_style_top">
        <div class="banner__wrap <?= $hasDate ? '' : 'banner__wrap_bg' ?>">
            <? if ($hasLink) : ?>
                <a class="banner__link" href="<?= $banner->link ?>">
                <? endif; ?>
                <? if (!empty($banner->date_from)) : ?>
                    <div class="banner__calendar">
                        <div class="banner__meta">
                            <div class="banner__date"><?= $formatter->format('dd MMMM', $banner->date_from) ?></div>
                            <div class="banner__time"><?= $formatter->format('HH:mm', $banner->date_from) ?></div>
                        </div>
                        <? if (!empty($banner->date_note)) : ?>
                            <div class="banner__extra">
                                <?= $banner->date_note ?>
                            </div>
                        <? endif; ?>
                    </div>
                <? endif; ?>
                <div class="banner__body <?= $hasDate ? '' : 'banner__body_full' ?>">
                    <div class="banner__header">
                        <?= $banner->header ?>
                    </div>
                    <div class="banner__note">
                        <?= $banner->description ?>
                    </div>
                </div>
                <div class="clear"></div>
                <? if ($hasLink) : ?>
                </a>
            <? endif; ?>
        </div>
    </div>
<? endif; ?>

<div class="clear"></div>

<div class="span-14">

    <h1 class="" style="font-size: 26px;">Уважаемые коллеги, члены РОСПРОФЖЕЛ!</h1>

    <img src="/files/site/shuliansky.jpg" class="top pull-1" style="float: left; margin: 0 1.5em 0 0" alt="Шулянский Дмитрий Алексеевич">
    <div class="lead text-justify">
        На протяжении 115-летней истории Российский профессиональный союз железнодорожников и транспортных строителей, независимо от происходящих в стране исторических и экономических процессов, отстаивает социально-экономические интересы железнодорожников. Немалая роль в этой работе отведена Дорожной территориальной организации Профсоюза на Московской железной дороге – одной из самых крупных общественных организаций в стране.
    </div>
    <p class=" text-right">
        <a style="" href="/index.php/post/view?id=794">
            <i>читать далее...</i>
        </a>
    </p>
    <p class="muted">
        <i>Председатель Дорпрофжел<br> на Московской железной дороге<br> Шулянский Дмитрий Алексеевич</i>
    </p>
</div>

<div class="span-9 pull-right last">


    <div class="calendar-wrapper">
        <div class="dk-calendar dk-calendar_loading">
            <div class="dk-calendar__heading"><i class="fa fa-calendar" aria-hidden="true"></i>Мероприятия</div>
            <div class="dk-calendar__popover" data-original-title="" title=""></div>
            <div class="dk-calendar__holder"></div>
        </div>

    </div>
    <script>
        $(document).ready(function() {

            var beforeShowDay = function(events, date) {
                var res = {
                    enabled: false
                };
                var eventsByDate = getEventsByDate(date, events);
                if (eventsByDate.length) {
                    res.enabled = true;
                    res.classes = 'active inner';
                    res.tooltip = event.title;
                }
                return res;
            };

            var showPop = function(popover, date, d) {
                if (d && date.getDate() === d) {
                    popover.toggle();
                } else {
                    popover.show();
                }
                popover.find('.dk-calendar__popover-close').on('click', function() {
                    popover.hide();
                });
            };

            var getEventsByDate = function(date, events) {
                var eventData = [];

                $.each(events, function(key, event) {
                    var from = new Date(event.at);
                    from.setHours(0, 0, 0, 0);
                    if (from.getTime() === date.getTime()) {
                        eventData.push(event);
                    }
                });
                return eventData;
            };

            var onChangeDate = function(e, events, popover, dp) {
                var date = e.date;
                var currentDate = dp.data('selectedDate');
                dp.data('selectedDate', date.getDate());
                var eventData = getEventsByDate(date, events);
                if (date.getDate() >= 15) {
                    dp.addClass('dk-calendar_pos_up');
                    dp.removeClass('dk-calendar_pos_down');
                } else {
                    dp.addClass('dk-calendar_pos_down');
                    dp.removeClass('dk-calendar_pos_up');
                }
                popover.html(getPopHtml(eventData, date));
                showPop(popover, date, currentDate);
            };

            var getPopHtml = function(eventData, date) {
                var a = '';
                if (eventData.length) {
                    var dateHuman = eventData[0].dateHuman;
                    $.each(eventData, function(k, v) {
                        var link = v.url;
                        a += '<a href="' + link + '"><b>' +
                            v.title + '</b>' +
                            (v.content ? ' (' + v.content + ')' : '') +
                            '</a>';
                    });
                }

                var $close = $('<div/>').addClass('dk-calendar__popover-close').text("×");
                return $close[0].outerHTML + '<h3>' + dateHuman + '</h3>' + a;
            };

            var loadData = function() {
                $.getJSON('/post/getEvents', function(events) {
                    var dp = $('.dk-calendar');
                    var popover = dp.find('.dk-calendar__popover').popover();
                    dp.find('.dk-calendar__holder').datepicker({
                        todayBtn: false,
                        language: 'ru',
                        todayHighlight: false,
                        beforeShowDay: function(date) {
                            return beforeShowDay(events, date);
                        }
                    });
                    dp.on('changeDate', function(e) {
                        onChangeDate(e, events, popover, dp);
                    });
                    dp.removeClass('dk-calendar_loading');
                });
            };

            loadData();
        })
    </script>


</div>

<hr class="space" />


<div class="clear"></div>
<!--<hr class="space"/>-->
<div class="span-6">
    <div class="sidebar__top-nav">
        <a class="banner banner_type_info banner_style_sidebar" href="<?php echo Yii::app()->createUrl('post/index', array('cat' => 55)) ?>">
            <div class="banner__label">
                <i class="fa fa-play banner__icon" aria-hidden="true"></i>
                Проект «ПрофИнфо»
            </div>
        </a>
        <a class="banner  banner_type_help banner_style_sidebar" href="<?php echo Yii::app()->createUrl('post/index', array('cat' => 20)) ?>">
            <div class="banner__label">
                <i class="fa fa-thumbs-o-up banner__icon" aria-hidden="true"></i>
                Профсоюз помог
            </div>
        </a>
        <a class="banner  banner_type_doc banner_style_sidebar" href="<?php echo Yii::app()->createUrl('doc/index?cat=72') ?>">
            <div class="banner__label">
                <i class="fa fa-file-text-o banner__icon" aria-hidden="true"></i>
                Документы
            </div>
        </a>
        <a class="banner  banner_type_doc banner_style_sidebar" href="<?php echo Yii::app()->createUrl('post/index', array('cat' => 87)) ?>">
            <div class="banner__label">
                <i class="fa fa-newspaper-o banner__icon" aria-hidden="true"></i>
                Дорпрофжел в СМИ
            </div>
        </a>
        <style>
            .banner.banner_type_reso {
                font-size: 12px;
                background-color: #23803F;
                background-image: linear-gradient(90deg, rgba(90, 168, 68, 0) 3%, #5AA844 100%);
            }

            .banner.banner_type_image {
                background-color: transparent;
                box-sizing: border-box;
                padding-right: 10px;
            }

            .banner.banner_type_image img {
                max-width: 100%;
            }
        </style>
        <a class="banner banner_type_reso banner_style_sidebar" href="<?php echo Yii::app()->createUrl('special/reso') ?>">
            <div class="banner__label">
                <i class="fa fa-percent banner__icon" aria-hidden="true"></i>
                Скидка на страхование
            </div>
        </a>
    </div>

    <?php
    $this->widget('zii.widgets.CMenu', array(
        'items' => Category::model()->getOneLevelMenu(1),
        'id' => 'side-menu',
    ));
    ?>

    <hr class="space" />



    <hr class="space" />
    <h1 class="sidebar__header">Подразделения</h1>
    <?php
    $this->widget('zii.widgets.CMenu', array(
        'items' => RegionMenuPortlet::getMenuContent(),
        'id' => 'side-menu',
    ));
    ?>
    <hr class="space" />
    <!--<h2>Ваше мнение</h2>-->
    <?php
    //    $this->widget('poll.components.PollWidget', array('poll_id' => 1));
    ?>
    <hr class="space" />
    <h2 class="sidebar__header">Полезные ссылки</h2>
    <?php
    $this->widget('BannersWidget');
    ?>
</div>
<div class="span-16 last ">
    <?php
    $this->widget('ext.widgets.DListView.DListView', array(
        'dataProvider' => $dataProvider,
        'itemView' => '_preview',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории',
    ));
    ?>
    <div class="more-link">
        <a href="<?php echo Yii::app()->createUrl('post/index') ?>"><span class="icon"></span>ВСЕ НОВОСТИ</a>
    </div>
</div>

<hr class="space" />

<div id="profhelpnews">
    <h1><img src="<?php echo Yii::app()->request->baseUrl; ?>/files/site/logo-small.png" class="top" alt="test" />
        Профсоюз помог!</h1>
    <hr />
    <?php
    $this->widget('zii.widgets.CListView', array(
        'dataProvider' => $profHelpDP,
        'itemView' => '_preview',
        'template' => "{items}\n{pager}",
        'emptyText' => 'Нет статей по этой категории',
    ));
    ?>
</div>