<?php

$pageTitle = 'Электронная приемная председателя Дорпрофжел';

$this->pageTitle= $pageTitle . ' - ' . Yii::app()->name;
$this->breadcrumbs=false;
?>



<?php if(Yii::app()->user->hasFlash('contact')): ?>

<div class="flash-success alert alert-success">
	<?php echo Yii::app()->user->getFlash('contact'); ?>
</div>

<?php else: ?>

<div class="bs" style="margin-top: 32px;">
    <div class="i-bem">
        <div class="container-fluid">
			
			<h1 class="col-md-12 " style="
    		font-size: 32px;
    		line-height: 1;
			margin-bottom: 24px"><?php echo $pageTitle ?></h1>
            <form class="col-md-12 " method="post" action="#form" id="form" style="
			margin-bottom: 32px">
                <div class="" style="
    		font-size: 18px;
    		line-height: 1;
			margin-bottom: 24px">Есть проблема? Пишите!</div>

                <? if($model->hasErrors()): ?>
                    <div class="alert alert-danger" role="alert">
                        <?= CHtml::errorSummary($model) ?>
                    </div>
                <? endif; ?>

                <? if($success): ?>
                    <div class="alert alert-success" role="alert">Ваша заявка успешно отправлена</div>
                <? else: ?>

    
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="">Ваше имя</label>
                    </div>
                    <div class="col col-md-6">
                        <input class=" form-control" name="ContactForm[name]" type="text" value="<?= $model->name ?>">
                    </div>
                </div>
               
                <div class="row form-group">
                    <div class="col col-md-4">
                        <label class="">Адрес email</label>
                    </div>
                    <div class="col col-md-6">
                        <input class=" form-control" name="ContactForm[email]" type="text" value="<?= $model->email ?>">
                    </div>
                </div>

				<div class="row form-group">
                    <div class="col col-md-4">
                        <label class="">Тема обращения</label>
                    </div>
                    <div class="col col-md-6">
                        <input class=" form-control" name="ContactForm[subject]" type="text" value="<?= $model->subject ?>">
                    </div>
                </div>
				<div class="row form-group">
                    <div class="col col-md-4">
                        <label class="">Сообщение</label>
                    </div>
                    <div class="col col-md-6">
						<textarea rows="10" class="form-control" name="ContactForm[body]"><?= $model->body ?></textarea>
                    </div>
                </div>

				
                
                <div class="row form-group">
                    <div class="col col-md-4"></div>
                    <div class="col col-md-6">
                        <div class="dk-recaptcha">
                            <div class="g-recaptcha" data-sitekey="6LdCDsAUAAAAAF378Xb2F38wp59s-4EyyEULoxJK"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
					<div class="col col-md-4"></div>
					<div class="col col-md-6">
                        <button class="btn btn-primary">Отправить</button>
                    </div>
                </div>
                <? endif; ?>
            </form>
        </div>
    </div>
</div>
<div class="clear"></div>

<?php endif; ?>