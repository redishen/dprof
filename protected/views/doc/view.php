<?php
//$this->breadcrumbs = array(
//    $model->title,
//);

header("Access-Control-Allow-Origin: *");
$this->breadcrumbs = $model->getBreadcrumbs();

$this->pageTitle = $model->title . ' - ' . Yii::app()->name;
?>

<?php
$this->renderPartial('_view', array(
    'data' => $model,
));
?>

