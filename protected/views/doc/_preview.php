<div class="post">
    <div class="title">
        <?php echo CHtml::link(CHtml::encode($data->title), $data->url); ?>
        <?php echo CHtml::link($data->viewCount, '', array('id' => 'counter', 'title' => "Просмотры - {$data->viewCount}")) ?>
    </div>
    <div class="content">
        <?php
        $this->beginWidget('CMarkdown', array('purifyOutput' => false));

        echo $data->preview_text;

        echo ' ' . CHtml::link('<i>подробнее...</i>', $data->url);
        $this->endWidget();
        ?>
    </div>
</div>