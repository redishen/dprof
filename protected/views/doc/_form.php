<?php ?>
<div class="form col-xs-10 col-xs-offset-1">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id' => 'post-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldControlGroup($model, 'title', array('maxlength' => 128)); ?>

    <?php
    echo $form->textAreaControlGroup($model, 'preview_text', array(
        'rows' => 5,
    ));
    ?>

    <fieldset>
        <legend>Связи</legend>
        <?php echo $form->textFieldControlGroup($model, 'related_posts', array('maxlength' => 128, 'help' => 'ID постов через запятую без пробелов')); ?>
    </fieldset>

    <?php
    echo BsHtml::dropDownListControlGroup('Post[categories]', $model->categories[0]->id,
        Category::model()->getChildrenList(Category::DOC),
        array('label' => 'Раздел', 'empty' => '- Не выбрано -'))
    ?>


    <div class="form-group">
        <?php echo $form->label($model, 'tags'); ?>
        <?php
        $this->widget('CAutoComplete', array(
            'model' => $model,
            'attribute' => 'tags',
            'url' => array('suggestTags'),
            'multiple' => true,
            'htmlOptions' => array('size' => 50, 'class' => 'form-control'),
        ));
        ?>
        <p class="hint">Введите, разделяя запятыми.</p>
        <?php echo $form->error($model, 'tags'); ?>
    </div>


    <fieldset>
        <legend>Документы</legend>
        <?php if (!empty($model->postDocuments)): ?>
            <div class="form-group">
                <ul class="list-unstyled list_type_doc">
                    <?php foreach ($model->postDocuments as $doc): ?>
                        <li class="row list__item" data-id="<?= $doc->id ?>" data-name="<?= $doc->file_name ?>">
                            <div class="col-xs-5 list__label ">
                                <?= $doc->file_name ?>
                            </div>

                            <div class="col-xs-2 pull-right list__actions">
                                <?=
                                BsHtml::button('Переименовать', array(
                                        'class' => 'list__button_action_rename',
                                        'size' => 'xs',
                                        'icon' => BsHtml::GLYPHICON_PENCIL,
                                    )
                                );
                                ?>
                                <?=
                                BsHtml::ajaxButton('Удалить', array(
                                    'ajaxDeleteDoc',
                                    'id' => $doc->id),
                                    array(
                                        'success' => new CJavaScriptExpression('$.proxy(function(){$(this).parents(".list__item").remove()}, this)'),
                                        'error' => new CJavaScriptExpression('$.proxy(function(){$(this).attr("disabled", "disabled").text("ОШИБКА УДАЛЕНИЯ")}, this)')), array(
                                        'confirm' => 'Вы действительно хотите удалить элемент?',
                                        'size' => 'xs',
                                        'icon' => BsHtml::GLYPHICON_TRASH,
                                        'class' => 'list__button_action_delete',
                                    )
                                );
                                ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <div class="form-group">
            <?php echo $form->label($model, 'documents'); ?>
            <?php
            $this->widget('xupload.FileUpload', array(
                    'url' => $this->createUrl('upload'),
                    'showForm' => false,
                    'model' => $files,
                    'attribute' => 'file',
                    'multiple' => true,
                    'autoUpload' => true,
                    'formView' => 'application.views.doc._uploadform',
                    'htmlOptions' => array('id' => 'post-form'),
                    'options' => array(
                        'maxFileSize' => 300000000,
                        'disableImageResize' => 'js:/Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent)',
                        'previewMaxWidth' => 150,
                        'previewMaxHeight' => 150,
                        'previewCrop' => true
                    )
                )
            );
            ?>
        </div>
    </fieldset>


    <fieldset>
        <legend>Параметры размещения</legend>

        <?php
        echo BsHtml::dropDownListControlGroup('postType', 0, array('Сегодня', 'Вчера', 'Завтра', 'Дата'), array('label' => 'Разместить', 'help' => 'Используется для сортировки'))
        ?>

        <div class="form-group" id='updTime' style='display: none'>
            <?= $form->label($model, 'update_time') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'update_time',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'update_time') ?>
        </div>

        <?php
        echo $form->dropDownListControlGroup($model, 'status', Lookup::items('PostStatus'));
        ?>
    </fieldset>

    <?php echo BsHtml::inputSubmitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('id' => 'submit')); ?>


    <?php $this->endWidget(); ?>
</div>

<script>
    $(function () {
        $(document).on('click', '.list__button_action_rename', function () {
            var that = $(this),
                item = that.parents('.list__item'),
                label = item.children('.list__label'),
                id = item.data('id'),
                name = item.data('name');

            var wrap = $('<div class="col-xs-5 list__actions_type_rename"></div>'),
                input = $('<input type="text" class="form-control input-sm list__input" value="' + name + '"/>'),
                ok = $('<button class="btn btn-default btn-xs list__button_action_ok" ><span class="glyphicon glyphicon-ok"></span></button>'),
                no = $('<button class="btn btn-default btn-xs list__button_action_no" ><span class="glyphicon glyphicon-remove"></span></button>');

            that.attr('disabled', true);

            wrap.append(input);
            wrap.append(ok);
            wrap.append(no);
            label.after(wrap);


            no.click(function () {
                wrap.remove();
                that.removeAttr('disabled');
            });

            ok.click(function (e) {
                e.preventDefault();
                $.ajax({
                    url: 'ajaxRenameDoc',
                    data: {
                        id: id,
                        name: input.val()
                    }
                }).done(function (data) {
                    label.html(input.val());
                    item.data('name', input.val());
                    wrap.remove();
                    that.removeAttr('disabled');
                }).fail(function (data) {
                    wrap.remove();
                    that.text(data.responseText)
                });
            })


        })
    });
</script>