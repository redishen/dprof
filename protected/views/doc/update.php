<?php
$this->pageTitle = array('Изменить', CHtml::encode($model->title));
$this->breadcrumbs = array(
    'Управление' => array('admin'),
    $model->title => $model->url,
    'Изменить',
);
?>

<?php
echo $this->renderPartial('_form', array(
    'model' => $model,
    'files' => $files,
));
?>
