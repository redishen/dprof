<?php if (isset($catName)): ?>
    <h1 class="heading heading_type_cat">Документы</h1>
    <?php $this->pageTitle = $catName . ' - ' . Yii::app()->name; ?>
<?php elseif (!empty($_GET['tag'])): ?>
    <h1 class="heading heading_type_tag">Записи, отмеченные тэгом <i>"<?php echo CHtml::encode($_GET['tag']); ?>"</i>
    </h1>
    <?php $this->pageTitle = 'Тэг: ' . $_GET['tag'] . ' - ' . Yii::app()->name; ?>
<?php endif; ?>

<?
$this->widget('zii.widgets.CMenu', array(
    'items' => Category::model()->getDocFilterMenu(),
    'htmlOptions' => array('class' => 'filter'),
    'itemCssClass' => 'filter__item',
    'activeCssClass' => 'filter__item_active',
    'firstItemCssClass' => 'filter__item_all',
    'lastItemCssClass' => 'filter__item_last'
));
?>
<div class="clear"></div>
<?php
$this->widget('zii.widgets.CListView', array(
    'dataProvider' => $dataProvider,
    'itemView' => '_preview',
    'template' => "{items}\n{pager}",
    'emptyText' => 'Нет документов в этой категории'
));
?>
<br>
<br>
