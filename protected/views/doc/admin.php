<?php
$this->pageTitle = 'Управление записями';
$this->breadcrumbs = array(
    'Управление записями',
);
?>
<div class="row">
    <div class="col-xs-12">
        <?php
        $this->widget('ext.groupgridview.BootGroupGridView', array(
            'id' => 'post-grid',
            'dataProvider' => $model->searchDocs(),
            'columns' => array(
                array(
                    'name' => 'title',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->title), $data->url)'
                ),
                array(
                    'name' => 'status',
                    'value' => 'Lookup::item("PostStatus",$data->status)',
                    'filter' => Lookup::items('PostStatus'),
                ),
                array(
                    'name' => 'create_time',
                    'type' => 'datetime',
                    'filter' => false,
                ),
                array(
                    'name' => 'update_time',
                    'type' => 'datetime',
                    'filter' => false,
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                ),
            ),
        ));
        ?>
    </div>
</div>

