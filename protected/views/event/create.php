<?php
/* @var $this EventController */
/* @var $model Event */

$this->breadcrumbs=array(
	'Собятия'=>array('index'),
	'Создать',
);

$this->menu=array(
	array('label'=>'List Event', 'url'=>array('index')),
	array('label'=>'Manage Event', 'url'=>array('admin')),
);
?>

<h1>Создать событие</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>