<?
/**
 * @var Banner $model
 * @var BsActiveForm $form
 *
 * */
?>
<div class="form col-xs-10 col-xs-offset-1">
    <?php
    $form = $this->beginWidget('bootstrap.widgets.BsActiveForm', array(
        'id' => 'banner-form',
        'enableAjaxValidation' => true,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->textFieldControlGroup($model, 'header', array('maxlength' => 255)); ?>

    <div class="form-group">
        <?= $form->label($model, 'description') ?>
        <div>
            <?php
            $this->widget('ext.tinymce.TinyMce', array(
                'model' => $model,
                'attribute' => 'description',
                'htmlOptions' => array(
                    'rows' => 15,
                ),
            ));
            ?>
            <?= $form->error($model, 'description') ?>
        </div>
    </div>

    <fieldset>
        <legend>Событие</legend>
        <div class="form-group">
            <?= $form->label($model, 'date_from') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'date_from',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'date_from') ?>
        </div>

        <div class="form-group">
            <?= $form->label($model, 'date_to') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'date_to',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'date_to') ?>
        </div>

        <?= $form->textFieldControlGroup($model, 'date_note', array('maxlength' => 255)); ?>
    </fieldset>

    <fieldset>
        <legend>Публикация</legend>
        <?= $form->checkBoxControlGroup($model, 'active'); ?>


        <div class="form-group">
            <?= $form->label($model, 'active_from') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'active_from',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'active_from') ?>
        </div>

        <div class="form-group">
            <?= $form->label($model, 'active_to') ?>
            <?php
            $this->widget('CMaskedTextField', array(
                'model' => $model,
                'attribute' => 'active_to',
                'mask' => '9999-99-99 99:99',
                'htmlOptions' => array('class' => 'form-control'),
            ));
            ?>
            <?= $form->error($model, 'active_to') ?>
        </div>
    </fieldset>


    <?php echo BsHtml::inputSubmitButton($model->isNewRecord ? 'Создать' : 'Сохранить', array('id' => 'submit')); ?>


    <?php $this->endWidget(); ?>
</div>
