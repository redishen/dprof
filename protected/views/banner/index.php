<?php
$this->pageTitle = 'Управление записями';
$this->breadcrumbs = array(
    'Управление записями',
);
?>
<div class="row">
    <div class="col-xs-12">
        <?php
        $this->widget('bootstrap.widgets.BsGridView', array(
            'id' => 'post-grid',
            'dataProvider' => $model->search(),
            'columns' => array(
                array(
                    'name' => 'header',
                    'type' => 'raw',
                    'value' => 'CHtml::link(CHtml::encode($data->header), $data->url)'
                ),
                array(
                    'name' => 'active',
                    'type' => 'boolean',
                ),
                array(
                    'name' => 'active_from',
                    'type' => 'datetime',
                ),
                array(
                    'name' => 'active_to',
                    'type' => 'datetime',
                ),
                array(
                    'class' => 'bootstrap.widgets.BsButtonColumn',
                ),
            ),
        ));
        ?>
    </div>
</div>

