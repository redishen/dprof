<?php
$this->pageTitle = array('Изменить', CHtml::encode($model->id));
$this->breadcrumbs = array(
    'Управление' => array('admin'),
    $model->header => $model->url,
    'Изменить',
);
?>

<?php
echo $this->renderPartial('_form', array(
    'model' => $model,
));
?>
