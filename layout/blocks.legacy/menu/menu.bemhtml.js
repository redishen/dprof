block('menu')(
    attrs()({id: 'content-begin'}),
    content()(function () {
        return [
            {
                html: '\n' +
                '                    <div id="mainmenu" class="mainFixed">\n' +
                '                        <form action="/post/search" method="post"><ul id="yw1">\n' +
                '<li class="first-menu"><a href="/site/index">Главная</a></li>\n' +
                '<li><a href="/post/index">Новости</a></li>\n' +
                '<li><a href="/site/contact">Задать вопрос</a></li>\n' +
                '<li><a href="/site/page?view=about">О нас</a></li>\n' +
                '<li class="active"><a href="/site/page?view=contact">Контакты</a></li>\n' +
                '<li style="float: right; margin-top: -3px;"><span><input name="search-str" id="search-input" value="Поиск по сайту"></span></li>\n' +
                '</ul></form>                    </div><!-- mainmenu -->\n' +
                '            ',
            }
        ];
    })
)