block('header')(
    attrs()({id: 'header'}),
    content()(function () {
        return [
            {
                html: '' +
                '\n' +
                '                    <div id="logo">\n' +
                '                                                <div id="logo-pic">\n' +
                '                            <img src="http://dprof.lo/css/img/logo-title-big.png"></div>\n' +
                '                        <div id="site-title">\n' +
                '                            ДОРОЖНАЯ ТЕРРИТОРИАЛЬНАЯ ОРГАНИЗАЦИЯ РОСПРОФЖЕЛ<br>\n' +
                '                            НА МОСКОВСКОЙ ЖЕЛЕЗНОЙ ДОРОГЕ\n' +
                '                        </div>\n' +
                '                        <div id="site-title-transparent">\n' +
                '                        </div>\n' +
                '\n' +
                '                        <div id="site-contacts">\n' +
                '                            <strong>"Горячая линия"<br>\n' +
                '                                +7 (499) 266-23-81<br>\n' +
                '                                +7 (499) 266-38-44 <br>\n' +
                '                                пн-пт с 9-00 до 18-00<br></strong>\n' +
                '                        </div>\n' +
                '                        <div id="site-contacts-transparent">\n' +
                '                        </div>\n' +
                '\n' +
                '\n' +
                '                    </div>\n' +
                '                    \n' +
                '<div id="recentPhotosLogo">\n' +
                '\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-21.jpg" alt="">            <div>\n' +
                '                <h4>ВСТУПАЙ В ПРОФСОЮЗ!</h4>\n' +
                '                <a href="/post/index?cat=2">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-30.jpg" alt="">            <div>\n' +
                '                <h4>ДЕТИ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=15">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-16.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-13.jpg" alt="">            <div>\n' +
                '                <h4>МОЛОДЕЖЬ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=14">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-18.jpg" alt="">            <div>\n' +
                '                <h4>ЗАБОТА О ВЕТЕРАНАХ</h4>\n' +
                '                <a href="/post/index?cat=37">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-15.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ</h4>\n' +
                '                <a href="/post/index?cat=38">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-26.jpg" alt="">            <div>\n' +
                '                <h4>ДЕТИ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=15">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-10.jpg" alt="">            <div>\n' +
                '                <h4>ЗАБОТА О ВЕТЕРАНАХ</h4>\n' +
                '                <a href="/post/index?cat=37">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-111.jpg" alt="">            <div>\n' +
                '                <h4>ПРАВОВАЯ ЗАЩИТА</h4>\n' +
                '                <a href="/post/index?cat=16">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-29.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ</h4>\n' +
                '                <a href="/post/index?cat=38">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-20.jpg" alt="">            <div>\n' +
                '                <h4>МОЛОДЕЖЬ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=14">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-21.jpg" alt="">            <div>\n' +
                '                <h4>ВСТУПАЙ В ПРОФСОЮЗ!</h4>\n' +
                '                <a href="/post/index?cat=2">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-27.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-14.jpg" alt="">            <div>\n' +
                '                <h4>ДЕТИ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=15">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-8.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-28.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ – ЗА ЗДОРОВЫЙ ОБРАЗ ЖИЗНИ</h4>\n' +
                '                <a href="/post/index?cat=38">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-1.jpg" alt="">            <div>\n' +
                '                <h4>ВСТУПАЙ В ПРОФСОЮЗ!</h4>\n' +
                '                <a href="/post/index?cat=2">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-9.jpg" alt="">            <div>\n' +
                '                <h4>ЗАБОТА О ВЕТЕРАНАХ</h4>\n' +
                '                <a href="/post/index?cat=37">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-19.jpg" alt="">            <div>\n' +
                '                <h4>ПРОФСОЮЗ - ЗА БЕЗОПАСНЫЙ ТРУД</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-25.jpg" alt="">            <div>\n' +
                '                <h4>МОЛОДЕЖЬ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=14">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-24.jpg" alt="">            <div>\n' +
                '                <h4>МОЛОДЕЖЬ И ПРОФСОЮЗ</h4>\n' +
                '                <a href="/post/index?cat=14">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '            <div class="slide">\n' +
                '            <img src="http://dprof.lo/files/site/carousel/main-pic-xx.png" alt="">            <div>\n' +
                '                <h4>ТЕХНИЧЕСКАЯ ИНСПЕКЦИЯ ТРУДА</h4>\n' +
                '                <a href="/post/index?cat=17">подробнее</a>            </div>\n' +
                '        </div>\n' +
                '    \n' +
                '</div>\n' +
                '<div class="clearfix"></div>\n' +
                '\n' +
                '                    <div id="logoPic">\n' +
                '                    </div>\n' +
                '\n' +
                '               '
            },

        ];
    })
)