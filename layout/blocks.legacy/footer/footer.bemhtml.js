block('footer')(
    attrs()({id: 'footer'}),
    content()(function () {
        return [
            {
                html: '\n' +
                '                    Copyright © 2018 "Дорожный комитет РОСПРОФЖЕЛ Московской железной\n' +
                '                    дороги"<br>\n' +
                '                    При использовании материалов <a href="http://www.dprof.ru">активная ссылка</a> на сайт обязательна.\n' +
                '                                            <a href="/site/login">Войти</a>\n' +
                '                                        <br>\n' +
                '                    Создано на <a href="http://www.yiiframework.com/" rel="external">Yii Framework</a>.               ',
            }
        ];
    })
)