block('legacy-container')(
    cls()('bgShadow'),
    content()(function () {
        return [
            {
                html: '<div class="shadowTop"></div>'
            },
            {
                cls: 'shadowTopSub',
                content: {
                    cls: 'shadowFooter',
                    content: [
                        {
                            block: 'container',
                            attrs: {id: 'page'},

                            content: [
                                {
                                    block: 'header'
                                },
                                {
                                    block: 'menu'
                                },

                                {
                                    block: 'container',
                                    content:
                                        {
                                            tag: 'div',
                                            attrs: {
                                                id: 'content'
                                            },
                                            content: this.ctx.content
                                        },
                                },
                                {
                                    block: 'footer'
                                }
                            ]
                        },
                    ]
                }
            },

        ];
    })
)