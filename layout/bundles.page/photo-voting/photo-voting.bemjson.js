var params = {
    title: 'photo-voting',
};

var nominations = [
    'Все',
    'Как молоды мы были…',
    'Молодежь за достойные условия труда',
    'Молодежь за здоровый образ жизни',
    'Профсоюз – это мы!',
    'Сделаем мир добрее',
    //fake
    'Как молоды мы были…',
    'Молодежь за достойные условия труда',
    'Молодежь за здоровый образ жизни',
    'Профсоюз – это мы!',
    'Сделаем мир добрее'
];

var regions = [
    'Все',
    'Московско-Курское РОП',
    'Рязанское РОП',
    'Московско-Смоленское РОП',
    'Тульское РОП',
    'Орловско-Курское РОП',
    //fake
    'Московско-Курское РОП',
    'Рязанское РОП',
    'Московско-Смоленское РОП',
    'Тульское РОП',
    'Орловско-Курское РОП'
];

var items = [
    {
        nomination: nominations[1],
        region: regions[1],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[2],
        region: regions[2],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[3],
        region: regions[3],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[4],
        region: regions[4],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[5],
        region: regions[5],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[6],
        region: regions[6],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[7],
        region: regions[7],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[8],
        region: regions[8],
        name: 'Молодежь за достойные условия труда'
    },
    {
        nomination: nominations[9],
        region: regions[9],
        name: 'Молодежь за достойные условия труда'
    },
];


module.exports = {
    block: 'page',
    title: params.title,
    favicon: '/favicon.ico',
    head: [
        {elem: 'meta', attrs: {name: 'format-detection', content: 'telephone=no'}},
        {
            elem: 'meta',
            attrs: {
                name: 'viewport',
                content: 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'
            }
        },
        {elem: 'css', url: '/blocks.legacy/build/screen.css'},
        {elem: 'css', url: params.title + '.min.css'},
        {elem: 'css', url: '/blocks.legacy/build/main.min.css'},


    ],
    scripts: [
        // {elem: 'js', url: '/blocks.legacy/build/vendor.js'},
        // {elem: 'js', url: '/blocks.legacy/build/script.js'},
        {elem: 'js', url: '//code.jquery.com/jquery-latest.js'},
        {elem: 'js', url: params.title + '.min.js'}
    ],
    content: [
        {
            block: 'legacy-container',
            mods: {type: 'bare'},
            content: [
                // {
                //     html: '<div class="breadcrumbs">\n' +
                //     '<a href="/">Главная</a> » <span>Фотоконкурс к 15-летию ОАО «РЖД»</span></div>'
                // },
                {
                    block: 'voting',
                    js: {
                        'voteUrl': 'http://dprof.lo/layout/ajax.test/vote.php',
                    },
                    mods: {type: 'photo'},
                    // mix: ['container-fluid'],
                    content: [
                        {
                            html: '<h1 class="page-heading">Фотоконкурс к 15-летию ОАО «РЖД»</h1>'
                        },
                        {
                            block: 'container-fluid',
                            content: [
                                {
                                    block: 'voting',
                                    elem: 'form',
                                    tag: 'form',
                                    cls: 'container-fluid',
                                    content: [
                                        {
                                            block: 'row',
                                            content: [
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-4',
                                                    content: [
                                                        {
                                                            block: 'voting',
                                                            elem: 'label',
                                                            tag: 'label',
                                                            content: 'Номинация'
                                                        },
                                                        {
                                                            block: 'voting',
                                                            elem: 'select',
                                                            tag: 'select',
                                                            attrs: {name: 'nomination'},
                                                            content: [
                                                                nominations.map(function (opt) {
                                                                    return {
                                                                        tag: 'option',
                                                                        content: opt
                                                                    }
                                                                })
                                                            ]
                                                        }
                                                    ]
                                                },
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-4',
                                                    content: [
                                                        {
                                                            block: 'voting',
                                                            elem: 'label',
                                                            tag: 'label',
                                                            content: 'Регион'
                                                        },
                                                        {
                                                            block: 'voting',
                                                            elem: 'select',
                                                            tag: 'select',
                                                            attrs: {name: 'region'},
                                                            content: [
                                                                regions.map(function (opt) {
                                                                    return {
                                                                        tag: 'option',
                                                                        content: opt
                                                                    }
                                                                })
                                                            ]
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-4',
                                                    content: [
                                                        {
                                                            block: 'voting',
                                                            elem: 'label',
                                                            tag: 'label',
                                                            content: ''
                                                        },
                                                        {
                                                            tag: 'button',
                                                            cls: 'btn btn-primary',
                                                            content: 'Показать'
                                                        }
                                                    ]
                                                },
                                            ]
                                        }
                                    ]
                                },
                                {
                                    block: 'voting',
                                    elem: 'items',
                                    content: [
                                        [1, 2, 3].map(function (o, e) {
                                            return {
                                                block: 'voting',
                                                elem: 'row',
                                                cls: 'row',
                                                content: [1, 2, 3].map(function (item, i) {
                                                    var item = items[i];
                                                    return {
                                                        block: 'voting',
                                                        elem: 'item',
                                                        attrs: {
                                                            'data-image': 'http://dprof.lo/files/voting/' + (i + 1) + '.jpg',
                                                            'data-id': i,
                                                            'data-region':  item.region,
                                                            'data-nomination':  item.nomination,
                                                            'data-vote-count':  i*100,
                                                            'data-has-voted': false,
                                                        },
                                                        cls: ['col-md-4'],
                                                        content:
                                                            [
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'preview',
                                                                    content: [
                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'thumb',
                                                                            tag: 'img',
                                                                            attrs: {
                                                                                src: 'http://dprof.lo/files/voting/' + (i + 1) + '.jpg'
                                                                            }
                                                                        },
                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'overlay',
                                                                        }
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'heading',
                                                                    content: item.name
                                                                },
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'body',
                                                                    content: [

                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'detail',
                                                                            content: [
                                                                                'Регион: ' + item.region,
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'detail',
                                                                            content: [
                                                                                'Номинация: ' + item.nomination,
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'detail',
                                                                            content: [
                                                                                'Голосов: 123',
                                                                            ]
                                                                        },
                                                                        {
                                                                            block: 'voting',
                                                                            elem: 'buttons',
                                                                            content: {
                                                                                block: 'dk-button',
                                                                                tag: 'button',
                                                                                cls: 'btn btn-warning',
                                                                                options: {
                                                                                    id: i,
                                                                                    toggle: 'modal',
                                                                                    // target: '#loginModal',
                                                                                    target: '#voteModal'
                                                                                },
                                                                                mix: {
                                                                                    block: 'voting',
                                                                                    elem: 'control',
                                                                                    elemMods: {action: 'show-modal'}
                                                                                },
                                                                                content: 'Голосовать'
                                                                            }
                                                                        },

                                                                    ]
                                                                },
                                                            ]
                                                    }
                                                })
                                            }
                                        }),
                                        {
                                            html: '<div class="voting__pager">Перейти к странице: <ul id="yw1" class="yiiPager"><li class="voting__pager-item voting__pager-item_hidden"><a href="/voteGallery/index">&lt;&lt; Первая</a></li>\n' +
                                            '<li class="voting__pager-item voting__pager-item_hidden"><a href="/voteGallery/index">&lt; Предыдущая</a></li>\n' +
                                            '<li class="voting__pager-item voting__pager-item_active"><a href="/voteGallery/index">1</a></li>\n' +
                                            '<li class="voting__pager-item"><a href="/voteGallery/index?VotingGallery_page=2">2</a></li>\n' +
                                            '<li class="voting__pager-item"><a href="/voteGallery/index?VotingGallery_page=2">Следующая &gt;</a></li>\n' +
                                            '<li class="voting__pager-item"><a href="/voteGallery/index?VotingGallery_page=2">Последняя &gt;&gt;</a></li></ul></div>'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'dk-modal',
                            mods: {
                                // openOnInit: true,
                                overlay: true,
                                type: 'info',
                                alert: true,
                            },
                            js: true,
                            mix: [{
                                block: 'voting',
                                elem: 'modal',
                                elemMods: {'action': 'login'}
                            }],
                            attrs: {
                                id: 'loginModal',
                            },
                            resultsText: 'Для голосования необходимо авторизоваться на сайте',
                            resultsContent: [
                                {
                                    block: 'voting',
                                    elem: 'auth',
                                    content: [
                                        {
                                            html: '<div id="hoauthWidgetyw0" class="hoauthWidget"><p>\n' +
                                            '    <a href="/user/login/oauth/provider/Facebook" class="zocial  facebook">Войти через Facebook</a>\n' +
                                            '</p>\n' +
                                            '<p>\n' +
                                            '    <a href="/user/login/oauth/provider/Vkontakte" class="zocial  vkontakte">Войти через Vkontakte</a>\n' +
                                            '</p>\n' +
                                            '</div>'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            block: 'dk-modal',
                            mods: {
                                // openOnInit: true,
                                overlay: true,
                                type: 'info',
                                alert: true,
                            },
                            js: true,
                            mix: [{
                                block: 'voting',
                                elem: 'modal',
                                elemMods: {'action': 'vote'}
                            }],
                            attrs: {
                                id: 'voteModal',
                            },
                            resultsText: 'Молодежь за достойные условия труда',
                            resultsContent: [
                                {
                                    block: 'row',
                                    content: [
                                        {
                                            block: 'col',
                                            cls: 'col-md-8',
                                            content: [{
                                                block: 'voting',
                                                elem: 'modal-image',
                                                content: [
                                                    {
                                                        block: 'voting',
                                                        elem: 'image',
                                                        tag: 'img',
                                                        cls: 'img img-fluid',
                                                        attrs: {
                                                            src: 'http://dprof.lo/files/voting/1.jpg'
                                                        }
                                                    },
                                                ]
                                            },]
                                        },
                                        {
                                            block: 'col',
                                            cls: 'col-md-4',
                                            content: [
                                                {
                                                    block: 'voting',
                                                    elem: 'modal-details',
                                                    content: [
                                                        {
                                                            block: 'voting',
                                                            elem: 'detail',
                                                            content: [
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-label',
                                                                    content: [
                                                                        'Регион:',
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-value',
                                                                    attrs: {'data-name': 'region'},
                                                                    content: [
                                                                        'Московско-Курское РОП',
                                                                    ]
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            block: 'voting',
                                                            elem: 'detail',
                                                            content: [
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-label',
                                                                    content: [
                                                                        'Номинация:',
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-value',
                                                                    attrs: {'data-name': 'nomination'},
                                                                    content: [
                                                                        'Молодежь за достойные условия труда',
                                                                    ]
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            block: 'voting',
                                                            elem: 'detail',
                                                            content: [
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-label',
                                                                    content: [
                                                                        'Голосов:',
                                                                    ]
                                                                },
                                                                {
                                                                    block: 'voting',
                                                                    elem: 'detail-value',
                                                                    attrs: {'data-name': 'vote-count'},
                                                                    content: [
                                                                        '0',
                                                                    ]
                                                                },
                                                            ]
                                                        },
                                                    ]
                                                },
                                                {
                                                    block: 'dk-button',
                                                    tag: 'button',
                                                    mix: {
                                                        block: 'voting',
                                                        elem: 'control',
                                                        elemMods: {action: 'vote'}
                                                    },
                                                    cls: 'btn btn-warning',
                                                    content: 'Я голосую за эту работу'
                                                },
                                                {
                                                    block: 'dk-button',
                                                    options: {
                                                        dismiss: 'modal',
                                                    },
                                                    cls: 'btn btn-secondary',
                                                    content: 'Закрыть'
                                                },
                                            ]
                                        }
                                    ]
                                },


                            ],
                            resultsActions: ' '
                        },
                    ]
                },
            ]
        },
    ]
};
