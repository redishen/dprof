var params = {
    title: 'polk',
};

module.exports = {
    block: 'page',
    title: params.title,
    favicon: '/favicon.ico',
    head: [
        {elem: 'meta', attrs: {name: 'format-detection', content: 'telephone=no'}},
        {
            elem: 'meta',
            attrs: {
                name: 'viewport',
                content: 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'
            }
        },
        {elem: 'css', url: '/blocks.legacy/build/screen.css'},
        {elem: 'css', url: params.title + '.min.css'},
        {elem: 'css', url: '/blocks.legacy/build/main.min.css'},


    ],
    scripts: [
        // {elem: 'js', url: '//code.jquery.com/jquery-latest.js'},
        // {elem: 'js', url: '//dprof.lo/assets/5f4a672/jquery.js'},
        {elem: 'js', url: params.title + '.min.js'}
    ],
    content: [
        {
            block: 'legacy-container',
            mods: {type: 'bare'},
            content: [
                {
                    block: 'polk',
                    js: true,
                    content: [
                        {
                            block: 'container-fluid',
                            content: [
                                {
                                    html: '<div class="post">\n' +
                                    '            <div class="post__title">\n' +
                                    '                <a class="post__link" href="/post/3640/%D0%91%D0%B5%D1%81%D1%81%D0%BC%D0%B5%D1%80%D1%82%D0%BD%D1%8B%D0%B9+%D0%BF%D0%BE%D0%BB%D0%BA.+%D0%9D%D0%B0%D1%88%D0%B0+%D0%BF%D0%B0%D0%BC%D1%8F%D1%82%D1%8C+%E2%80%94+%D0%B8%D1%81%D1%82%D0%BE%D1%80%D0%B8%D1%8F+%D1%81%D1%82%D1%80%D0%B0%D0%BD%D1%8B">Бессмертный полк. Наша память — история страны</a>\n' +
                                    '                                    <a class="post__btn_action_edit ui-button ui-widget ui-state-default ui-corner-all ui-button-text-icon-primary" id="button3640" name="button3640" href="/post/update?id=3640" role="button"><span class="ui-button-icon-primary ui-icon ui-icon-pencil"></span><span class="ui-button-text">Изменить</span></a>\n' +
                                    '                            </div>\n' +
                                    '\n' +
                                    '            <div class="content">\n' +
                                    '                <strong>\n' +
                                    '                    <p>В память о железнодорожниках и членах их семей, кто отдал свои жизни ради мира на земле, кто ковал победу в тылу, кто восстанавливал страну из руин, кто пережил голод и истязания врага.<br>\n' +
                                    'Вечная Вам память и низкий поклон!</p>\n' +
                                    '                </strong>\n' +
                                    '                \n' +
                                    '            </div>\n' +
                                    '\n' +
                                    '        </div>'
                                },
                                {
                                    block: 'dk-slider',
                                    content: {
                                        html: '  <div>your content</div>\n' +
                                        '    <div>your content</div>\n' +
                                        '    <div>your content</div>' +
                                        '  <div>your content</div>\n' +
                                        '    <div>your content</div>\n' +
                                        '    <div>your content</div>' +
                                        '  <div>your content</div>\n' +
                                        '    <div>your content</div>\n' +
                                        '    <div>your content</div>'
                                    }
                                },
                                {
                                    block: 'polk',
                                    elem: 'list',
                                    content: [

                                    ]
                                },
                            ]
                        },

                    ]
                },
            ]
        },
    ]
};
