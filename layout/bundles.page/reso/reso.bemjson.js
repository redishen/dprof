var params = {
    title: 'reso',
};

var features = ['Автострахование (КАСКО, ОСАГО, «Доктор РЕСО в ДТП», «Зелёная карта»)',
    'Страхование квартир и домов',
    'Страхование от критических заболеваний (с лечением за рубежом)',
    'Страхование от несчастных случаев',
    'Инвестиционное страхование жизни'];
var stats = [
    '950 офисов продаж',
    '10 млн клиентов по всей стране',
    '45 млрд 144 млн рублей – выплаты по страховым случаям за 2018 год',
    'Работа в режиме онлайн, круглосуточно',
    'Приложение РЕСО Мобайл для удаленного обслуживания и оплаты полисов',
    '«Народная марка/Марка №1 в России» в номинации «Страховая компания»',
];
var products = [
    'Автострахование',
    'Страхование квартир и домов',
    'Страхование дач и домов',
    'Страхование критических заболеваний',
    'Страхование от несчастных случаев',
    '"Инвестиционное страхование жизни',
];
var form = [
    {
        label: 'Ваша фамилия',
        name: 'surname',
    },
    {
        label: 'Имя',
        name: 'name',
    },
    {
        label: 'Отчество',
        name: 'second_name',
    },
    {
        label: 'Контактный телефон',
        name: 'phone',
    },
    {
        label: 'Адрес email',
        name: 'email',
    },
    {
        label: 'Номер профсоюзного билета',
        name: 'card_number',
    },
    {
        label: 'Выберите продукт',
        name: 'product',
        items: products
    },
    {
        label: 'Ваш город',
        name: 'city',
    },

]

module.exports = {
    block: 'page',
    title: params.title,
    favicon: '/favicon.ico',
    head: [
        {elem: 'meta', attrs: {name: 'format-detection', content: 'telephone=no'}},
        {
            elem: 'meta',
            attrs: {
                name: 'viewport',
                content: 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no'
            }
        },
        {elem: 'css', url: '/blocks.legacy/build/screen.css'},
        {elem: 'css', url: params.title + '.min.css'},
        {elem: 'css', url: '/blocks.legacy/build/main.min.css'},


    ],
    scripts: [
        // {elem: 'js', url: '//code.jquery.com/jquery-latest.js'},
        {elem: 'js', url: '//code.jquery.com/jquery-latest.js'},
        {elem: 'js', url: params.title + '.min.js'}
    ],
    content: [
        {
            block: 'legacy-container',
            mods: {type: 'bare'},
            content: [
                {
                    block: 'reso',
                    js: true,
                    content: [
                        {
                            block: 'container-fluid',
                            content: [
                                {
                                    block: 'reso',
                                    elem: 'cover',
                                    content: {
                                        block: 'reso',
                                        elem: 'cover-inner',
                                        content: [
                                            {
                                                block: 'reso',
                                                tag: 'h1',
                                                elem: 'cover-text',
                                                content: {html: 'Льготное страхование<br> для членов РОСПРОФЖЕЛ'}
                                            },
                                            {
                                                block: 'dk-button',
                                                tag: 'a',
                                                attrs: {
                                                    href: '#form'
                                                },
                                                mix: {
                                                    block: 'reso',
                                                    elem: 'btn',
                                                },
                                                content: 'Оставить заявку'
                                            },
                                        ]
                                    }

                                },
                                {
                                    block: 'reso',
                                    elem: 'block',
                                    content: [
                                        {
                                            elem: 'block-header',
                                            content: 'РЕСО-Гарантия - официальный партнер РОСПРОФЖЕЛ'
                                        },
                                        {
                                            elem: 'block-text',
                                            content: 'Члены РОСПРОФЖЕЛ и их близкие могут рассчитывать на полноценную страховую защиту РЕСО-Гарантия на оптимальных условиях.'
                                        },
                                    ]
                                },
                                {
                                    block: 'reso',
                                    elem: 'highlight',
                                    elemMods: {
                                        color: 'yellow'
                                    },
                                    content: [
                                        {
                                            elem: 'list-heading',
                                            content: 'Скидки распространяются на:'
                                        },
                                        {
                                            elem: 'list',
                                            tag: 'ul',
                                            content: features.map(function (i) {
                                                    return {
                                                        tag: 'li',
                                                        content: i
                                                    }
                                                }
                                            ),
                                        }
                                    ]
                                },
                                {
                                    block: 'reso',
                                    elem: 'block',
                                    content: [
                                        {
                                            elem: 'block-header',
                                            content: 'Специально для членов РОСПРОФЖЕЛ действуют сокращенные сроки урегулирования страховых случаев.'
                                        },
                                        {
                                            elem: 'block-text',
                                            content: 'Перед покупкой полиса предъявите электронный профсоюзный билет.'
                                        },
                                    ]
                                },
                                {
                                    block: 'reso',
                                    elem: 'highlight',
                                    elemMods: {
                                        color: 'grey'
                                    },
                                    content: [
                                        {
                                            elem: 'list-heading',
                                            content: 'РЕСО-Гарантия – один из крупнейших в России универсальных страховщиков. Компания оказывает клиентам более 100 видов страховых услуг.'
                                        },
                                        {
                                            elem: 'list',
                                            tag: 'ul',
                                            content: stats.map(function (i) {
                                                    return {
                                                        tag: 'li',
                                                        content: i
                                                    }
                                                }
                                            ),
                                        }
                                    ]
                                },
                                {
                                    block: 'reso',
                                    elem: 'form',
                                    attrs: {
                                      id: 'form'
                                    },
                                    tag: 'form',
                                    cls: 'col-md-8 col-md-offset-2',
                                    content: [
                                        {
                                            elem: 'form-header',
                                            content: 'Оставьте заявку и персональный менеджер свяжется с вами'
                                        },
                                        {
                                            elem: 'form-errors',
                                            cls: 'alert alert-danger',
                                            attrs: {role: 'alert'},
                                            content: 'Ошибка отправки формы',
                                        },
                                        {
                                            elem: 'form-errors',
                                            cls: 'alert alert-success',
                                            attrs: {role: 'alert'},
                                            content: 'Ваша заявка успешно отправлена',
                                        },
                                        form.map(function (field) {
                                            return [
                                                {
                                                    block: 'row',
                                                    cls: 'form-group',
                                                    content: [
                                                        {
                                                            block: 'col',
                                                            cls: 'col-md-4',
                                                            content: [
                                                                {
                                                                    block: 'reso',
                                                                    elem: 'label',
                                                                    tag: 'label',
                                                                    content: field.label
                                                                },
                                                            ]
                                                        },
                                                        {
                                                            block: 'col',
                                                            cls: 'col-md-6',
                                                            content: [
                                                                field.items ?
                                                                    {
                                                                        block: 'reso',
                                                                        elem: 'select',
                                                                        tag: 'select',
                                                                        cls: 'form-control',
                                                                        attrs: {name: field.name},
                                                                        content: [
                                                                            field.items.map(function (opt) {
                                                                                return {
                                                                                    tag: 'option',
                                                                                    content: opt
                                                                                }
                                                                            })
                                                                        ]
                                                                    } : {
                                                                        block: 'reso',
                                                                        elem: 'input',
                                                                        tag: 'input',
                                                                        cls: 'form-control',
                                                                        attrs: {name: field.name, type: 'text'}
                                                                    },
                                                            ]
                                                        },
                                                    ]
                                                },
                                            ]
                                        }),
                                        {
                                            block: 'row',
                                            cls: 'form-group',
                                            content: [
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-4',
                                                    content: [
                                                    ]
                                                },
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-6',
                                                    content: [
                                                        {
                                                            block: "dk-recaptcha",
                                                            key: "6LdCDsAUAAAAAF378Xb2F38wp59s-4EyyEULoxJK",
                                                        },
                                                    ]
                                                },
                                            ]
                                        },
                                        {
                                            block: 'row',
                                            content: [
                                                {
                                                    block: 'col',
                                                    cls: 'col-md-12 text-center',

                                                    content: [
                                                        {
                                                            block: 'reso',
                                                            elem: 'form-confirm',
                                                            content: {html: 'Нажимая кнопку «Отправить» я даю ' +
                                                                    '<a target="_blank" href="http://www.reso.ru/regulations/personal-agreement-2.html">согласие</a> ' +
                                                                    'на обработку своих персональных данных. ' +
                                                                    '<br>С <a target="_blank" href="http://www.reso.ru/regulations/safety-of-personal.html">политикой обеспечения безопасности персональных данных </a> СПАО "РЕСО-Гарантия" ознакомлен.'}
                                                        },

                                                        {
                                                            block: 'reso',
                                                            elem: 'btn',
                                                            tag: 'button',
                                                            content: 'Отправить'
                                                        },
                                                        {
                                                            block: 'reso',
                                                            elem: 'note',
                                                            cls: 'text-muted',
                                                            content: {html: 'Узнать подробности программы страхования и приобрести страховой полис можно в Первичной профсоюзной организации или в ближайшем филиале РЕСО-Гарантия. Полный список офисов продаж <a target="_blank" href="http://www.reso.ru/About/Contacts/">на сайте СПАО "РЕСО-Гарантия"</a>'}
                                                        },
                                                    ]
                                                },
                                            ]
                                        }
                                    ]
                                },



                            ]
                        },
                    ]
                },
            ]
        },
    ]
};
