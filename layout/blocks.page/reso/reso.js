modules.define('reso', ['i-bem-dom', 'jquery', 'bootstrap', 'dk-recaptcha'],
    function (provide, bemDom, $) {
        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        console.log('loaded: reso');
                    }
                }
            },
        }));
    });

