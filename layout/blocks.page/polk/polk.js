modules.define('polk', ['i-bem-dom', 'jquery', 'slick'],
    function (provide, bemDom, $, Slick) {
        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        var that = this;
                        console.log('loaded: polk');

                        that._init();


                    }
                }
            },
            _init: function () {
                var that = this;

                $('.dk-slider').slick({
                    centerMode: true,
                    slidesToShow: 1,
                    autoplay: true,
                    autoplaySpeed: 2000,
                    pauseOnHover: false,
                });
            },
        }));
    });

