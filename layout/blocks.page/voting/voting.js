modules.define('voting', ['i-bem-dom', 'jquery', 'bootstrap-select'],
    function (provide, bemDom, $) {
        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        var that = this;
                        console.log('loaded: voting');

                        that._init();


                    }
                }
            },
            _init: function () {
                var that = this;

                $('.voting__select').selectpicker({
                    // style: 'btn-info',
                    // size: 4
                });
                that._bindEvents();
            },
            _bindEvents: function () {
                var that = this;

                var showModalBtns = that.findChildElems({
                    elem: 'control',
                    modName: 'action',
                    modVal: 'show-modal'
                });

                if (showModalBtns) {
                    that._domEvents(showModalBtns).on('click', that._onShowModalClick);
                }

                var voteBtn = that.findChildElem({
                    elem: 'control',
                    modName: 'action',
                    modVal: 'vote'
                });

                if (voteBtn) {
                    that._domEvents(voteBtn).on('click', that._onVoteClick);
                }
            },
            _onShowModalClick: function (e) {
                e.preventDefault();
                var that = this,
                    btn = e.bemTarget;

                var item = btn.findParentElem('item');
                var $item = item.domElem;

                var imgSrc = $item.data('image');
                var id = $item.data('id');


                var hasVoted = $item.data('has-voted');

                var modal = that.findChildElem({
                    elem: 'modal',
                    modName: 'action',
                    modVal: 'vote'
                });
                var $modal = modal.domElem;
                $modal.data('id', id);

                var voteBtn = modal.findChildElem({
                    elem: 'control',
                    modName: 'action',
                    modVal: 'vote'
                });

                if(voteBtn) {
                    var $voteBtn = voteBtn.domElem

                    $voteBtn.removeAttr('disabled');
                    $voteBtn.text($voteBtn.data('origText'));

                    if(!$voteBtn.data('origText')) {
                        $voteBtn.data('origText', $voteBtn.text());
                    }

                    if (hasVoted) {
                        $voteBtn.attr('disabled', 'disabled');
                        $voteBtn.text('Вы уже проголосовали');
                    }
                }


                var name = $item.data('name');
                var region = $item.data('region');
                var nomination = $item.data('nomination');
                var voteCount = $item.data('vote-count');


                $modal.find('[data-name="name"]').text(name);
                $modal.find('[data-name="region"]').text(region);
                $modal.find('[data-name="nomination"]').text(nomination);
                $modal.find('[data-name="vote-count"]').text(voteCount);


                modal.findChildElem('image').domElem.prop('src', imgSrc);

            },
            _onVoteClick: function (e) {
                var that = this;
                var btn = e.bemTarget;
                var url = that.params.voteUrl;

                var modal = btn.findParentElem('modal'),
                    $modal = modal.domElem;

                var id = $modal.data('id');


                var data = {
                    id: id,
                };

                var $btn = btn.domElem;

                $btn.attr('disabled', 'disable');

                $.ajax({
                    type: "GET",
                    url: url,
                    data: data,
                    success: function (response) {
                        $btn.text(response.message);
                        if (response.success) {
                            var $count = $modal.find('[data-name="vote-count"]');
                            $count.text(parseInt($count.text()) + 1);
                        }
                    },
                    completed: function () {
                        $btn.prop('disable', false);
                    },
                    error: function () {
                        console.error('Ошибка сервера голосовании');
                        $btn.removeAttr('disabled');
                    },
                    dataType: 'json'
                });
            },

        }));
    });

