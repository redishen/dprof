const techs = {
        // essential
        fileProvider: require('enb/techs/file-provider'),
        fileMerge: require('enb/techs/file-merge'),

        // optimization
        borschik: require('enb-borschik/techs/borschik'),

        // css
        postcss: require('enb-postcss/techs/enb-postcss'),
        postcssPlugins: [
            require('postcss-import')(),
            require('postcss-each'),
            require('postcss-for'),
            require('postcss-simple-vars')(),
            require('postcss-calc')(),
            require('postcss-nested'),
            require('rebem-css'),
            // require('postcss-url')({url: 'inline'}),
            require('postcss-url')({url: 'rebase'}),
            require('autoprefixer')(),
            require('postcss-reporter')(),
            // require('sharps').postcss({columns: 12, maxWidth: '1200px', gutter: '30px', flex: 'flex'})
        ],

        // js
        browserJs: require('enb-js/techs/browser-js'),

        // bemhtml
        bemhtml: require('enb-bemxjst/techs/bemhtml'),
        bemjsonToHtml: require('enb-bemxjst/techs/bemjson-to-html'),
        beautify: require('enb-beautify/techs/enb-beautify-html'),
        mergedBundle: require('./techs/merged-bundle')
    },
    enbBemTechs = require('enb-bem-techs'),
    levels = [
        {path: 'node_modules/bem-core/common.blocks', check: false},
        {path: 'node_modules/bem-core/desktop.blocks', check: false},
        {path: 'node_modules/bem-history/common.blocks', check: false},
        {path: 'node_modules/bem-history/touch.blocks', check: false},
        'blocks.legacy',
        'blocks.vendor',
        'blocks.basic',
        'blocks.page'
    ],
    fs = require('fs'),
    path = require('path');

module.exports = function (config) {
    const isProd = process.env.YENV === 'production';

    const mergedBundleName = 'merged',
        pathToMargedBundle = path.join('bundles.page', mergedBundleName);
    fs.existsSync(pathToMargedBundle) || fs.mkdirSync(pathToMargedBundle);
    techs.mergedBundle(config, pathToMargedBundle);

    config.nodes('bundles.*/*', function (nodeConfig) {
        const isMergedNode = path.basename(nodeConfig.getPath()) === mergedBundleName;

        isMergedNode || nodeConfig.addTechs([
            [techs.fileProvider, {target: '?.bemjson.js'}],
            [enbBemTechs.bemjsonToBemdecl]
        ]);

        nodeConfig.addTechs([
            // essential
            [enbBemTechs.levels, {levels: levels}],
            [enbBemTechs.deps],
            [enbBemTechs.files],

            // css
            [techs.postcss, {
                target: '?.css',
                oneOfSourceSuffixes: ['post.css', 'css'],
                plugins: techs.postcssPlugins
            }],

            // bemhtml
            [techs.bemhtml, {
                sourceSuffixes: ['bemhtml', 'bemhtml.js'],
                forceBaseTemplates: true,
                engineOptions: {elemJsInstances: true}
            }],

            // html
            [techs.bemjsonToHtml, {target: '?.min.html'}],

            //beautify
            [techs.beautify, {
                htmlFile: '?.min.html',
                target: '?.pre.html'
            }],

            // client bemhtml
            [enbBemTechs.depsByTechToBemdecl, {
                target: '?.bemhtml.bemdecl.js',
                sourceTech: 'js',
                destTech: 'bemhtml'
            }],
            [enbBemTechs.deps, {
                target: '?.bemhtml.deps.js',
                bemdeclFile: '?.bemhtml.bemdecl.js'
            }],
            [enbBemTechs.files, {
                depsFile: '?.bemhtml.deps.js',
                filesTarget: '?.bemhtml.files',
                dirsTarget: '?.bemhtml.dirs'
            }],
            [techs.bemhtml, {
                target: '?.browser.bemhtml.js',
                filesTarget: '?.bemhtml.files',
                sourceSuffixes: ['bemhtml', 'bemhtml.js'],
                engineOptions: {elemJsInstances: true}
            }],

            // js
            [techs.browserJs, {includeYM: true}],
            [techs.fileMerge, {
                target: '?.js',
                sources: ['?.browser.js', '?.browser.bemhtml.js']
            }],

            // borschik
            [techs.borschik, {source: '?.js', target: '?.min.js', minify: isProd}],
            [techs.borschik, {source: '?.css', target: '?.min.css', minify: isProd}],
            [techs.borschik, {source: '?.pre.html', target: '?.html'}]
        ]);

        nodeConfig.addTargets(['?.min.css', '?.min.js']);
        isMergedNode || nodeConfig.addTargets(['?.html']);
    });
};
