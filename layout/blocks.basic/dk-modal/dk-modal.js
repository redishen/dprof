modules.define('dk-modal', ['i-bem-dom'],
    function (provide, bemDom) {

        var blockName = this.name;

        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        console.log('loaded: ' + blockName);
                    }
                }
            },
        }));

    });

