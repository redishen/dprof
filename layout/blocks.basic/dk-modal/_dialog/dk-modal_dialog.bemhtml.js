block('dk-modal')(
    mod('dialog', true)(
        content()(function () {
            return {
                block: 'dk-modal',
                elem: 'inner',
                cls: 'modal-dialog',
                mix: ['modal-dialog-centered', 'modal-lg'],
                content: [
                    {
                        elem: 'content',
                        cls: 'modal-content',
                        content: [
                            {html: '<button type="button" class="close" data-dismiss="modal" aria-label="Close">+</button>'},
                            {
                                elem: 'body',
                                cls: 'modal-body',
                                content: [
                                    {
                                        elem: 'dialog',
                                        content: [
                                            {
                                                block: 'title-box',
                                                mix: [
                                                    {
                                                        block: 'dk-modal',
                                                        elem: 'title'
                                                    }
                                                ],
                                                content: [
                                                    {
                                                        block: 'dk-modal',
                                                        elem: 'dialog-text',
                                                        tag: 'h3',
                                                        cls: 'text-center',
                                                        content: this.ctx.dialogText
                                                    }
                                                ]
                                            },
                                            this.ctx.dialogContent
                                        ]
                                    },
                                    {
                                        elem: 'results',
                                        cls: 'text-center',
                                        content: [
                                            {
                                                block: 'title-box',
                                                content: [
                                                    {
                                                        block: 'dk-modal',
                                                        elem: 'results-text',
                                                        tag: 'h3',
                                                        content: this.ctx.resultsText
                                                    }
                                                ]
                                            },
                                            {
                                                block: 'dk-button',
                                                options: {
                                                    dismiss: 'modal',
                                                },
                                                mods: {
                                                    color: 'blue',
                                                },
                                                content: 'Ок'
                                            },
                                        ]
                                    }
                                ]
                            },
                            this.ctx.footer ?
                                {
                                    block: 'dk-modal',
                                    elem: 'footer',
                                    cls: 'modal-footer',
                                    content: this.ctx.footer
                                } : []
                        ]
                    }
                ]
            }
        }),
        attrs()(function () {
            return Object.assign(this.ctx.attrs, {
                tabindex: '-1',
                role: 'dialog',
                'aria-hidden': true
            });

        })
    )
);

