modules.define('dk-modal', ['i-bem-dom', 'jquery'],
    function (provide, bemDom) {

        var blockName = this.name;

        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        console.log('loaded: ' + blockName + ' mode: dialog');
                    }
                }
            },
            resetDefault: function () {
                var that = this;
                that.setMod('stage', 'dialog');
                that.setMod('loading', 'false');

                ['results-text', 'dialog-text'].forEach(function (value) {
                    var $el = that.findChildElem(value).domElem;
                    var origText = $el.data('orig-text');
                    if(origText) {
                        $el.text(origText);
                    }
                })
            },
            getDialogText: function () {
                return this.findChildElem('dialog-text').domElem.text();
            },
            setDialogText: function (text) {
                var $el = this.findChildElem('dialog-text').domElem;
                $el.data('orig-text', $el.text());
                $el.text(text);
            },
            setResultsText: function (text) {
                var $el = this.findChildElem('results-text').domElem;
                $el.data('orig-text', $el.text());
                $el.text(text);
            }
        }));

    });

