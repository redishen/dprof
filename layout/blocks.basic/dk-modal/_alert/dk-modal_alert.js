modules.define('dk-modal', ['i-bem-dom', 'jquery', 'bootstrap'],
    function (provide, bemDom) {

        var blockName = this.name;

        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        var that = this;
                        that._init();
                        console.log('loaded: ' + blockName + ' mode: alert');
                    }
                }
            },
            show: function () {
                this.domElem.modal('show');
            },
            resetDefault: function () {
                var that = this;
                that.setMod('stage', 'dialog');
                that.setMod('loading', 'false');

                ['results-text'].forEach(function (value) {
                    var $el = that.findChildElem(value).domElem;
                    var origText = $el.data('orig-text');
                    if (origText) {
                        $el.text(origText);
                    }
                });

                var el = this.findChildElem('results-content');
                if(el) {
                    el.domElem.html('');
                }
            },
            getResultsText: function () {
                return this.findChildElem('results-text').domElem.text();
            },
            setResultsText: function (text) {
                var $el = this.findChildElem('results-text').domElem;
                $el.data('orig-text', $el.text());
                $el.text(text);
            },
            setResultsContent: function (html) {
                var el = this.findChildElem('results-content');
                if(el) {
                    el.domElem.html(html);
                }
            },
            _init: function () {
                var that = this;
                if (that.hasMod('openOnInit')) {
                    that.domElem.modal('show');
                }
            }
        }));

    });

