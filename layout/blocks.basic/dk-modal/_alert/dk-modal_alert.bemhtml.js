block('dk-modal')(
    mod('alert', true)(
        content()(function () {
            return {
                block: 'dk-modal',
                elem: 'inner',
                cls: 'modal-dialog',
                mix: ['modal-dialog-centered', 'modal-lg'],
                content: [
                    {
                        elem: 'content',
                        cls: 'modal-content',
                        content: [
                            // {html: '<button type="button" class="close" data-dismiss="modal" aria-label="Close">+</button>'},
                            {
                                elem: 'body',
                                cls: 'modal-body',
                                content: [
                                    {
                                        elem: 'results',
                                        cls: 'text-center',
                                        content: [
                                            {
                                                block: 'title-box',
                                                content: [
                                                    {
                                                        block: 'dk-modal',
                                                        elem: 'results-text',
                                                        tag: 'h3',
                                                        content: this.ctx.resultsText
                                                    }
                                                ]
                                            },
                                            this.ctx.resultsContent ?
                                                {
                                                    elem: 'results-content',
                                                    content: this.ctx.resultsContent
                                                }
                                                : [],
                                            this.ctx.resultsActions ? this.ctx.resultsActions : {
                                                block: 'dk-button',
                                                options: {
                                                    dismiss: 'modal',
                                                },
                                                cls: 'btn btn-secondary',
                                                mods: {
                                                    color: 'blue',
                                                },
                                                content: 'Закрыть'
                                            },
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        }),
        attrs()(function () {
            return Object.assign(this.ctx.attrs, {
                tabindex: '-1',
                role: 'dialog',
                'aria-hidden': true
            });
        })
    )
);

