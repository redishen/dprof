block('dk-modal')(
    mod('xl', true)(
        content()(function () {
            return {
                block: 'modal-dialog',
                mix: ['modal-dialog-centered', 'modal-xl'],
                content: [
                    {
                        block: 'dk-modal',
                        elem: 'content',
                        cls: 'modal-content',
                        content: [
                            {html: '<button type="button" class="close" data-dismiss="modal" aria-label="Close">+</button>'},
                            {
                                block: 'modal-body',
                                content: this.ctx.body
                            },
                            this.ctx.footer ?
                                {
                                    block: 'modal-footer',
                                    content: this.ctx.footer
                                } : []
                        ]
                    }
                ]
            }
        })
    ),
    mod('type', 'info')(
        cls()('modal fade'),
        content()(function () {
            return {
                block: 'modal-dialog',
                mix: ['modal-dialog-centered', 'modal-xl'],
                content: [
                    {
                        block: 'modal-dialog',
                        mix: ['modal-dialog-centered', 'modal-lg'],
                        content: [
                            {
                                block: 'dk-modal',
                                elem: 'content',
                                cls: 'modal-content',
                                content: [
                                    {html: '<button type="button" class="close" data-dismiss="modal" aria-label="Close">+</button>'},
                                    {
                                        block: 'modal-body',
                                        content: this.ctx.body
                                    }
                                ]
                            },
                        ]
                    }
                ]
            }
        })
    ),
    elem('content')(
        // {
        //     appendContent: {
        //         block: 'dk-modal',
        //         elem: 'overlay',
        //         content: [
        //             {
        //                 block: 'dk-loading',
        //             }
        //         ]
        //     }
        // }
        content()(
            function () {
                return [
                    [
                        applyNext(),
                        {
                            block: 'dk-modal',
                            elem: 'overlay',
                            content: [
                                {
                                    block: 'dk-loading',
                                }
                            ]
                        }
                    ]
                ]
            }
        )
    )
);

