block('dk-button')(
    attrs()(function () {
        var res = this.ctx.attrs ? this.ctx.attrs : {'href': '#'};
        var opt = this.ctx.options;

        for (var key in opt) {
            if (opt.hasOwnProperty(key)) {
                res['data-' + key] = opt[key]
            }
        }

        return res;
    })
)

