modules.define(
    'dk-recaptcha',
    ['loader_type_js'],
    function (provide, loader) {
        setTimeout(function () {
            if (window.hasOwnProperty('getLang')) {
                loader('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=' + window.getLang(), function () {
                    provide(window);
                }, function () {
                    console.log('Error while loading recaptcha');
                    provide(window);
                });
            } else {
                loader('https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit&hl=ru', function () {
                    provide(window);
                }, function () {
                    console.log('Error while loading recaptcha');
                    provide(window);
                });
            }

        }, 0);
    }
);

modules.require(
    ['jquery', 'dk-recaptcha', 'i-bem-dom'],
    function ($) {
        window.onloadCallback = function () {
            $('.g-recaptcha').each(function (k, v) {
                grecaptcha.render(v, {
                    'sitekey': $(v).data('sitekey')
                });
            });
        };
    });

