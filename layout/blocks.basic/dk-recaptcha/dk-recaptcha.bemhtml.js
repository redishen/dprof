block('dk-recaptcha')(
    content()(function(){
        return [
            {
                block: 'g-recaptcha',
                attrs: {
                    'data-sitekey': this.ctx.key,
                },
            },
        ];
    })
)