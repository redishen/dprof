modules.define('page', ['i-bem-dom', 'jquery'],
    function (provide, bemDom, $) {
        var blockName = this.name;
        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        var that = this;

                        console.log('loaded: ' + blockName);
                    }
                }
            }
        }));

    });

