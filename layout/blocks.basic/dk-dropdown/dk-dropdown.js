modules.define('dk-dropdown', ['i-bem-dom', 'jquery', 'bootstrap-dropdown', 'js-cookie'],
    function (provide, bemDom, $, Dropdown, Cookies) {

        provide(bemDom.declBlock(this.name, {
            onSetMod: {
                'js': {
                    'inited': function () {
                        var that = this;
                        console.log('loaded: dk-dropdown');

                        var type = that.getMod('type');

                        if (type === 'header-departments') {
                            var curId = Cookies.get('Fillial');
                            if (curId) {
                                var curElem = that.findChildElem({elem: 'choice', modName: 'id', modVal: curId});
                                if (curElem) {
                                    var curVal = curElem.domElem.text();
                                    that.findChildElem('action').domElem.text(curVal);
                                }
                            }

                            that._domEvents(that.findChildElems('choice')).on('click', function (e) {
                                var val = $(e.currentTarget).text();
                                var id = e.bemTarget.getMod('id');
                                console.log(id);
                                var intId = parseInt(id);

                                Cookies.remove('Fillial');
                                if (intId !== 0) {
                                    Cookies.set('Fillial', id);
                                }

                                // that.findChildElem('action').domElem.text(val);
                                location.reload();
                            });
                        }
                    }
                }
            }
        }));

    });

