modules.define(
    'bootstrap-select',
    ['jquery', 'bootstrap'],
    function (provide, jQuery) {
        setTimeout(function () {
            window.$ = jQuery;
            /* borschik:include:../../node_modules/bootstrap-select/dist/js/bootstrap-select.js */
            provide(jQuery);
        }, 0);
    }
);