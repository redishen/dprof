modules.define(
    'bootstrap',
    ['jquery'],
    function (provide, jQuery) {
        setTimeout(function () {
            window.$ = jQuery;
            /* borschik:include:../../node_modules/bootstrap/dist/js/bootstrap.js */
            provide(jQuery);
        }, 0);
    }
);