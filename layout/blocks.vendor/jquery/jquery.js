modules.define(
    'jquery',
    function (provide) {

        function doProvide() {
            clearInterval(waitJQuery);
            provide(jQuery);
        }

        var waitJQuery = setInterval(function () {
            if (typeof jQuery !== 'undefined') {
                doProvide();
            }
        }, 50);
    });