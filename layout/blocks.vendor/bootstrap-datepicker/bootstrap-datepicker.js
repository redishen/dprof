modules.define(
    'bootstrap-datepicker',
    ['jquery', 'bootstrap'],
    function (provide, jQuery) {
        window.$ = jQuery;
        /* borschik:include:../../node_modules/bootstrap-datepicker/dist/js/bootstrap-datepicker.js */
        /* borschik:include:../../node_modules/bootstrap-datepicker/dist/locales/bootstrap-datepicker.ru.min.js */
        provide(jQuery);
    }
);