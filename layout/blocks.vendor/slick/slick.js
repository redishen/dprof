modules.define(
    'slick',
    ['jquery'],
    function (provide, jQuery) {
        window.jQuery = jQuery;
        /* borschik:include:../../node_modules/slick-carousel/slick/slick.min.js */
        provide(jQuery);
    }
);