modules.define(
    'bootstrap-daterangepicker',
    ['jquery', 'moment'],
    function (provide, jQuery) {
        setTimeout(function () {
            window.$ = jQuery;
            /* borschik:include:../../node_modules/daterangepicker/daterangepicker.js */

            window.daterangepickerlocales = {
                "format": "DD.MM.YYYY",
                "separator": " - ",
                "applyLabel": "применить",
                "cancelLabel": "отменить",
                "fromLabel": "с",
                "toLabel": "до",
                "customRangeLabel": "Произвольный",
                "weekLabel": "W",
                "daysOfWeek": ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                "monthNames": ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                "firstDay": 1
            }

            provide(jQuery);
        }, 0);
    }
);