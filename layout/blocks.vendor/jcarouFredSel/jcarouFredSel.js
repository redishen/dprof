modules.define(
    'jcarouFredSel',
    ['jquery'],
    function (provide, jQuery) {
        window.jQuery = jQuery;
        /* borschik:include:../../blocks.vendor/jcarouFredSel/jquery.carouFredSel-6.2.1-packed.js */
        provide(jQuery);
    }
);